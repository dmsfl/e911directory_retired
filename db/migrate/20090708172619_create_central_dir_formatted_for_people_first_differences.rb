class CreateCentralDirFormattedForPeopleFirstDifferences < ActiveRecord::Migration
  def self.up
    create_view(:central_dir_formatted_for_people_first_differences,"SELECT 
       employees.first_name,
       employees.last_name,       
       employees.middle_name,
       employees.people_first_id as employee_id,       
       concat(phones.area_code,'-',
             substring(phones.main_number,1,3),'-', 
             substring(phones.main_number,4,7)) as four_11_phone
        FROM master_relations 
        inner join employees on master_relations.employee_id = employees.id
        inner join phones on master_relations.phone_id = phones.id
        where master_relations.used_for_411 = 1") do |t|
      t.column :first_name
      t.column :last_name
      t.column :middle_name
      t.column :employee_id
      t.column :four_11_phone
    end
  end

  def self.down
    drop_view :central_dir_formatted_for_people_first_differences
  end
end

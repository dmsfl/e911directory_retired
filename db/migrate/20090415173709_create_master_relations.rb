class CreateMasterRelations < ActiveRecord::Migration
  def self.up
    create_table :master_relations do |t|
      t.references :phone
      t.references :location
      t.references :employee
      t.boolean :primary_employee_location, :default => false
      t.boolean :primary_phone_location, :default => false
      t.boolean :primary_employee_for_phone, :default => false
      t.boolean :primary_phone_for_employee, :default => false
      t.boolean :used_for_411, :default => false
      t.string :extension, :limit => 6
      t.timestamps
    end
  end

  def self.down
    drop_table :master_relations
  end
end

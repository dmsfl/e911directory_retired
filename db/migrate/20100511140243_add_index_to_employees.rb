class AddIndexToEmployees < ActiveRecord::Migration
  def self.up
    add_index :employees, [:first_name, :last_name, :division_name], :name => 'first_last_div'
  end

  def self.down
    remove_index :employees, :name => 'first_last_div'
  end
end

class CreatePhones < ActiveRecord::Migration
  def self.up
    create_table :phones do |t|
      t.string :area_code, :limit => 3 
      t.string :main_number, :limit => 7 
      t.references :phone_category
      t.string :comment
      t.timestamps
    end
  end

  def self.down
    drop_table :phones
  end
end

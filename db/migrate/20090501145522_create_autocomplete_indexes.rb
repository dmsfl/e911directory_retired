class CreateAutocompleteIndexes < ActiveRecord::Migration
  def self.up
    add_index :phones, :main_number
    add_index :phones, :area_code
    
    add_index :buildings, :full_street_address
    add_index :buildings, :city
    
    add_index :employees, :first_name
    add_index :employees, :last_name
    
    
  end

  def self.down
    remove_index :phones, :column => :main_number
    remove_index :phones, :column => :area_code

    remove_index :buildings, :column => :full_street_address
    remove_index :buildings, :column => :city

    remove_index :employees, :column => :first_name
    remove_index :employees, :column => :last_name
    
  end
end

class CreateJudyFaxes < ActiveRecord::Migration
  def self.up
    create_table :judy_faxes do |t|
      t.string :lookup_address
      t.string :room
      t.string :long_number
      t.string :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :judy_faxes
  end
end

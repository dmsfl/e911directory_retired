class AddActivatedAtToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :activated_at, :datetime
    User.all.each {|u| u.update_attribute('activated_at', Date.today)}
  end

  def self.down
    remove_column :users, :activated_at
  end
end

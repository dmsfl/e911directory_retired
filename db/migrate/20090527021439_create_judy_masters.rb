class CreateJudyMasters < ActiveRecord::Migration
  def self.up
    create_table :judy_masters do |t|
      t.string :last_name
      t.string :first_name
      t.string :middle_name
      t.string :long_phone
      t.string :program_area
      t.string :work_area
      t.string :lookup_address
      t.string :room
      t.string :email

      t.timestamps
    end
  end

  def self.down
    drop_table :judy_masters
  end
end

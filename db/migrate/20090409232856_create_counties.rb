class CreateCounties < ActiveRecord::Migration
  def self.up
    create_table :counties do |t|
      t.integer :fips_code
      t.string :name
      t.integer :people_first_code

      t.timestamps
    end
  end

  def self.down
    drop_table :counties
  end
end

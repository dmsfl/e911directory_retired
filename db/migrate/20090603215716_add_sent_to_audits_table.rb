class AddSentToAuditsTable < ActiveRecord::Migration
  def self.up
    change_table :audits do |t|
      t.boolean :sent, :default => false
      t.boolean :sent_to_911, :default => false
    end
  end

  def self.down
    change_table :audits do |t|
      t.remove :sent
      t.remove :sent_to_911
    end
  end
end

class CreatePeopleFirsts < ActiveRecord::Migration
  def self.up
    create_table :people_firsts do |t|
      t.integer :county_id
      t.string  :location_room
      t.string  :location_state
      t.string  :location_campus_name
      t.string  :building_name
      t.string  :location_address
      t.string  :vacancy_indicator, :limit => 1
      t.string  :last_name, :limit => 32
      t.string  :first_name, :limit => 32
      t.string  :middle_name, :limit => 32
      t.string  :four_11_phone, :limit => 12
      t.string  :four_11_phone_number_ext, :limit => 10
      t.string  :location_bldg_number
      t.string  :pos_num
      t.string  :olo_code 
      t.string  :location_City, :limit => 32
      t.string  :email, :limit => 100
      t.integer :employee_id, :limit => 10
      t.string  :contractor, :limit => 1
      t.string  :employee_group_desc
      t.string  :name_suffix
      t.string  :mail_zip, :limit => 10
      t.string  :emp_confid_ind
      t.string  :emp_sworn_ind
      t.string  :protected_ind
      t.string  :exempt_ind
      t.string  :confidential, :limit => 1
      t.string  :people_first_user, :limit => 10
      t.timestamps
    end
    add_index :people_firsts, [:employee_id, :last_name, :first_name, :middle_name, :four_11_phone, :four_11_phone_number_ext], {:name => 'ix_pf_name'}
  end

  def self.down
    drop_table :nine_one_one_files
  end
end

class AddDivisionNameToPeopleFirsts < ActiveRecord::Migration
  def self.up
    add_column :people_firsts, :division_name, :string
  end

  def self.down
    remove_column :people_firsts, :division_name
  end
end

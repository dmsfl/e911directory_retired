class AddDivisionNameToEmployees < ActiveRecord::Migration
  def self.up
    add_column :employees, :division_name, :string
  end

  def self.down
    remove_column :employees, :division_name
  end
end

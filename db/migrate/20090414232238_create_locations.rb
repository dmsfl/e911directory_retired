class CreateLocations < ActiveRecord::Migration
  def self.up
    create_table :locations do |t|
      t.references :building
      t.string :floor
      t.string :room
      t.string :building_identifier
      t.timestamps
    end
  end

  def self.down
    drop_table :locations
  end
end

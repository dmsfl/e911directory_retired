class CreateEmployees < ActiveRecord::Migration
  def self.up
    create_table "employees", :force => true do |t|
      #This table is the parent for several sublcasses
      t.string :type, :limit => 30
      
      #Attributes for employees
      t.column  :email,                     :string, :limit => 100
      t.string  :people_first_user, :limit => 6
      t.string  :first_name, :limit => 100
      t.string  :middle_name, :limit => 15
      t.string  :last_name, :limit => 100
      t.string  :suffix, :limit => 3
      t.boolean :contractor_flag
      t.boolean :active_flag
      t.integer :olo_code, :limit => 5
      t.integer :people_first_id, :limit => 10
      t.string  :position_number, :limit => 10
      t.string  :group, :limit => 5
      t.string  :group_description, :limit => 50   
      t.string  :confidential, :limit => 1

    end
      
  end

  def self.down
    drop_table "employees"
  end
end

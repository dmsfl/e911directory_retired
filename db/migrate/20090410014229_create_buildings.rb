class CreateBuildings < ActiveRecord::Migration
  def self.up
    create_table :buildings do |t|
      t.string :name, :limit => 50
      t.references :campus
      t.string :alias, :limit => 50
      t.string :lookup_address, :limit => 700
      t.string :street_number, :limit => 10
      t.string :street_number_suffix, :limit => 4
      t.string :prefix_directional, :limit => 2 
      t.string :street_name, :limit => 60
      t.string :street_suffix, :limit => 4
      t.string :post_directional, :limit => 2
      t.string :city, :limit => 50
      t.string :state, :limit => 2, :default => 'FL'
      t.references :county
      t.string :zip_code_5, :limit => 5
      t.string :zip_code_4, :limit => 4
      t.string :created_by
      t.string :updated_by
      t.string :identifier, :limit => 15
      t.integer :building_number, :limit => 5
      t.string :full_street_address
      t.timestamps
    end
  end

  def self.down
    drop_table :buildings
  end
end

class AddRestrictedEmployeeToPeopleFirsts < ActiveRecord::Migration
  def self.up
    add_column :people_firsts, :restricted_relative_ind, :string
    add_column :people_firsts, :restricted_employee_ind, :string
  end

  def self.down
    remove_column :people_firsts, :restricted_employee_ind
    remove_column :people_firsts, :restricted_relative_ind
  end
end

class CreatePeopleFirstLocationMaps < ActiveRecord::Migration
  def self.up
    create_table :people_first_location_maps do |t|
      t.string :lookup_address
      t.string :house_number
      t.string :house_number_suffix
      t.string :prefix_directioneal
      t.string :street_name
      t.string :street_suffix
      t.string :post_directional
      t.string :unit
      t.string :floor
      t.string :building
      t.references :building
      t.timestamps
    end
  end

  def self.down
    drop_table :people_first_location_maps
  end
end

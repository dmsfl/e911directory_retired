class CreateStreetAbbreviations < ActiveRecord::Migration
  def self.up
    create_table :street_abbreviations do |t|
      t.string :name, :limit => 15 #the post office names max out at 10 characters anyway.
      t.string :standard_abbreviation, :limit => 4

      t.timestamps
    end
  end

  def self.down
    drop_table :street_abbreviations
  end
end

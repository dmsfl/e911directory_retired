# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110711180432) do

  create_table "audit_changes", :force => true do |t|
    t.integer "audit_id"
    t.string  "association_type"
    t.integer "association_id"
    t.string  "field"
    t.text    "old_value"
    t.text    "new_value"
  end

  add_index "audit_changes", ["audit_id", "association_type", "association_id"], :name => "audit_association_index"
  add_index "audit_changes", ["audit_id"], :name => "audit_index"

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.integer  "version",        :default => 0
    t.datetime "created_at"
    t.boolean  "sent",           :default => false
    t.boolean  "sent_to_911",    :default => false
  end

  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "buildings", :force => true do |t|
    t.string   "name",                 :limit => 50
    t.integer  "campus_id"
    t.string   "alias",                :limit => 50
    t.string   "lookup_address",       :limit => 700
    t.string   "street_number",        :limit => 10
    t.string   "street_number_suffix", :limit => 4
    t.string   "prefix_directional",   :limit => 2
    t.string   "street_name",          :limit => 60
    t.string   "street_suffix",        :limit => 4
    t.string   "post_directional",     :limit => 2
    t.string   "city",                 :limit => 50
    t.string   "state",                :limit => 2,   :default => "FL"
    t.integer  "county_id"
    t.string   "zip_code_5",           :limit => 5
    t.string   "zip_code_4",           :limit => 4
    t.string   "created_by"
    t.string   "updated_by"
    t.string   "identifier",           :limit => 15
    t.integer  "building_number",      :limit => 8
    t.string   "full_street_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "buildings", ["city"], :name => "index_buildings_on_city"
  add_index "buildings", ["full_street_address"], :name => "index_buildings_on_full_street_address"

  create_table "campuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "counties", :force => true do |t|
    t.integer  "fips_code"
    t.string   "name"
    t.integer  "people_first_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", :force => true do |t|
    t.string  "type",              :limit => 30
    t.string  "email",             :limit => 100
    t.string  "people_first_user", :limit => 6
    t.string  "first_name",        :limit => 100
    t.string  "middle_name",       :limit => 15
    t.string  "last_name",         :limit => 100
    t.string  "suffix",            :limit => 3
    t.boolean "contractor_flag"
    t.boolean "active_flag"
    t.integer "olo_code",          :limit => 8
    t.integer "people_first_id"
    t.string  "position_number",   :limit => 10
    t.string  "group",             :limit => 5
    t.string  "group_description", :limit => 50
    t.string  "confidential",      :limit => 1
    t.string  "division_name"
  end

  add_index "employees", ["first_name", "last_name", "division_name"], :name => "first_last_div"
  add_index "employees", ["first_name"], :name => "index_employees_on_first_name"
  add_index "employees", ["last_name"], :name => "index_employees_on_last_name"

  create_table "judy_faxes", :force => true do |t|
    t.string   "lookup_address"
    t.string   "room"
    t.string   "long_number"
    t.string   "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "judy_masters", :force => true do |t|
    t.string   "last_name"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "long_phone"
    t.string   "program_area"
    t.string   "work_area"
    t.string   "lookup_address"
    t.string   "room"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", :force => true do |t|
    t.integer  "building_id"
    t.string   "floor"
    t.string   "room"
    t.string   "building_identifier"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "master_relations", :force => true do |t|
    t.integer  "phone_id"
    t.integer  "location_id"
    t.integer  "employee_id"
    t.boolean  "primary_employee_location",               :default => false
    t.boolean  "primary_phone_location",                  :default => false
    t.boolean  "primary_employee_for_phone",              :default => false
    t.boolean  "primary_phone_for_employee",              :default => false
    t.boolean  "used_for_411",                            :default => false
    t.string   "extension",                  :limit => 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people_first_location_maps", :force => true do |t|
    t.string   "lookup_address"
    t.string   "house_number"
    t.string   "house_number_suffix"
    t.string   "prefix_directioneal"
    t.string   "street_name"
    t.string   "street_suffix"
    t.string   "post_directional"
    t.string   "unit"
    t.string   "floor"
    t.string   "building"
    t.integer  "building_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people_firsts", :force => true do |t|
    t.integer  "county_id"
    t.string   "location_room"
    t.string   "location_state"
    t.string   "location_campus_name"
    t.string   "building_name"
    t.string   "location_address"
    t.string   "vacancy_indicator",        :limit => 1
    t.string   "last_name",                :limit => 32
    t.string   "first_name",               :limit => 32
    t.string   "middle_name",              :limit => 32
    t.string   "four_11_phone",            :limit => 12
    t.string   "four_11_phone_number_ext", :limit => 10
    t.string   "location_bldg_number"
    t.string   "pos_num"
    t.string   "olo_code"
    t.string   "location_City",            :limit => 32
    t.string   "email",                    :limit => 100
    t.integer  "employee_id"
    t.string   "contractor",               :limit => 1
    t.string   "employee_group_desc"
    t.string   "name_suffix"
    t.string   "mail_zip",                 :limit => 10
    t.string   "emp_confid_ind"
    t.string   "emp_sworn_ind"
    t.string   "protected_ind"
    t.string   "exempt_ind"
    t.string   "confidential",             :limit => 1
    t.string   "people_first_user",        :limit => 10
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "division_name"
    t.string   "restricted_relative_ind"
    t.string   "restricted_employee_ind"
  end

  add_index "people_firsts", ["employee_id", "last_name", "first_name", "middle_name", "four_11_phone", "four_11_phone_number_ext"], :name => "ix_pf_name"

  create_table "phone_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "phones", :force => true do |t|
    t.string   "area_code",         :limit => 3
    t.string   "main_number",       :limit => 7
    t.integer  "phone_category_id"
    t.string   "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "phones", ["area_code"], :name => "index_phones_on_area_code"
  add_index "phones", ["main_number"], :name => "index_phones_on_main_number"

  create_table "roles", :force => true do |t|
    t.string "name"
    t.string "description"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  add_index "roles_users", ["role_id"], :name => "index_roles_users_on_role_id"
  add_index "roles_users", ["user_id"], :name => "index_roles_users_on_user_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "street_abbreviations", :force => true do |t|
    t.string   "name",                  :limit => 15
    t.string   "standard_abbreviation", :limit => 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "login",                     :limit => 40
    t.string   "crypted_password",          :limit => 40
    t.string   "salt",                      :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token",            :limit => 40
    t.datetime "remember_token_expires_at"
    t.string   "password_reset_code",       :limit => 40
    t.integer  "employee_id"
    t.string   "activation_code"
    t.datetime "activated_at"
  end

  create_view "central_dir_formatted_for_people_first_differences", "select `employees`.`first_name` AS `first_name`,`employees`.`last_name` AS `last_name`,`employees`.`middle_name` AS `middle_name`,`employees`.`people_first_id` AS `employee_id`,concat(`phones`.`area_code`,'-',substr(`phones`.`main_number`,1,3),'-',substr(`phones`.`main_number`,4,7)) AS `four_11_phone` from ((`master_relations` join `employees` on((`master_relations`.`employee_id` = `employees`.`id`))) join `phones` on((`master_relations`.`phone_id` = `phones`.`id`))) where (`master_relations`.`used_for_411` = 1)", :force => true do |v|
    v.column :first_name
    v.column :last_name
    v.column :middle_name
    v.column :employee_id
    v.column :four_11_phone
  end

end

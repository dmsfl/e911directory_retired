require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')
include ApplicationHelper
include AuditsHelper
include AuthenticatedTestHelper

describe AuditsHelper do
  
  describe 'which_item_labels' do 
    #Delete this example and add some real ones or delete this file
    it "item label should return correct items given a master relation change" do
      @audit = mock_model(AuditE911, 
        :changes => {'primary_phone_location' => [true, nil], 
                     'location_id' => [1075,nil], 
                     'primary_employee_location' => [true, nil]}, 
        :auditable_type => 'MasterRelation',
        :revision => mock_model(MasterRelation,{
                      :extension => "",
                      :phone_id => 724,
                      :employee_id => 233,
                      :primary_phone_location => nil,
                      :location_id => nil,
                      :primary_employee_for_phone => false,
                      :used_for_411 => false,
                      :primary_employee_location => nil,
                      :primary_phone_for_employee => false})
      )
      @parent = mock_model(MasterRelation,{
                      :extension => "",
                      :phone_id => 724,
                      :employee_id => 233,
                      :primary_phone_location => false,
                      :location_id => 1075,
                      :primary_employee_for_phone => false,
                      :used_for_411 => false,
                      :primary_employee_location => false,
                      :primary_phone_for_employee => false}
      )
      which_item_labels(@audit, @parent).should eql %w{location employee}
    end
  end
end

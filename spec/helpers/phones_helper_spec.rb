require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

include ApplicationHelper 
describe PhonesHelper do
  #Delete this example and add some real ones or delete this file
  it "is included in the helper object" do
    included_modules = (class << helper; self; end).send :included_modules
    included_modules.should include(PhonesHelper)
  end
  describe 'primary_employee_for_phone' do
    before do
      @ml = MasterRelation.make
    end
    it "should render a checkbox with the master_relation id" do
      @ml.update_attribute(:primary_employee_for_phone, true)
      helper.primary_employee_for_phone(@ml).should match /hide.*value=\"#{@ml.id}\"/
      helper.primary_employee_for_phone(@ml).should match /img.*yes/
    end
  end
end

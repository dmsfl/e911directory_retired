require 'machinist/active_record'
require 'sham'
require 'faker'

Sham.define do
  area_code      { Faker.numerify('###').to_s}
  main_number    { Faker.numerify('#######').to_s}
  comment        { Faker::Lorem.sentence }
  created        { 1.day.ago }
  updated        { 6.hours.ago }
  email          { Faker::Internet.email }
  first_name     { Faker::Name.first_name }
  last_name      { Faker::Name.last_name }
  name_suffix    { Faker::Name.suffix }
  db_bool        { [false,true,nil,0,1].rand }
  db_true        { [true, 1].rand }
  olo_code       { Faker.numerify('#####')}
  people_first_id { Faker.numerify('########')}
  position_number { Faker.numerify('#####')}
  description    { Faker::Lorem.sentence }
  zip_code_4      { Faker.numerify('####')}
  zip_code_5      { Faker.numerify('#####')}
  company         { Faker::Company.name }
end

Phone.blueprint do
  area_code   
  main_number 
  comment
  phone_category
end
PhoneCategory.blueprint do
  name {%w{Mobile Fax Voice Conference}.rand}
end

MasterRelation.blueprint do
  employee
  phone
  location
end
MasterRelation.blueprint(:primary_phone_location) do 
  employee
  phone
  location
  primary_phone_location { true }
end
MasterRelation.blueprint(:all_primaries) do
  employee {Employee.make}
  phone {Phone.make}
  location {Location.make}
  primary_phone_location { true }
  primary_employee_location { true }
  primary_employee_for_phone { true }
  primary_phone_for_employee { true }
  used_for_411 {true}
end
Employee.blueprint do
  email           
  first_name      
  last_name       
  middle_name     { ('A'..'Z').to_a.rand }
  suffix          { Sham.name_suffix }
  contractor_flag { Sham.db_bool }
  active_flag     { [1, true].rand }
  olo_code        
  people_first_id 
  people_first_user {Sham.people_first_id} #in many cases, these two are actually the same value
  position_number
end

Location.blueprint do
  building { Building.make }
  building_identifier { ('A'..'Z').to_a.rand}
  floor    { (1..100).to_a.rand}
  room     { (1..1000).to_a.rand}
end

Building.blueprint do
  name { Faker::Company.name}
  campus { Campus.make }
  street_number { Faker.numerify('####')}
  street_number_suffix { Faker::Address.us_street_suffix_abbr}
  street_name { Faker::Address.street_name}
  post_directional { %w{N S E W NW NE SW SE}.rand}
  prefix_directional { %w{N S E W NW NE SW SE}.rand}
  zip_code_4
  zip_code_5
  city { Faker::Address.city}
  state { Faker::Address.us_state_abbr }
  county { County.first(:conditions => "name = 'LEON'") ? County.first(:conditions => "name = 'LEON'") : County.make }
end
County.blueprint do
  name { 'LEON' }
  fips_code { '073' }
  people_first_code { 37 }
end

Campus.blueprint do
  name { Faker::Company.name}
end

User.blueprint do
  employee { Employee.make }
  login  { employee.people_first_user}
  password { 'hi_therea'}
  password_confirmation { 'hi_therea'}
end

Role.blueprint do
  name {Faker::Lorem.word}
  description
end

User.blueprint(:administrator) do
  roles { [Role.make(:name => 'Administrator')]}
end

AuditE911.blueprint do
  
end

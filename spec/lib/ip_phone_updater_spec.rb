require File.dirname(__FILE__) + '/../spec_helper'
require 'ip_phone_updater'

IpPhoneUpdater.class_eval do
  def bare_results
    @message.add_footer
    @message
  end
end
IpPhoneUpdater.send(:public, 
                    :update_or_create_buildings,
                    :update_or_create_locations,
                    :update_or_create_phones,
                    :update_or_create_master_relations)
describe "IpPhoneUpdater" do
  it "reports results" do
    updater = IpPhoneUpdater.new
    updater.bare_results[0].should match /Begin Processing/
    updater.bare_results[1].should match /\-\-\-/
    updater.bare_results[2].should match /\-\-\-/
    updater.bare_results[3].should match /End Processing/
  end
  describe "update_or_create_buildings" do
    it "should create new buildings" do
      Building.delete_all
      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("bldg_num,bldg_num_suffix,street,suffix,post_dir,city,state,zip\n"+
                                                 "4033,,Esplenade,Way,,Tallahassee,Fl,323013333.0", :headers => :first_row))
      updater = IpPhoneUpdater.new
      updater.update_or_create_buildings
      Building.all.size.should == 1
      Building.first.street_number.should == '4033'
      Building.first.street_name.should == 'ESPLENADE'
    end
    it "does not create existing buildings" do
      Building.delete_all
      Building.create(:street_number => '4033',
                      :street_name => 'ESPLENADE',
                      :street_suffix => 'WAY',
                      :city => 'TALLAHASSEE',
                      :zip_code_5 => '32301',
                      :state => 'FL')
      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("bldg_num,bldg_num_suffix,street,suffix,post_dir,city,state,zip\n"+
                                                 "4033,,Esplenade,Way,,Tallahassee,Fl,323013333.0", :headers => :first_row))
      updater = IpPhoneUpdater.new
      updater.update_or_create_buildings
      Building.all.size.should == 1
    end
  end
  describe "update_or_create_locations" do
    before :each do
      Building.delete_all
      Location.delete_all
      @building = Building.create(:street_number => '4033',
                      :street_name => 'ESPLENADE',
                      :street_suffix => 'WAY',
                      :city => 'TALLAHASSEE',
                      :zip_code_5 => '32301',
                      :state => 'FL')
    end
    it "creates new locations" do
      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("bldg_num,bldg_num_suffix,street,suffix,post_dir,city,state,zip,room\n"+
                                                 "4033,,Esplenade,Way,,Tallahassee,Fl,323013333.0,215.z3", :headers => :first_row))
      updater = IpPhoneUpdater.new
      updater.update_or_create_locations
      Location.all.size.should == 1
      Location.first.building_id.should == @building.id
      Location.first.room.should == '215.z3'
    end
    it "does not create existing locations" do
      Location.create(:building => @building,
                      :room => '215.z3')
      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("bldg_num,bldg_num_suffix,street,suffix,post_dir,city,state,zip,room\n"+
                                                 "4033,,Esplenade,Way,,Tallahassee,Fl,323013333.0,215.z3", :headers => :first_row))
      updater = IpPhoneUpdater.new
      updater.update_or_create_locations
      Location.all.size.should == 1
      Location.first.building_id.should == @building.id
      Location.first.room.should == '215.z3'
    end
  end
  describe "update_or_create_phones" do
    it "creates new phones" do
      Phone.delete_all
      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("voip\n"+
                                                 "8509876543", :headers => :first_row))
      updater = IpPhoneUpdater.new
      updater.update_or_create_phones
      Phone.all.size.should == 1
      Phone.first.area_code.should == '850'
      Phone.first.main_number.should == '9876543'
    end
    it "does not create existing phones" do
      Phone.delete_all
      Phone.create(:area_code => '850',
                   :main_number => '9876543')
      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("voip\n"+
                                                 "8509876543", :headers => :first_row))
      updater = IpPhoneUpdater.new
      updater.update_or_create_phones
      Phone.all.size.should == 1
      Phone.first.area_code.should == '850'
      Phone.first.main_number.should == '9876543'
    end
  end

  describe "update_or_create_master_relations" do
    it "does not create a master relation if it already exists" do
      Phone.delete_all
      phone = Phone.create(:area_code => '850',
                   :main_number => '1113355')

      Building.delete_all
      building = Building.create(:street_number => '4033',
                      :street_name => 'ESPLENADE',
                      :street_suffix => 'WAY',
                      :city => 'TALLAHASSEE',
                      :zip_code_5 => '32301',
                      :state => 'FL')

      Location.delete_all
      location = Location.create(:building => building,
                      :room => '215.z3')

      Employee.delete_all
      employee = Employee.new(:first_name => 'aoeu',
                              :last_name => 'aoeuaoeu')
      employee.save(false)

      MasterRelation.delete_all
      master_relation = MasterRelation.new(:phone => phone,
                                           :location => location,
                                           :employee => employee)
      master_relation.save

      FasterCSV.should_receive(:open).and_return(FasterCSV.parse("voip_name,bldg_num,bldg_num_suffix,street,suffix,post_dir,city,state,zip,room,voip\n"+
                                                 "\"aoeuaoeu, aoeu\",4033,,Esplenade,Way,,Tallahassee,Fl,323013333.0,215.z3,8501113355", :headers => :first_row))

      updater = IpPhoneUpdater.new
      updater.update_or_create_master_relations
      MasterRelation.all.size.should == 1
      MasterRelation.first.phone.should == phone
      MasterRelation.first.location.should == location
      MasterRelation.first.employee.should == employee
    end
  end

  it "audits all changes as a user, in order to send them to 911"
end

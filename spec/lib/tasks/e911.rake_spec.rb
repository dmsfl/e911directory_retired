require File.dirname(__FILE__) + '/../../spec_helper'

require "rake"

describe "rake e911" do
  include AuditHelper 
  before do
    @rake = Rake::Application.new
    Rake.application = @rake
    Rake.application.rake_require "lib/tasks/e911"
    Rake::Task.define_task(:environment)
  end

  describe ':send_changes_to_911' do
    before do
      @update_file = "#{RAILS_ROOT}/lib/tasks/data/active_911_files/STFL_911_daily_changes_#{Date.today.strftime('%Y-%m-%d')}.txt"
      @archive_file = @update_file.gsub('active_911_files','archived_updates_sent_to_911')
    end
    
    after do
      FileUtils.rm(@update_file) if File.exist?(@update_file)
      FileUtils.rm(@archive_file) if File.exist?(@archive_file)
    end
    it 'should not create a file if there are no changes' do
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
      File.exist?(@archive_file).should eql false
    end
    it 'should not add changes for non-leon county changes' do
      c = County.make(:name => 'LEON')
      b = Building.make(:county => County.make( :name => 'Pasco', :fips_code => '12101'))
      l = Location.make(:building => b)
      ml = MasterRelation.make(:location => l)
      Audit.as_user User.make do
        ml.update_attribute(:primary_phone_location, true)
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql false
    end
    it 'should handle phone changes on the comment or phone_category fields' do
      ml = MasterRelation.make(:primary_phone_location)
      phone = ml.phone
      Audit.as_user(User.make) do
        phone.update_attribute(:comment, 'random phone')
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with('random phone')
      AuditE911.count(:conditions => "user_id is not null and sent_to_911 = 0").should eql 0
    end
    
    it 'should handle phone creations by NOT sending them to 911' do
      Audit.as_user(User.make) do
        Phone.make
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
    
    it 'should send phone deletions if the phone had a primary location ' do

      phone = Phone.make(:id => 99999991, :area_code => '111', :main_number => '1111114')
      ml = MasterRelation.make( :phone => phone, :location => Location.make)
      ml.update_attribute(:primary_phone_location, true)
      Audit.as_user User.make do
        Phone.destroy(phone)
      end
      @rake['e911:send_911_daily_update'].invoke
      #File.exist?(@archive_file).should eql true
      #@archive_file.should have_a_line_with(/^D.*1111111114.*4050.*ESPLANADE/)
    end

    it 'should handle employee changes on several fields' do
      ml = MasterRelation.make(:all_primaries)
      Audit.as_user(APP_CONFIG['settings']['cron_auditor']) do
        ml.employee.update_attribute(:first_name, 'Polyglot')
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^U.*Polyglot/)
    end

    it "should handle employee changes when employees don't have a primary phone" do
      ml = MasterRelation.make(:primary_phone_location)
      Audit.as_user(APP_CONFIG['settings']['cron_auditor']) do
        ml.employee.update_attribute(:first_name, 'Polyglot')
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end

    it 'should handle employee creations' do
      Audit.as_user(APP_CONFIG['settings']['cron_auditor']) do
        Employee.make
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
    it "should handle employee deletions if the employee belonged to a primary_employee_for_phone relation" do
      ml = MasterRelation.make(:all_primaries)
      employee_last_name = ml.employee.last_name
      Audit.as_user(APP_CONFIG['settings']['cron_auditor']) do
        ml.employee.destroy
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should_not have_a_line_with(/^U.*#{employee_last_name}/)
      @archive_file.should have_a_line_with(/^U.*#{ml.phone.main_number}.*#{ml.location.building.street_name}/)
    end

    it "should handle employee deletions if the employee DID NOT belong to a primary_employee_for_phone relation" do
      ml = MasterRelation.make(:all_primaries)
      ml.update_attribute(:primary_employee_for_phone, false)

      Audit.as_user(APP_CONFIG['settings']['cron_auditor']) do
        ml.employee.destroy
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
    it "should handle employee deletions if the employee DID NOT belong to any relations" do
      employee = Employee.make
      Audit.as_user(APP_CONFIG['settings']['cron_auditor']) do
        employee.destroy
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end

    it 'should handle location edits if the location had many primary phones' do
      location = Location.make
      ml1 = MasterRelation.make(:all_primaries)
      ml1.location = location
      ml1.save
 
      ml2 = MasterRelation.make(:all_primaries)
      ml2.location = location
      ml2.save

      Audit.as_user(User.make) do
        location.update_attribute(:room, 'aaa')
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true

      @archive_file.should have_a_line_with(/^U.*#{ml1.phone.main_number}.*Rm aaa/)
      @archive_file.should have_a_line_with(/^U.*#{ml2.phone.main_number}.*Rm aaa/)

    end

    it 'should handle location changes if the location did not have any primary phones' do
      ml = MasterRelation.make(:all_primaries)
      location = ml.location
      ml.location = nil
      ml.save
      Audit.as_user(User.make) do
        location.update_attribute(:room, 'bbb')
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
    
    it 'should handle location creations' do
      Audit.as_user(User.make) do
        Location.make
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
    
    it 'should handle location deletions if the location had a ml' do
      county = County.make(:name => 'LEON')
      building = Building.make(:county => county)
      location = Location.make(:building => building)
      
      ml1 = MasterRelation.make(:all_primaries)
      ml1.location = location
      ml1.save
      
      ml2 = MasterRelation.make(:all_primaries)
      ml2.location = location
      ml2.save
      
      Audit.as_user(User.make) do
        location.destroy
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true

      @archive_file.should have_a_line_with(/^D.*#{ml1.phone.main_number}/)
      @archive_file.should have_a_line_with(/^D.*#{ml2.phone.main_number}/)

    end
    
    it 'should handle location deletions if the location did not have
       have an ml' do
      location = Location.make
      Audit.as_user(User.make) do
        location.destroy
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql false
    end
    
    it 'should handle building edits' do
      ml = MasterRelation.make(:all_primaries)
      ml.save
      ml2 = MasterRelation.make(:all_primaries)
      ml2.primary_phone_location = false
      ml2.save
      new_value =  'aaaaaa'
      Audit.as_user(User.make) do
        ml.location.building.update_attribute(:street_name, 'aaaaaa')
        ml2.location.building.update_attribute(:street_name, 'aaaaaa')
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true

      @archive_file.should have_a_line_with(/^U.*#{ml.phone.main_number}.*#{new_value}/)
      @archive_file.should_not have_a_line_with(/^U.*#{ml2.phone.main_number}.*#{new_value}/)

    end
    it 'should handle building creations' do
      Audit.as_user(User.make) do
        Building.make
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
    
    it 'should handle building deletions' do
      county = County.make(:name => 'LEON')
      building = Building.make(:county => county)
      
      ml1 = MasterRelation.make(:all_primaries)
      ml1.location.building = building
      ml1.location.save
      
      ml2 = MasterRelation.make(:all_primaries)
      ml2.location.building = building
      ml2.location.save
      
      ml3 = MasterRelation.make(:all_primaries)
      ml3.location.building = building
      ml3.primary_phone_location = false
      ml3.save
      Audit.as_user(User.make) do
        building.destroy
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^D.*#{ml1.phone.main_number}/)
      @archive_file.should have_a_line_with(/^D.*#{ml2.phone.main_number}/)
      @archive_file.should_not have_a_line_with(/^D.*#{ml3.phone.main_number}/)
    end
    
    it 'should handle master relation updates where assigned phone exists but is relation is no longer primary_phone_location' do
      ml1 = MasterRelation.make(:all_primaries)
      ml1.phone.update_attribute(:main_number, '3333333')
      
      Audit.as_user(User.make) do
        ml1.update_attribute(:primary_phone_location, false) 
      end
      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^D.*#{ml1.phone.main_number}/)
    end
    it 'should handle master_relation updates where a primary_phone location is assigned for the first time' do
      ml1 = MasterRelation.make
      ml1.location = Location.make
      ml1.phone = Phone.make
      ml1.save
      Audit.as_user(User.make) do
        ml1.update_attribute(:primary_phone_location, true)
      end

      @rake['e911:send_911_daily_update'].invoke

      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^I.*#{ml1.phone.main_number}/)
      
    end
    it 'should handle master_relation_updates where a phone is created, then assigned then unassigned to a 911 location' do
      Audit.as_user User.make do
        p = Phone.make
        ml = MasterRelation.make(:phone => p)
        ml.update_attribute(:primary_phone_location, true)
        ml.update_attribute(:primary_phone_location, false)
      end
      @rake['e911:send_911_daily_update'].invoke
      @archive_file.should have_a_line_with(/^D/)
    end
    #This case will not occur by using the application, however if someone changes the database
    #Through script/console, this should catch a basic action.    
    it 'should handle phones and primary locations being set at the same time (in case someone changes the database manually)'do
      ml1 = MasterRelation.make
      ml1.location = Location.make
      ml1.phone = Phone.make
      Audit.as_user(User.make) do
        ml1.update_attribute(:primary_phone_location, true)
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^I.*#{ml1.phone.main_number}/)

    end
    it 'should handle master_relation updates where a phone is assigned to a different location' do
      ml1 = MasterRelation.make(:all_primaries)
      
      ml2 = MasterRelation.make(:location => Location.make)
      
      #There are a lot of saves here to mimic how the application would save records.
      Audit.as_user(User.make) do
        ml1.update_attribute(:primary_phone_location, false)
        ml2.phone = ml1.phone
        ml2.save
        ml1.phone = nil
        ml1.save
        ml1.primary_phone_location = false
        ml2.primary_phone_location = true
        ml1.save
        ml2.save
      end

      @rake['e911:send_911_daily_update'].invoke

      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^U.*#{ml2.phone.main_number}.*#{ml2.location.room}/)
    end
    
    it "should handle master_relation changes when a location is removed" do
      ml = MasterRelation.make(:all_primaries)
      Audit.as_user(User.make) do
        ml.location = nil
        ml.save
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^D.*#{ml.phone.main_number}/)
    end
    
    it "should handle employee removals from master_relations" do
      ml = MasterRelation.make(:all_primaries)
      Audit.as_user(User.make) do
        ml.employee = nil
        ml.save
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^U.*#{ml.phone.main_number}/)
    end
    it "should handle employee additions from master_relations" do
      ml = MasterRelation.make(:all_primaries)
      ml.employee = nil
      ml.save
      Audit.as_user(User.make) do
        ml.employee = Employee.make
        ml.primary_employee_for_phone = true
        ml.save
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^U.*#{ml.phone.main_number}.*#{ml.employee.last_name}/)
    end
    it "should handle employee additions from master_relations (alternate)" do
      ml = MasterRelation.make(:all_primaries)
      ml.employee = nil
      ml.save
      ml.employee = Employee.make
      ml.save
      Audit.as_user(User.make) do
        ml.primary_employee_for_phone = true
        ml.save
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@archive_file).should eql true
      @archive_file.should have_a_line_with(/^U.*#{ml.phone.main_number}.*#{ml.employee.last_name}/)
    end
    it "should handle employee additions from master_relations when the employee is NOT assigned as the primary_emp_for_phone" do
      ml = MasterRelation.make(:all_primaries)
      ml.employee = nil
      ml.save
      Audit.as_user(User.make) do
        ml.employee = Employee.make
        ml.save
      end

      @rake['e911:send_911_daily_update'].invoke
      File.exist?(@update_file).should eql false
    end
  end
end

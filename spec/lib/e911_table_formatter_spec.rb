require File.dirname(__FILE__) + '/../spec_helper'

require "rake"

describe "e911_table_formatter" do

  before do
    PeopleFirst.delete_all
    User.delete_all
    Employee.delete_all
    Phone.delete_all
    @update_file = File.expand_path(File.dirname(__FILE__) + '/TEST.TXT')
  end
  
  after do
    #FileUtils.rm(@update_file) if File.exist?(@update_file)
  end
  
  it 'should create a file from phone information' do
    ml = MasterRelation.make(:primary_phone_location)
    phone = ml.phone
    phone_id_and_function_code = {phone.id => 'U'}
    Audit.find(:first, :conditions => {:auditable_type => 'Phone'}).update_attribute('sent_to_911', true)
    phone.update_attribute(:main_number, '1111112')
    E911TableFormatter.new(phone_id_and_function_code, @update_file).table_created.should eql(true)
    File.exist?(@update_file).should eql(true)
    #@update_file.should have_a_line_with('ho')
  end
end


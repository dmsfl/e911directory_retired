require File.dirname(__FILE__) + '/../spec_helper'

describe 'AuditHelper' do
  before do
    Employee.delete_all
    PeopleFirst.delete_all
    Phone.delete_all
    @request = stub(ActionController::Request, :session => {:current_user => nil })
  end
  it 'should be included in Phone, Location, and Employee' do
    Phone.included_modules.should include(AuditHelper)
    Employee.included_modules.should include(AuditHelper)
    Location.included_modules.should include(AuditHelper)
  end
  
  describe 'has_master_relation_with_primary_phone_location?' do
    before(:each) do
      @p = Phone.make(:id => 2)
      @l = Location.make(:id => 2)
    end
    it "should return true for phone with primary_phone_location" do
      ml = MasterRelation.make(:phone_id => 2, :location_id => 2, :primary_phone_location => true)
      @p.has_master_relation_with_primary_phone_location?.should == true
    end
    it "should return false if the phone is missing" do
      ml = MasterRelation.make(:phone => nil, :location_id => 2, :primary_phone_location => true)
      @l.has_master_relation_with_primary_phone_location?.should == false
    end
    it "should return false if the location is missing" do
      ml = MasterRelation.make(:phone_id => 2, :location => nil, :primary_phone_location => true)
      @p.has_master_relation_with_primary_phone_location?.should == false
    end
    it "should return false if the primary_phone_location is false" do
      ml = MasterRelation.make(:phone_id => 2, :location_id => 2, :primary_phone_location => false)
      @p.has_master_relation_with_primary_phone_location?.should == false
    end
  end
  
  describe 'select_associated_911_phones' do
    
    it 'should return an error if a phone has more than one master_relation with a primary phone' do
      ml1 = MasterRelation.make(:phone_id => 3, :location_id => 3)
      ml2 = MasterRelation.make(:phone_id => 3, :location_id => 4)
      
      #for some reason mass assignment of this attr is not allowed
      ml1.primary_phone_location = true
      ml2.primary_phone_location = true
      
      p = Phone.make(:id => 3)
      p.should_receive(:master_relations).and_return [ml1, ml2]
      lambda {p.select_associated_911_phones}.should raise_error (AuditHelper::TooMany911LocationsError)
    end
    
    it 'should return an array with several phones if a location has more than one phone whose primary location is itself' do
      l = Location.make(:id => 5)
      ml1 = MasterRelation.make(:phone_id => 4, :location_id => 5)
      ml2 = MasterRelation.make(:phone_id => 3, :location_id => 5)
      ml3 = MasterRelation.make(:phone_id => 5, :location_id => 5)

      #for some reason mass assignment of this attr is not allowed
      ml1.primary_phone_location = true
      ml2.primary_phone_location = true
      l.should_receive(:master_relations).and_return [ml1, ml2, ml3]
      l.select_associated_911_phones.size.should == 2
    end
    
    it 'should return an empty array if there are no 911 phones with itself' do
      l = Location.make(:id => 5)
      ml1 = MasterRelation.make(:phone => nil, :location_id => 5)
      ml2 = MasterRelation.make(:phone_id => 3, :location => nil)
      ml3 = MasterRelation.make(:phone_id => 5, :location_id => 5)
      ml1.primary_phone_location = true
      ml2.primary_phone_location = true
      ml3.primary_phone_location = false
      l.should_receive(:master_relations).and_return [ml1, ml2, ml3]
      l.select_associated_911_phones.should eql([])
    end
  end
end

require File.dirname(__FILE__) + '/../spec_helper'

describe UsersController do
  fixtures :users, :employees

  describe "reset_password" do
    before do
      @u = User.make
      @u.send(:make_password_reset_code)
      @u.save
      login_as @u
    end
    it "should redirect to the search screen if the reset is successful" do
      post :reset_password, :id => @u.password_reset_code, :user => {:password => 'aaaa', :password_confirmation => 'aaaa'}
      response.should redirect_to :controller => 'search', :action => 'index'
    end
  end

  # TODO: Fill in the rest of tests for user controller.  
  describe "route generation" do
    it "should route users's 'index' action correctly" do
      route_for(:controller => 'users', :action => 'index').should == "/users"
    end
    
    it "should route users's 'show' action correctly" do
      route_for(:controller => 'users', :action => 'show', :id => '1').should == "/users/1"
    end
    
    it "should route users's 'edit' action correctly" do
      route_for(:controller => 'users', :action => 'edit', :id => '1').should == "/users/1/edit"
    end
    
    it "should route users's 'update' action correctly" do
      route_for(:controller => 'users', :action => 'update', :id => '1').should == {:path => "/users/1", :method => :put}
    end
    
    it "should route users's 'destroy' action correctly" do
      route_for(:controller => 'users', :action => 'destroy', :id => '1').should == {:path => "/users/1", :method => :delete}
    end
    
  end
  
  describe "route recognition" do
    it "should generate params for users's index action from GET /users" do
      params_from(:get, '/users').should == {:controller => 'users', :action => 'index'}
    end
    
    it "should generate params for users's show action from GET /users/1" do
      params_from(:get , '/users/1').should == {:controller => 'users', :action => 'show', :id => '1'}
    end
    
    it "should generate params for users's edit action from GET /users/1/edit" do
      params_from(:get , '/users/1/edit').should == {:controller => 'users', :action => 'edit', :id => '1'}
    end
    
    it "should generate params {:controller => 'users', :action => 'update', :id => '1'} from PUT /users/1" do
      params_from(:put , '/users/1').should == {:controller => 'users', :action => 'update', :id => '1'}
    end
    
  end
  
  describe "named routing" do
    
    it "should route users_path() to /users" do
      users_path().should == "/users"
    end
    
    it "should route user_path(:id => '1') to /users/1" do
      user_path(:id => '1').should == "/users/1"
    end
    
    it "should route edit_user_path(:id => '1') to /users/1/edit" do
      edit_user_path(:id => '1').should == "/users/1/edit"
    end
  end

end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe MasterRelationsController do
  fixtures :users, :roles
  before do
    login_as :administrator_user
  end
  before :each do
    @master_relation = mock_model(MasterRelation)
  end
  

  describe "GET edit" do
    before do
      @ml = MasterRelation.make
    end
    describe "when an employee is clicked" do
      it "should retrieve a form for changing or removing the employee" do
        get :edit, {:id => @ml.id.to_s, :component => 'employee', :from => 'phone'}
        assigns[:master_relation].should == @ml
        assigns[:component].should == @ml.employee
        assigns[:from].should == :phone
        response.should_not be_redirect
        response.should be_success
      end
    end
  end
  describe "GET edit_component" do
    before do
      @ml = MasterRelation.make
    end
    describe "with a master_relation, from a phone, requesting to change the employee " do
      before do
        get :edit_component, {:id => @ml.id, :from => 'phone', :component => 'employee'}
      end
      it "should expose master_relation" do
        assigns[:master_relation].should == @ml
      end
      it "should expose component the item came from" do
        assigns[:from].should == 'phone'
      end
      it "should expose the component that's being modified" do
        assigns[:component].should == 'employee'
      end
    end
    describe "with a master_relation, from a phone, requesting to change the location " do
      before do
        get :edit_component, {:id => @ml.id, :from => 'phone', :component => 'location'}
      end
      it "should expose master_relation" do
        assigns[:master_relation].should == @ml
      end
      it "should expose component the item came from" do
        assigns[:from].should == 'phone'
      end
      it "should expose the component that's being modified" do
        assigns[:component].should == 'location'
      end
    end
  end
  describe "GET delete_component" do
    before do
      @ml = MasterRelation.make
      session[:from] = 'phone'
      session[:from_id] = @ml.phone_id
    end
    describe "deleting an employee" do
      before do
        get :delete_component, :id => @ml.id.to_s, :component => 'employee'
      end
      it "should remove an employee, and not the phone or location" do
        @ml.reload
        @ml.employee.should be_nil
        @ml.phone.should_not be_nil
        @ml.location.should_not be_nil
      end
      it "should send a friendly flash back" do
        flash[:notice].should match /successful/i
      end
      it "should redirect back to the phone" do
        response.should redirect_to edit_phone_path(@ml.phone_id)
      end
    end
    describe "deleting the second-to-last component" do
      before do
        @ml.location = nil
        @ml.save
        get :delete_component, :id => @ml.id.to_s, :component => 'employee'
      end
      it "should remove an employee, and not the phone or location" do
        lambda { @ml.reload }.should raise_error
      end
      it "should send a friendly flash back" do
        flash[:notice].should match /successful/i
      end
    end
  end
  describe 'add_employee_search_results' do
    before do
      @e1 = Employee.make(:last_name => 'zzetelles')
      @e2 = Employee.make(:last_name => 'zyjtea')
      @e3 = Employee.make(:last_name => 'Tamoeu')
      @ml = MasterRelation.make
      post :add_employee_search_results, :from => 'phone', :id => @ml.id.to_s, 
        :search => {:last_name_like => 'te', :order => 'descend_by_last_name'}
    end
    it "should receive a set of employees from parameters" do
      assigns[:employees].should include(@e1, @e2)
      assigns[:employees].should_not include(@e3)
    end
    it "should remember which phone I'm coming from" do
      session[:from].should == 'phone'
      session[:from_id].should == @ml.phone_id
    end
    it "should paginate if there are more than 10 employees" do
      assigns[:employees].should respond_to :total_entries
    end
    it "should have a full set of parameters for sorting results" do
      assigns[:order_params][:last_name][:search].keys.should include('last_name_like')
      assigns[:order_params][:last_name][:search]['order'].should == 'ascend_by_last_name'
    end
    it "should sort by the given sort order (in this case, descend by last name)" do
      e = assigns[:employees]
      e.index(@e1).should < e.index(@e2)
    end
  end

  describe 'add_location_search_results' do
    before do
      @b = Building.make(:name => 'some')
      @l1 = Location.make(:building => @b, :floor => 2)
      @l2 = Location.make(:building => @b, :floor => 2)
      @l3 = Location.make(:building => @b, :floor => 1)
      @l4 = Location.make(:building => Building.make, :floor => 2)
      @ml = MasterRelation.make
    end
    describe "searching for locations" do
      before do
        session[:master_relation] = MasterRelation.make.id
        post :add_location_search_results, :location_query => 'true',
          :search => {:building_id => @b.id.to_s, :floor_equals => 2, :order => 'descend_by_floor'}
      end
      it "should expose a set of locations based on search parameters" do
        assigns[:locations].should include(@l1, @l2)
        assigns[:locations].should_not include(@l3, @l4)
      end
      it "should paginate if there are more than 10 locations" do
        assigns[:locations].should respond_to :total_entries
      end
      it "should expose the full set of search parameters for sorting results" do
        assigns[:order_params][:floor][:search].keys.should include('floor_equals')
        assigns[:order_params][:floor][:search]['order'].should == 'ascend_by_floor'
      end
      it "should sort by the given sort order (in this case, descend by last name)" do
        e = assigns[:locations]
        e.index(@l1).should < e.index(@l2)
      end
    end
    describe "searching for buildings" do
      before do
        @b2 = Building.make(:name => 'aoeu')
      end
      it "should figure out that it should be searching for a building" do
        Building.should_receive(:name_like).and_return(Building.city_like(''))
        get :add_location_search_results, :building_query => 'true', :search => {:name_like => 'som'}
      end
      it "should find buildings by name" do
        get :add_location_search_results, :building_query => 'true', :search => {:name_like => 'som'}
        assigns[:buildings].should include(@b)
        assigns[:buildings].should_not include(@b2)
      end
      it "should expose search parameters for sorting results" do
        get :add_location_search_results, :building_query => 'true', :search => {:name_like => 'som'}
        assigns[:order_params][:name][:search].keys.should include('name_like')
        assigns[:order_params][:full_street_address][:search].keys.should include('name_like')
        assigns[:order_params][:full_street_address][:search]['order'].should == 'descend_by_full_street_address'
      end
    end
  end
    describe "get add_phone_search_results" do
      before :each do
        @ml = MasterRelation.make
        @p1 = Phone.make(:main_number => '1231234')
        @p2 = Phone.make(:main_number =>'1235678')
        @p3 = Phone.make(:main_number => '9999999')
        session[:master_relation] = @ml.id
        post :add_phone_search_results, :id => @ml.id.to_s, :from => 'employee', :search => {:main_number => '123'}
      end
      it "should assign a set of phones from parameters" do
        assigns[:phones].should include(@p1, @p2)
        assigns[:phones].should_not include(@p3)
      end
      it "should remember which employee I'm coming from" do
        session[:from].should == 'employee'
        session[:from_id].should == @ml.employee_id
      end
      it "should paginate if there are more than 10 phones" do
        assigns[:phones].should respond_to :total_entries
      end
    end
    describe "get add_phone_search_results for creating a new master relation" do
      before do
        @employee = Employee.make
        @p1 = Phone.make(:main_number => '1231234')
        @p2 = Phone.make(:main_number =>'1235678')
        @p3 = Phone.make(:main_number => '9999999')
        session[:from] = 'employee'
        session[:from_id] = @employee.id
        post :add_phone_search_results, :search => {:main_number => '123'}, :new => 'true'
      end
      it "should assign a set of phones from parameters" do
        assigns[:phones].should include(@p1, @p2)
        assigns[:phones].should_not include(@p3)
      end
      it "should remember which employee I'm coming from" do
        session[:from].should == 'employee'
        session[:from_id].should == @employee.id
      end
      it "should paginate if there are more than 10 phones" do
        assigns[:phones].should respond_to :total_entries
      end
      it 'should assign master_relation as a new master_relation' do
        assigns[:master_relation].should be_new_record
      end
    end
  describe "new" do
    before do
      @employee = Employee.make
    end
    it "GET should remember where I'm coming from" do
      get :new, :from => 'employee', :id => @employee.id.to_s, :component => 'phone'
      assigns[:from].should == 'employee'
      assigns[:from_id].should == @employee.id.to_s
      assigns[:from_object].should == @employee
      assigns[:search_form_partial] = 'find_phones'
      assigns[:component].should == 'phone'
      assigns[:locals].keys.should include(:phone_to_search, :destination, :html_results)
    end
  end
  describe "PUT update" do
    before do
      @ml = MasterRelation.make
    end
    %w{employee phone}.each do |from|
      %w{employee phone location}.each do |component|
        describe "updates an #{component} from a #{from}" do
          before do
            session[:from] = from
            session[:from_id] = @ml.reload.send(from+'_id')
            eval("@#{component} = #{component.classify}.make")
            put :update, :id => @ml.id.to_s, component.to_sym => eval("@"+component+".id")
          end
          it "should save the new #{component}" do
            @ml.reload.send(component+'_id').should == eval("@"+component+".id")
          end
        end
      end
    end
  end


  describe "save_primaries" do
    @primaries_for_phone = [ :primary_phone_location, 
      :primary_employee_for_phone]

    @primaries_for_phone.each do |primary_type|
      describe "saving from a \"phone\", the #{primary_type}" do
        before do
          @ml = MasterRelation.make(primary_type => false)
          @ml2 = MasterRelation.make(:location => @ml.location, 
                                     :phone => @ml.phone,
                                     primary_type => false)
          flash[:params] = {}
          flash[:params][:from] = 'phone'
          flash[:params][:from_id] = @ml.phone.id.to_s
          flash[:params][primary_type] = @ml.id.to_s

          [ :primary_phone_location, 
            :primary_employee_for_phone].reject {|item| item == primary_type}.each do |other_primary|
            controller.should_receive(('save_'+other_primary.to_s).to_sym).and_return({})
          end
          controller.should_receive(:redirect_to).and_return true
        end
        it "should save the #{primary_type} when given the params" do
          controller.save_primaries
          @ml.reload.send(primary_type).should == true
          @ml2.reload.send(primary_type).should == false
        end
        it "should set @success :new_save when a change was made" do
          controller.save_primaries
          controller.instance_variable_get(:@success).should == :new_save
        end
        it "should not set @success :new_save when there weren't any changes" do
          flash[:params][primary_type] = nil
          controller.save_primaries
          controller.instance_variable_get(:@success).should_not == :new_save
        end
      end
    end
    @primaries_for_employee= [ :primary_employee_location, 
     :primary_phone_for_employee,
     :used_for_411]

    @primaries_for_employee.each do |primary_type|
      describe "saving from an \"employee\", the #{primary_type}" do
        before do
          @ml = MasterRelation.make(primary_type => false)
          @ml2 = MasterRelation.make(:location => @ml.location, 
                                     :employee => @ml.employee,
                                     primary_type => false)
          flash[:params] = {}
          flash[:params][:from] = 'employee'
          flash[:params][:from_id] = @ml.employee.id.to_s
          flash[:params][primary_type] = @ml.id.to_s

          [ :primary_employee_location, 
            :primary_phone_for_employee,
            :used_for_411].reject {|item| item == primary_type}.each do |other_primary|
            controller.should_receive(('save_'+other_primary.to_s).to_sym).and_return({})
          end
          controller.should_receive(:redirect_to).and_return true
        end

        it "should save the #{primary_type} when given the params" do
          controller.save_primaries
          @ml.reload.send(primary_type).should == true
          @ml2.reload.send(primary_type).should == false
        end
        it "should set @success :new_save when a change was made" do
          controller.save_primaries
          controller.instance_variable_get(:@success).should == :new_save
        end
        it "should not set @success :new_save when there weren't any changes" do
          flash[:params][primary_type] = nil
          controller.save_primaries
          controller.instance_variable_get(:@success).should_not == :new_save
        end
      end
    end
  end
end

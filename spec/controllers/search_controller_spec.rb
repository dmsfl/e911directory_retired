require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SearchController do
  fixtures :roles, :users

  before do
    login_as :administrator_user  end
  describe "route generation" do
    it "should route search's 'index' action correctly" do
      route_for(:controller => 'search', :action => 'index').should == "/search"
    end
  end
  
  it "should always figure out if the user is an administrator" do
    controller.should_receive(:administrator?)
    get :index
  end
end

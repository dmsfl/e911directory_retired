require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe ReportsController do
  fixtures :roles, :users

  before do
    login_as :administrator_user
  end

  #Delete these examples and add some real ones
  it "should use ChangesController" do
    controller.should be_an_instance_of(ReportsController)
  end


  describe "GET 'index'" do
    it "should be successful" do
      get 'index'
      response.should be_success
    end
  end
end

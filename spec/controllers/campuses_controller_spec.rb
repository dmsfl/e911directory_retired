require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe CampusesController do
  fixtures :roles, :users
  
  before do
    login_as :administrator_user
  end
  
  def mock_campus(stubs={})
    @mock_campus ||= mock_model(Campus, stubs)
  end
  
  describe "GET index" do
    fixtures :users, :roles
    before do
      login_as :administrator_user
    end
    it "exposes all campus as @campus" do
      Campus.should_receive(:find).with(:all).and_return([mock_campus])
      get :index
      assigns[:campuses].should == [mock_campus]
    end

    describe "with mime type of xml" do
  
      it "renders all campus as xml" do
        Campus.should_receive(:find).with(:all).and_return(campus = mock("Array of Campus"))
        campus.should_receive(:to_xml).and_return("generated XML")
        get :index, :format => 'xml'
        response.body.should == "generated XML"
      end
    
    end

  end

  describe "GET show" do

    it "exposes the requested campus as @campus" do
      Campus.should_receive(:find).with("37").and_return(mock_campus)
      get :show, :id => "37"
      assigns[:campus].should equal(mock_campus)
    end
    
    describe "with mime type of xml" do

      it "renders the requested campus as xml" do
        Campus.should_receive(:find).with("37").and_return(mock_campus)
        mock_campus.should_receive(:to_xml).and_return("generated XML")
        get :show, :id => "37", :format => 'xml'
        response.body.should == "generated XML"
      end

    end
    
  end

  describe "GET new" do
  
    it "exposes a new campus as @campus" do
      Campus.should_receive(:new).and_return(mock_campus)
      get :new
      assigns[:campus].should equal(mock_campus)
    end

  end

  describe "GET edit" do
  
    it "exposes the requested campus as @campus" do
      Campus.should_receive(:find).with("37").and_return(mock_campus)
      get :edit, :id => "37"
      assigns[:campus].should equal(mock_campus)
    end

  end

  describe "POST create" do

    describe "with valid params" do
      
      it "exposes a newly created campus as @campus" do
        Campus.should_receive(:new).with({'these' => 'params'}).and_return(mock_campus(:save => true))
        post :create, :campus => {:these => 'params'}
        assigns(:campus).should equal(mock_campus)
      end

      it "redirects to the created campus" do
        Campus.stub!(:new).and_return(mock_campus(:save => true))
        post :create, :campus => {}
        response.should redirect_to(campus_url(mock_campus))
      end
      
    end
    
    describe "with invalid params" do

      it "exposes a newly created but unsaved campus as @campus" do
        Campus.stub!(:new).with({'these' => 'params'}).and_return(mock_campus(:save => false))
        post :create, :campus => {:these => 'params'}
        assigns(:campus).should equal(mock_campus)
      end

      it "re-renders the 'new' template" do
        Campus.stub!(:new).and_return(mock_campus(:save => false))
        post :create, :campus => {}
        response.should render_template('new')
      end
      
    end
    
  end

  describe "PUT udpate" do

    describe "with valid params" do

      it "updates the requested campus" do
        Campus.should_receive(:find).with("37").and_return(mock_campus)
        mock_campus.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :campus => {:these => 'params'}
      end

      it "exposes the requested campus as @campus" do
        Campus.stub!(:find).and_return(mock_campus(:update_attributes => true))
        put :update, :id => "1"
        assigns(:campus).should equal(mock_campus)
      end

      it "redirects to the campus" do
        Campus.stub!(:find).and_return(mock_campus(:update_attributes => true))
        put :update, :id => "1"
        response.should redirect_to(campus_url(mock_campus))
      end

    end
    
    describe "with invalid params" do

      it "updates the requested campus" do
        Campus.should_receive(:find).with("37").and_return(mock_campus)
        mock_campus.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :campus => {:these => 'params'}
      end

      it "exposes the campus as @campus" do
        Campus.stub!(:find).and_return(mock_campus(:update_attributes => false))
        put :update, :id => "1"
        assigns(:campus).should equal(mock_campus)
      end

      it "re-renders the 'edit' template" do
        Campus.stub!(:find).and_return(mock_campus(:update_attributes => false))
        put :update, :id => "1"
        response.should render_template('edit')
      end

    end

  end

  describe "DELETE destroy" do

    it "destroys the requested campus" do
      Campus.should_receive(:find).with("37").and_return(mock_campus)
      mock_campus.should_receive(:destroy)
      delete :destroy, :id => "37"
    end
  
    it "redirects to the campus list" do
      Campus.stub!(:find).and_return(mock_campus(:destroy => true))
      delete :destroy, :id => "1"
      response.should redirect_to(campus_url)
    end

  end

end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PhonesController do
  fixtures :roles, :users, :phone_categories
  before do
    login_as :administrator_user
  end
  def mock_phone(stubs={:area_code => '111',:main_number => '1234567', :phone_category_id => 1, :comment => 'old_comment'})
    @mock_phone ||= mock_model(Phone, stubs)
  end
  
  describe "GET show" do

    it "assigns the requested phone as @phone" do
      Phone.should_receive(:find).with("37", :include => ['master_relations', 'locations', 'employees']).and_return(mock_phone)
      get :show, :id => "37"
      assigns[:phone].should equal(@mock_phone)
    end
  end


  describe "GET new" do
    it "assigns a new phone as @phone" do
      Phone.stub!(:new).and_return(mock_phone)
      @mock_phone.should_receive(:phone_category=).with(PhoneCategory.find_by_name('Voice'))
      get :new
      assigns[:phone].should equal(@mock_phone)
    end
  end

  describe "GET edit" do
    it "assigns the requested phone as @phone" do
      Phone.should_receive(:find).with("37", :include => ['master_relations', 'locations', 'employees']).at_least(1).and_return(mock_phone)
      get :edit, :id => "37"
      assigns[:phone].should equal(@mock_phone)
    end
        
  end

  describe "POST create" do
    
    describe "with valid params" do
      it "assigns a newly created phone as @phone" do
        Phone.should_receive(:new).with({'these' => 'params'}).and_return(mock_phone(:save => true))
        post :create, :phone => {:these => 'params'}
        assigns[:phone].should equal(@mock_phone)
      end

      it "redirects to the created phone" do
        Phone.stub!(:new).and_return(mock_phone(:save => true))
        post :create, :phone => {}
        response.should redirect_to(edit_phone_url(@mock_phone))
      end
    end
    
    describe "with invalid params" do
      it "assigns a newly created but unsaved phone as @phone" do
        Phone.stub!(:new).with({'these' => 'params'}).and_return(mock_phone(:save => false))
        post :create, :phone => {:these => 'params'}
        assigns[:phone].should equal(@mock_phone)
      end

      it "re-renders the 'new' template" do
        Phone.stub!(:new).and_return(mock_phone(:save => false))
        post :create, :phone => {}
        response.should render_template('new')
      end
    end
    
  end

  describe "PUT update" do
    
    describe "with valid params" do
      it "updates the requested phone if the phone's comment changed." do
        Phone.should_receive(:find).with("37").and_return(mock_phone)
        @mock_phone.should_receive(:comment).and_return('old_comment')
        @mock_phone.should_receive(:update_attributes).with({'comment' => 'new_comment'})
        put :update, :id => "37", :phone => {:comment => 'new_comment'}
      end

      it "updates the requested phone if the phone's phone_category changed." do
        Phone.should_receive(:find).with("37").and_return(mock_phone)
        @mock_phone.should_receive(:comment)
        @mock_phone.should_receive(:update_attributes).with('phone_category_id' => 2)
        put :update, :id => "37", :phone => {:phone_category_id => 2}
      end

      it "redirects to master relations" do
        Phone.stub!(:find).and_return(mock_phone(:update_attributes => true))
        @mock_phone.should_receive(:comment)
         @mock_phone.should_receive(:phone_category_id)
       put :update, :id => "1", :phone => {}
        response.should redirect_to(:controller => 'master_relations', :action => 'save_primaries')
      end
      it "updates increases the count of unsent changes." do
        Phone.should_receive(:find).with("37").and_return(mock_phone)
        @mock_phone.should_receive(:comment).and_return('old_comment')
        @mock_phone.should_receive(:update_attributes).with({'comment' => 'new_comment'})
        put :update, :id => "37", :phone => {:comment => 'new_comment'}
      end
    end
    
    describe "with invalid params" do
      it "updates the requested phone" do
        Phone.should_receive(:find).with("37").and_return(mock_phone)
        @mock_phone.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :phone => {:these => 'params'}
      end

      it "assigns the phone as @phone" do
        Phone.stub!(:find).and_return(mock_phone(:update_attributes => false))
        @mock_phone.should_receive(:comment).at_least(1).and_return('old_comment')
        put :update, :id => "1", :phone => {}
        assigns[:phone].should equal(@mock_phone)
      end

      it "re-renders the 'edit' template" do
        Phone.stub!(:find).and_return(mock_phone(:update_attributes => false))
        @mock_phone.should_receive(:comment).at_least(1).and_return('old_comment')
        put :update, :id => "1", :phone => {}
        response.should redirect_to :controller => 'phones', :action => 'edit', :id => @mock_phone.id
      end
    end
    
  end

  describe "DELETE destroy" do
    it "destroys the requested phone" do
      Phone.should_receive(:find).with("37", :include => 'master_relations').and_return(mock_phone)
      @mock_phone.should_receive(:master_relations).and_return(nil)
      @mock_phone.should_receive(:destroy)
      delete :destroy, :id => "37"
    end
  
    it "redirects to the search url" do
      Phone.stub!(:find).and_return(mock_phone(:destroy => true))
      @mock_phone.should_receive(:master_relations).and_return(nil)
      delete :destroy, :id => "1"
      response.should redirect_to(search_url)
    end
  end

end

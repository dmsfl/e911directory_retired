require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe CountiesController do
      fixtures :roles, :users
  before do
    login_as :administrator_user
  end

  def mock_county(stubs={})
    @mock_county ||= mock_model(County, stubs)
  end
  
  describe "GET index" do

    it "exposes all counties as @counties" do
      County.should_receive(:find).with(:all).and_return([mock_county])
      get :index
      assigns[:counties].should == [mock_county]
    end

    describe "with mime type of xml" do
  
      it "renders all counties as xml" do
        County.should_receive(:find).with(:all).and_return(counties = mock("Array of Counties"))
        counties.should_receive(:to_xml).and_return("generated XML")
        get :index, :format => 'xml'
        response.body.should == "generated XML"
      end
    
    end

  end

  describe "GET show" do

    it "exposes the requested county as @county" do
      County.should_receive(:find).with("37").and_return(mock_county)
      get :show, :id => "37"
      assigns[:county].should equal(mock_county)
    end
    
    describe "with mime type of xml" do

      it "renders the requested county as xml" do
        County.should_receive(:find).with("37").and_return(mock_county)
        mock_county.should_receive(:to_xml).and_return("generated XML")
        get :show, :id => "37", :format => 'xml'
        response.body.should == "generated XML"
      end

    end
    
  end

  describe "GET new" do
  
    it "exposes a new county as @county" do
      County.should_receive(:new).and_return(mock_county)
      get :new
      assigns[:county].should equal(mock_county)
    end

  end

  describe "GET edit" do
  
    it "exposes the requested county as @county" do
      County.should_receive(:find).with("37").and_return(mock_county)
      get :edit, :id => "37"
      assigns[:county].should equal(mock_county)
    end

  end

  describe "POST create" do

    describe "with valid params" do
      
      it "exposes a newly created county as @county" do
        County.should_receive(:new).with({'these' => 'params'}).and_return(mock_county(:save => true))
        post :create, :county => {:these => 'params'}
        assigns(:county).should equal(mock_county)
      end

      it "redirects to the created county" do
        County.stub!(:new).and_return(mock_county(:save => true))
        post :create, :county => {}
        response.should redirect_to(county_url(mock_county))
      end
      
    end
    
    describe "with invalid params" do

      it "exposes a newly created but unsaved county as @county" do
        County.stub!(:new).with({'these' => 'params'}).and_return(mock_county(:save => false))
        post :create, :county => {:these => 'params'}
        assigns(:county).should equal(mock_county)
      end

      it "re-renders the 'new' template" do
        County.stub!(:new).and_return(mock_county(:save => false))
        post :create, :county => {}
        response.should render_template('new')
      end
      
    end
    
  end

  describe "PUT udpate" do

    describe "with valid params" do

      it "updates the requested county" do
        County.should_receive(:find).with("37").and_return(mock_county)
        mock_county.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :county => {:these => 'params'}
      end

      it "exposes the requested county as @county" do
        County.stub!(:find).and_return(mock_county(:update_attributes => true))
        put :update, :id => "1"
        assigns(:county).should equal(mock_county)
      end

      it "redirects to the county" do
        County.stub!(:find).and_return(mock_county(:update_attributes => true))
        put :update, :id => "1"
        response.should redirect_to(county_url(mock_county))
      end

    end
    
    describe "with invalid params" do

      it "updates the requested county" do
        County.should_receive(:find).with("37").and_return(mock_county)
        mock_county.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :county => {:these => 'params'}
      end

      it "exposes the county as @county" do
        County.stub!(:find).and_return(mock_county(:update_attributes => false))
        put :update, :id => "1"
        assigns(:county).should equal(mock_county)
      end

      it "re-renders the 'edit' template" do
        County.stub!(:find).and_return(mock_county(:update_attributes => false))
        put :update, :id => "1"
        response.should render_template('edit')
      end

    end

  end

  describe "DELETE destroy" do

    it "destroys the requested county" do
      County.should_receive(:find).with("37").and_return(mock_county)
      mock_county.should_receive(:destroy)
      delete :destroy, :id => "37"
    end
  
    it "redirects to the counties list" do
      County.stub!(:find).and_return(mock_county(:destroy => true))
      delete :destroy, :id => "1"
      response.should redirect_to(counties_url)
    end

  end

end

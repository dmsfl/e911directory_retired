require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe EmployeesController do
      fixtures :roles, :users
  before do
    login_as :administrator_user
  end

  describe "GET division_employees" do
    it "should assign all employees from same division as params[:id]" do
      company = Sham.company
      e1 = Employee.make(:division_name => company)
      e2 = Employee.make(:division_name => company)
      e3 = Employee.make
      get :division_employees, :id => e1.id
      assigns[:division_employees].should have(2).records
    end
    it "should show an error flash if no employees exist" do
      assigns[:division_employees] = []
      get :division_employees, :id => 1
      flash[:error].should_not be_nil
    end
  end

  describe "POST search" do
    it "receives paramaters in an employee hash" do
      emp = mock_model(Employee, :to_xml => "<a>b</a>")
      Employee.stub!(:find => [emp])
      post :search, :employee => {:first_name => 'bob'}, :format => 'xml'
      assigns[:employees].should == [emp]
    end
    #it "returns an xml array" do
    #  emp = mock_model(Employee, :to_xml => "<a />")
    #  Employee.stub!(:find, [emp])
    #  post :search, :employee => {:first_name => 'bob'}, :format => 'xml'
    #  response.body.should =~ '<a />'
    #end
  end

end

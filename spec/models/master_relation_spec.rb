require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe MasterRelation do
  before(:each) do
    @valid_attributes = {
      :phone_id => 1,
      :location_id => 1,
      :employee_id => 1,
      :primary_employee_location => false,
      :primary_phone_location => false,
      :primary_employee_for_phone => false,
      :primary_phone_for_employee => false
    }
  end

  it "should create a new instance given valid attributes" do
    MasterRelation.create!(@valid_attributes)
  end
end

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AuditE911 do
  
  
  #The application defines auditables in the application controller, so I define them in the
  #following includes for testing purposes
  before :all do
    class AuditE911
      attr_accessible :action, :user_id, :user_type, :changes
    end
  end

  before do
    @valid_attributes = {
      :auditable_id => 1,
      :auditable_type => 'Phone',
      :action => 'update',
      :user_id => 1
    }
  end
  after do
    @audit.delete if @audit
  end
  
  it "should create a new instance given valid attributes" do
    AuditE911.create!(@valid_attributes)
  end

  describe 'should return friendly descriptions' do
    
    describe 'for phone' do
      before do
        @valid_attributes.deep_merge!({:auditable_type => 'Phone'})
      end
      
      it 'creations' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({:action => 'create'}))
        @audit.change_description.should == 'Create a new phone number'
      end
      
      it 'updates'do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({:action => 'update'}))
        @audit.change_description.should == 'Change the phone properties'
      end
      
      it 'deletions' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'destroy'
        }))
        @audit.change_description.should == 'Delete a phone number'
      end
    end
    
    describe 'for employee' do
      before do
        @valid_attributes.deep_merge!({:auditable_type => 'Employee'})
      end
      
      it 'updates' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({:action => 'update' }))
        @audit.change_description.should == 'Change an employee'
      end
      
      it 'creations' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({:action => 'create' }))
        @audit.change_description.should == 'Add a new employee'
      end
      
      it 'deletions' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({:action => 'destroy' }))
        @audit.change_description.should == 'Remove an employee'
      end
    end
    describe 'for master relationship' do
      before do
        @valid_attributes.deep_merge!({:auditable_type => 'MasterRelation'})
      end
      
      it 'creations based on employee/locations' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'create'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'employee_id', :old_value => nil, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'location_id', :old_value => 1, :new_value => 1})
        @audit.should_receive(:revision).at_least(1).and_return(MasterRelation.new({'employee_id' => 1, 'location_id' => 1}))
        @audit.change_description.should == 'Add an employee to a location'
      end
      
      it 'creations based on phone/locations' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'create'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'phone_id',    :old_value => nil, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'location_id', :old_value => nil, :new_value => 1})
        @audit.should_receive(:revision).at_least(1).and_return(MasterRelation.new({'phone_id' => 1, 'location_id' => 1}))
        @audit.change_description.should == 'Add a phone number to a location'
      end

      it 'creations based on employee/phones' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'create', 
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'employee_id', :old_value => nil, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'phone_id', :old_value => nil, :new_value => 1})
        @audit.should_receive(:revision).at_least(1).and_return(MasterRelation.new({'employee_id' => 1, 'phone_id' => 1}))
        @audit.change_description.should == 'Add a phone number to an employee'
      end
      
      it 'updates where a primary location is assignged to an employee and the primary location was not set for that relation' do
        #First time a primary location is set.
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
        @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => '1', :new_value => '1'})
        @audit.change_description.should == "Set the listed location to the employee's work location address."
      end
      
      it 'updates where a primary location is assignged to an employee and the primary location was already set for that relation' do
        #Primary location was set to something else before.
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => nil, :new_value => '1'})
        @audit.change_description.should == "Set the listed location to the employee's work location address."
      end

      it 'updates where a primary location is removed from an employee and the primary location was already set for that relation' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => '1', :new_value => nil})
        @audit.change_description.should == "Remove the listed location from the employee's work location address."
      end
      it 'updates where a primary location is removed from an employee and the primary location was not previously set for that relation' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => nil, :new_value => nil})
        @audit.change_description.should == "Remove the listed location from the employee's work location address."
      end

      it 'updates where the 411 phone number is set for an employee and it was not previously set for that relation' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => '1', :new_value => '1'})
        @audit.change_description.should == "Set the listed phone number, to the employee's 411 phone number."
      end
      it 'updates where the 411 phone number is set for an employee and it was previously set for that relation' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => '1', :new_value => '1'})
        @audit.change_description.should == "Set the listed phone number, to the employee's 411 phone number."
      end
      it 'updates where the 411 phone number is removed and it was previously set for that relation' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => '1', :new_value => nil})
        @audit.change_description.should == "Remove the listed phone number from the employee's 411 phone number field."
      end
      it 'updates where the 411 phone number is removed and it was previously not set for that relation' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'update'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => false, :new_value => false})
        @audit.change_description.should == false
      end
      it 'destroys where the used for 411 changes' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'destroy'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'employee_id', :old_value => '1', :new_value => '1'})
          @audit.audit_changes << AuditChange.create({  :field => 'phone_id', :old_value => '1', :new_value => '1'})
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => '1', :new_value => '0'})
        @audit.change_description.should == "Remove the listed phone number from the employee's 411 phone number field."
      end
      
      it 'destroys where the primary location changes' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'destroy'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'employee_id', :old_value => 1, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'location_id', :old_value => 1, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => '1', :new_value => '1'})
        @audit.change_description.should == "Remove the listed location from the employee's work location address."
      end
      it 'destroys where neither the 411 phone nor the location change' do
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'destroy'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'employee_id', :old_value => 1, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'location_id', :old_value => 1, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => false, :new_value => false})
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => false, :new_value => false})
        @audit.change_description.should == false
        @audit = AuditE911.create!(@valid_attributes.deep_merge({
          :action => 'destroy'
        }))
          @audit.audit_changes << AuditChange.create({  :field => 'employee_id', :old_value => 1, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'location_id', :old_value => 1, :new_value => 1})
          @audit.audit_changes << AuditChange.create({  :field => 'primary_employee_location', :old_value => nil, :new_value => nil})
          @audit.audit_changes << AuditChange.create({  :field => 'used_for_411', :old_value => nil, :new_value => nil})
        @audit.change_description.should == false
      end
    end
  end 
end

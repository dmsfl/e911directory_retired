require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PeopleFirstLocationMap do
  before(:each) do
    @valid_attributes = {
      :lookup_address => "value for lookup_address",
      :house_number => "value for house_number",
      :house_number_suffix => "value for house_number_suffix",
      :prefix_directioneal => "value for prefix_directioneal",
      :street_name => "value for street_name",
      :street_suffix => "value for street_suffix",
      :post_directional => "value for post_directional",
      :unit => "value for unit",
      :floor => "value for floor",
      :building => "value for building"
    }
  end

  it "should create a new instance given valid attributes" do
    PeopleFirstLocationMap.create!(@valid_attributes)
  end
end

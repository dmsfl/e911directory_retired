require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe County do
  before(:each) do
    @valid_attributes = {
      :fips_code => 1,
      :name => "value for name",
      :people_first_code => 1
    }
  end

  it "should create a new instance given valid attributes" do
    County.create!(@valid_attributes)
  end
end

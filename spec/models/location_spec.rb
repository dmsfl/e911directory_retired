require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Location do
  fixtures :buildings
  
  before(:each) do
    @valid_attributes = {
      :floor => "value for floor",
      :room => "value for room",
      :building_identifier => "value for building_identifier"
    }
  end

  it "should create a new instance given valid attributes" do
    Location.create!(@valid_attributes)
  end
  
  it 'should always provide a label' do
    l = create_location({:building => nil})
    l.to_label.should == 'Flr 1, Rm a123'
  end
  
  it 'should break lines before the city when generating labels' do
    l = Location.make
    city = l.building.city
    l.to_label.should match /<br \/>#{city}/
  end
  private
  def create_location options
    Location.new({
      :room => 'a123',
      :floor  => '1',
      :building => buildings(:one)      
    }.merge(options))
  end
end

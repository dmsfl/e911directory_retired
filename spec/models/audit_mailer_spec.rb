require File.dirname(__FILE__) + '/../spec_helper'
require 'legacy_migrations'
require 'legacy_migrations/status_report'
describe "AuditMailer" do
  describe "user_friendly_ip_phone_changes" do
    before(:each) do
      Employee.delete_all
      Phone.delete_all
      @status_report = StatusReport.instance
      @ml = MasterRelation.make
      @phone = @ml.phone.to_label
      @table = FasterCSV.parse("voip_name,bldg_num,bldg_num_suffix,street,suffix,post_dir,city,state,zip,room,voip\n"+
                                                 "\"aoeuaoeu, aoeu\",4033,,Esplenade,Way,,Tallahassee,Fl,323013333.0,215.z3,8501113355", :headers => :first_row)
      @status_report.add_operation(:destination => MasterRelation,
                                  :inserts => [@ml],
                                  :source => @table)
    end
    it "sends to the telcom interested parties" do
      @email = AuditMailer.create_user_friendly_ip_phone_changes(@status_report)
      @email.to.should == APP_CONFIG['mail']['telecom_interested_parties']
    end
    it "shows all the updated records" do
      @email = AuditMailer.create_user_friendly_ip_phone_changes(@status_report)
      @email.body.should match /#{Regexp.escape("(850) 111-3355")}/
      #@email.body.should match /#{Regexp.escape(@ml.location.to_label)}/
      #@email.body.should match /#{Regexp.escape(@ml.employee.to_label)}/
    end
    it "shows all of the source records" do
      @email = AuditMailer.create_user_friendly_ip_phone_changes(@status_report)
      #debugger
      @email.body.should match /aoeuaoeu\, aoeu/
    end
  end
end

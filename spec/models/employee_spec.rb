# -*- coding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'
describe Employee do
  fixtures :employees

  describe 'being created' do
    before do
      @employee = nil
      @creating_employee = lambda do
        @employee = create_employee
        violated "#{@employee.errors.full_messages.to_sentence}" if @employee.new_record?
      end
    end

    it 'increments Employee#count' do
      @creating_employee.should change(Employee, :count).by(1)
    end
  end
  
  # Named scopes
  describe "named scope" do
    describe "with_same_division_as" do
      before do
        company = Faker::Company.name
        @e1 = Employee.make({:division_name => company})
        @e2 = Employee.make({:division_name => company})
        @e3 = Employee.make
      end

      it "should return other employees with the same division as given employee (with id)" do
        Employee.with_same_division_as(@e1.id).should have(2).records
      end
      it "should return other employees with the same division as given employee" do
        Employee.with_same_division_as(@e1).should have(2).records
      end
    end
  end
  
  #
  # Validations
  #
  it 'requires email' do
    lambda do
      u = create_employee(:email => nil)
      u.errors.on(:email).should_not be_nil
    end.should_not change(Employee, :count)
  end

  it "requires first_name" do
    lambda do
      u = create_employee(:first_name => nil)
      u.errors.on(:first_name).should_not be_nil
    end.should_not change(Employee, :count)
  end
  it "requires last_name" do
    lambda do
      u = create_employee(:last_name => nil)
      u.errors.on(:last_name).should_not be_nil
    end.should_not change(Employee, :count)
  end
  it "requires people_first_user" do
    lambda do
      u = create_employee(:people_first_user => nil)
      u.errors.on(:people_first_user).should_not be_nil
    end.should_not change(Employee, :count)
  end
  describe 'allows legitimate emails:' do
    ['foo@bar.com', 'foo@newskool-tld.museum', 'foo@twoletter-tld.de', 'foo@nonexistant-tld.qq',
     'r@a.wk', '1234567890-234567890-234567890-234567890-234567890-234567890-234567890-234567890-234567890@gmail.com',
     'hello.-_there@funnychar.com', 'uucp%addr@gmail.com', 'hello+routing-str@gmail.com',
     'domain@can.haz.many.sub.doma.in', 'student.name@university.edu'
    ].each do |email_str|
      it "'#{email_str}'" do
        lambda do
          u = create_employee(:email => email_str)
          u.errors.on(:email).should     be_nil
        end.should change(Employee, :count).by(1)
      end
    end
  end
  describe 'disallows illegitimate emails' do
    ['!!@nobadchars.com', 'foo@no-rep-dots..com', 'foo@badtld.xxx', 'foo@toolongtld.abcdefg',
     'Iñtërnâtiônàlizætiøn@hasnt.happened.to.email', 'need.domain.and.tld@de', "tab\t", "newline\n",
     'r@.wk', '1234567890-234567890-234567890-234567890-234567890-234567890-234567890-234567890-234567890@gmail2.com',
     # these are technically allowed but not seen in practice:
     'uucp!addr@gmail.com', 'semicolon;@gmail.com', 'quote"@gmail.com', 'tick\'@gmail.com', 'backtick`@gmail.com', 'space @gmail.com', 'bracket<@gmail.com', 'bracket>@gmail.com'
    ].each do |email_str|
      it "'#{email_str}'" do
        lambda do
          u = create_employee(:email => email_str)
          u.errors.on(:email).should_not be_nil
        end.should_not change(Employee, :count)
      end
    end
  end

  describe 'allows legitimate first names:' do
    ['Andre The Giant (7\'4", 520 lb.) -- has a posse',
     '1234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890',
    ].each do |name_str|
      it "'#{name_str}'" do
        lambda do
          u = create_employee(:first_name => name_str)
          u.errors.on(:first_name).should     be_nil
        end.should change(Employee, :count).by(1)
      end
    end
  end
  describe "disallows illegitimate first names" do
    ["tab\t", "newline\n",
     '1234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_',''
     ].each do |name_str|
      it "'#{name_str}'" do
        lambda do
          u = create_employee(:first_name => name_str)
          u.errors.on(:first_name).should_not be_nil
        end.should_not change(Employee, :count)
      end
    end
  end
  describe 'allows legitimate last names:' do
    ['Andre The Giant (7\'4", 520 lb.) -- has a posse',
     '1234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890',
    ].each do |name_str|
      it "'#{name_str}'" do
        lambda do
          u = create_employee(:last_name => name_str)
          u.errors.on(:last_name).should     be_nil
        end.should change(Employee, :count).by(1)
      end
    end
  end
  describe "disallows illegitimate last names" do
    ["tab\t", "newline\n",
     '1234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_234567890_',''
     ].each do |name_str|
      it "'#{name_str}'" do
        lambda do
          u = create_employee(:last_name => name_str)
          u.errors.on(:last_name).should_not be_nil
        end.should_not change(Employee, :count)
      end
    end
  end
  describe "sorts all queries by last_name, middle_name, and first_name" do
    it 'should sort by last_name' do
      Employee.find(:all, :conditions => "last_name LIKE '%te%'").size.should == 5
      Employee.find(:all, :conditions => "last_name LIKE '%te%'").first.last_name.should == 'telles'
    end
    it 'should sort by last_name, middle_name and first_name' do
      Employee.find(:all, :conditions => "last_name LIKE '%te%'").size.should == 5
      first_emp = Employee.find(:all, :conditions => "last_name LIKE '%te%'").first
      first_emp.last_name = 'telles'
      first_emp.middle_name = 'a'
      first_emp.first_name = 'bernii'
    end
  end

  describe "prints full_names" do
    it "should print a full_name if there's only a first_name" do
      emp = create_employee({
        :last_name => nil,
        :middle_name => nil
      })
      emp.full_name.should == 'Merrit'
    end
  end
  describe "sorted_master_relations" do
    it "should sort by primaries then phone then location" do
      @employee = Employee.make
      @ml1 = MasterRelation.make(:employee => @employee,
                                 :phone => Phone.make(:area_code => '995'),
                                 :primary_employee_location => true)
      @ml2 = MasterRelation.make(:employee => @employee,
                                 :phone => Phone.make(:area_code => '999'),
                                 :primary_employee_location => false,
                                 :primary_phone_for_employee => true)
      @ml3 = MasterRelation.make(:employee => @employee,
                                 :location => Location.make,
                                 :phone => Phone.make(:area_code => '111'),
                                 :primary_employee_location => false,
                                 :primary_phone_for_employee => false)
      @ml4 = MasterRelation.make(:employee => @employee,
                                 :phone => Phone.make(:area_code => '222'),
                                 :location => @ml3.location,
                                 :primary_employee_location => false,
                                 :primary_phone_for_employee => false)
      @employee.sorted_master_relations.should == [@ml1,@ml2,@ml3,@ml4]
    end
  end
protected
  def create_employee(options = {})
    record = Employee.new({ :login => 'quire', 
                        :email => 'quire@example.com', 
                        :password => 'quire69', 
                        :password_confirmation => 'quire69',
                        :last_name => 'tom',
                        :first_name => 'merrit',
                        :people_first_user => '999222',
                        :active_flag => true,     }.merge(options))
    record.save
    record
  end
end

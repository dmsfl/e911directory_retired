require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PeopleFirst do
  before(:each) do
    @valid_attributes = {
      :county_id => 1,
      :location_room => "value for location_room",
      :location_state => "value for location_state",
      :location_campus_name => "value for location_campus_name",
      :location_address => "value for location_address",
      :vacancy_indicator => false,
      :last_name => "value for last_name",
      :first_name => "value for first_name",
      :middle_name => "value for middle_name",
      :four_11_phone => ''
    }
  end

  it "should create a new instance given valid attributes" do
    PeopleFirst.create!(@valid_attributes)
  end
end

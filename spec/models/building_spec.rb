require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Building do
  before(:each) do
    @valid_attributes = {
      :name => "value for name",
      :city => 'Tally',
      :zip_code_5 => '33333'
    }
  end

  it "should create a new instance given valid attributes" do
    Building.create!(@valid_attributes)
  end
end

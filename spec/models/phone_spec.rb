require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Phone do
  before do
    @valid_attributes = {
      :area_code => "850",
      :main_number => "1234567",
      :phone_category_id => 1
    }
    #login_as User.make(:administrator)
  end

  it "should create a new instance given valid attributes" do
    Phone.create!(@valid_attributes)
  end
  
  describe 'fetch 911 employee' do
    it 'should fetch the nine_11_employee through master_relations' do
      p = Phone.make
      e = Employee.make
      
      ml1 = MasterRelation.make(:location_id => 123, :phone_id => 2)
      ml1.should_receive(:employee).twice.and_return(e)
      ml1.primary_phone_location = true
      
      ml2 = MasterRelation.make
      ml2.primary_phone_location = false
      ml2.should_not_receive(:employee)
      
      p.should_receive(:master_relations).and_return([ml1, ml2])
      p.nine_11_employee.should eql(e)
      
    end
    it "should return nil if there's no 911 employee" do
      p = Phone.make
      ml1 = MasterRelation.make(:location_id => 123, :phone_id => 2)
      ml1.primary_phone_location = true
      ml1.should_receive(:employee).and_return(nil)
      
      ml2 = MasterRelation.make
      ml2.primary_phone_location = false
      ml2.should_not_receive(:employee)
      
      p.should_receive(:master_relations).and_return([ml1, ml2])
      p.nine_11_employee.should be_nil
  
    end
  
    it "should raise an error when trying to find an employee and the phone has more than one primary location" do
      p = Phone.make
      ml1 = MasterRelation.make(:location_id => 123, :phone_id => 2, :employee => Employee.make)
      ml1.primary_phone_location = true
      
      ml2 = MasterRelation.make(:location_id => 124, :phone_id => 2, :employee => Employee.make)
      ml2.primary_phone_location = true
      
      p.should_receive(:master_relations).and_return([ml1, ml2])
      lambda {p.nine_11_employee}.should raise_error(AuditHelper::TooMany911LocationsError)
  
    end
  end
  
  describe 'fetch 911 location' do
    it 'should fetch the nine_11_location through master_relations' do
      p = Phone.make
      l = Location.make
      
      ml1 = MasterRelation.make(:location_id => 123, :phone_id => 2)
      ml1.should_receive(:location).twice.and_return(l)
      ml1.primary_phone_location = true
      
      ml2 = MasterRelation.make
      ml2.should_not_receive(:location)
      
      p.should_receive(:master_relations).and_return([ml1, ml2])
      p.nine_11_location.should eql(l)
      
    end
    
    it "should return nil if there's no 911 location" do
      p = Phone.make
      ml1 = MasterRelation.make(:location_id => 123, :phone_id => 2)
      ml1.primary_phone_location = true
      ml1.should_receive(:location).and_return(nil)
      
      ml2 = MasterRelation.make
      ml2.primary_phone_location = false
      ml2.should_not_receive(:location)
      
      p.should_receive(:master_relations).and_return([ml1, ml2])
      p.nine_11_location.should be_nil
  
    end
  
    it "should raise an error when trying to find a location and the phone has more than one primary location" do
      p = Phone.make
      ml1 = MasterRelation.make(:location => Location.make, :phone_id => 2)
      ml1.primary_phone_location = true
      
      ml2 = MasterRelation.make(:location => Location.make, :phone_id => 2)
      ml2.primary_phone_location = true
      
      p.should_receive(:master_relations).and_return([ml1, ml2])
      lambda {p.nine_11_location}.should raise_error(AuditHelper::TooMany911LocationsError)
  
    end
  end
  describe 'sorted_master_relations' do
    it "should sort by primary_employeee, then primary_location, then employee, then location" do
      @phone = Phone.make
      @ml1 = MasterRelation.make(:phone => @phone,
                                 :employee => Employee.make(:last_name => 'zz'),
                                 :primary_employee_for_phone => true)
      @ml2 = MasterRelation.make(:phone => @phone,
                                 :employee => Employee.make(:last_name => 'zz'),
                                 :primary_employee_for_phone => false,
                                 :primary_phone_location => true)
      @ml3 = MasterRelation.make(:phone => @phone,
                                 :employee => Employee.make(:last_name => 'aa'),
                                 :primary_employee_for_phone => false,
                                 :primary_phone_location => false)
      @ml4 = MasterRelation.make(:phone => @phone,
                                 :employee => Employee.make(:last_name => 'ab'),
                                 :primary_employee_for_phone => false,
                                 :primary_phone_location => false)
      @phone.sorted_master_relations.should == [@ml1,@ml2,@ml3,@ml4]
    end
  end
  
end

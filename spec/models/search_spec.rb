require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Search do
  fixtures :phone_categories, :campuses, :counties,  :buildings, :employees, :locations, :phones, :master_relations
  before :each do
    @s = Search.new
  end
  
  def valid_attributes
    {
      :first_name => "Bob",
      :last_name => "Smith",
      :middle_name => "M",
      :area_code => "850",
      :main_number => "1234567",
      :phone_category_id => PhoneCategory.find_by_name('Voice').id,
      :vacant => "true",
      :floor => "1",
      :room => "2",
      :city => "Tallahassee",
      :zip_code => "32301",
      :email => "bob.smith@dms.myflorida.com",
      :contractor_flag => "false",
      :active_flag => "true",
      :full_street_address => "4350",
      :building => "CCOC",

      #Kept for completeness, but not used in search (see commented section below)
      :county => "Leon",
      :campus => "CCOC"
    }
  end
    
  it "should create a new instance given valid attributes" do
    Search.new()
  end
  

  
  it 'should return the correct conditions when sent location_conditions' do
    @s.location_conditions({:building => 'pepper'}).should == { :include => ['building'], :conditions => ["buildings.name LIKE ?","%pepper%"]}
    
    #campus is excluded from search params
    @s.location_conditions({:building => 'pepper', :campus => 'ccoc'}).should == { :include => ['building'], :conditions => ["buildings.name LIKE ?","%pepper%"]}
  end
  
  it "should return one correct results with a variety of parameters" do
    results = @s.find_locations({ :building => "one"})
    
    #Fixtures define three locations for building 'one_name'
    results.should have(3).records
  end
  
  it "should handle symbols or string as the parameter key" do
    results = @s.find_locations({'room' => '1'})
    results.should have(1).record 
  end
  it "should find locations when more than one parameter is given" do
    results = @s.find_locations({'room' => '2', 'building' => 'one'})
    results.should have(2).records
  end
  
  # ---- EMPLOYEE SEARCH ----
  
  describe "Employee search" do
    it "should find employees through their last names" do
      results = @s.find_employees({:last_name => 'smith'})
      results.should have(1).record
      results.first.should == employees(:old_password_holder) 
    end 
  end
  
  describe "with the contractor flag should return the correct results" do
    it "should return the correct employees when the flag is sent by itself" do
      @s.find_employees(:contractor_flag => 1).size.should == 2
      @s.find_employees(:contractor_flag => '1').size.should == 2
      b = @s.find_employees(:contractor_flag => 0)
      b.should have(10).record

    end
    it "should return the ccorrect employees if it receives mulitple parameters" do
     result = @s.find_employees(:last_name => 'te', :contractor_flag => '1', :email => 'rda')
     result.should have(1).record
     result.first.should == employees(:sort_test1)
    
     result = @s.find_employees(:last_name => 'te', :contractor_flag => '0', :email => 'rda')
     result.should have(:no).records
    end
    
  end
  
  describe "with active_flag option return the correct results" do
    it "should return the correct employees when the flag is sent by itself" do
      @s.find_employees(:active_flag => 1).should have(5).records
      @s.find_employees(:active_flag => '1').should have(5).records
      @s.find_employees(:active_flag => 0).should have(7).records
    end
    
    it "should return the ccorrect employees if it receives mulitple parameters" do
     result = @s.find_employees(:active_flag => '0', :email => 'rda')
     result.should have(1).record
     result.first.should == employees(:sort_test1)
     
     result = @s.find_employees(:last_name => 'te', :active_flag => '1', :email => 'rda')
     result.should have(:no).records
    end
    
  end  

  # ---- PHONE SEARCH ----

  describe "should remove all non-numbers from the searched phone," do
    it "including the main number field" do
      @s.find_phones(:main_number => '!@<>PYIJ11 11#$111IAO.').should have(1).record
    end
    it "including the area code field" do
      @s.find_phones(:area_code => '!@<>PYIJ111IAO.').should have(1).record
    end    
  end
  
  it "should narrow a search by phone category id" do
    @s.find_phones(:phone_category_id => phone_categories(:voice).id).should have(2).records
  end
  
  it "should try to find any active employees that still use the phone number before including the # as a 'vacant' one" do
    @s.find_phones(:vacant => 1).should include(phones(:conference)) 
    @s.find_phones(:vacant => 1).should_not include(phones(:fax), phones(:voice))
    @s.find_phones(:vacant => 0).should include(phones(:fax), phones(:voice)) 
    @s.find_phones(:vacant => 0).should_not include(phones(:conference))
  end
  it "should accept additional parameters along with 'vacant' flag" do
    @s.find_phones(:vacant => 1, :area_code => '333').should include(phones(:conference)) 
    @s.find_phones(:vacant => 1, :area_code => '222').should_not include(phones(:conference)) 
    
  end
  
  
end

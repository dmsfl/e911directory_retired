class HaveALineWithMatcher
  # HaveALineWithMatcher is a simple rspec matcher that tests whether strings exist in a file
  #
  # = Usage
  #
  # 1. place this file in your spec/support directory.
  # 
  # 2. use it in your specs:
  #
  #   "/path/to/file".should have_a_line_with "some string"
  #   "/path/to/file".should have_a_line_with /some regular expression/
  #
  def initialize(reg_exp)
    if reg_exp.instance_of? String
      @reg_exp = Regexp.new(Regexp.escape(reg_exp))
    else
      @reg_exp = reg_exp
    end
  end

  def matches?(target_file)
    @target_file = target_file
    File.open(@target_file).each do |line|
      return true if line =~ @reg_exp
    end
    false
  end

  def failure_message
    "expected to find #{@reg_exp.source} in #{@target_file}, but didn't"
  end
  
  def negative_failure_message
    "expected not to find #{@reg_exp.source} in #{@target_file}, but it did"
  end

end

def have_a_line_with(expected)
  HaveALineWithMatcher.new(expected)
end

ENV["RAILS_ENV"] ||= 'test'
require 'rubygems'
require 'spork'

Spork.prefork do
  require File.dirname(__FILE__) + "/../config/environment" #unless defined?(RAILS_ROOT)
  require 'spec/autorun'
  require 'spec/rails'
  # Loading more in this block will cause your tests to run faster. However, 
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.
  
end

Spork.each_run do
  # This code will be run each time you run your specs.
  
  Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each {|f| require f}
  require File.expand_path(File.dirname(__FILE__) + "/blueprints")

    Spec::Runner.configure do |config|

      # Be sure to include AuthenticatedTestHelper in spec/spec_helper.rb instead.
      include AuthenticatedTestHelper


      config.use_transactional_fixtures = true
      config.use_instantiated_fixtures  = false
      config.fixture_path = RAILS_ROOT + '/spec/fixtures/'
      
      #Reset Sham so that machinist works correctly
      config.before(:all)    { Sham.reset(:before_all)  }
      config.before(:each)   do 
        Sham.reset(:before_each) 
        Role.make(:name => 'Read Only')
      end
      shared_examples_for "all associations tables" do 
        it "should have an associations table" do
          response.should have_tag("table.associations") 
        end
        it "should divide location and phone number sections" do
          response.should have_tag("td.divider, th.divider")
        end
        it "should have nice association headers" do
          response.should have_tag("tr.main_table_headers th", /location/i)
          response.should have_tag("tr.main_table_headers th", /employee|phone/i)
          response.should have_tag("tr.minor_table_headers th", /main/i)
        end
        it "should have images for yes and no" do
          response.should have_tag("img[alt=Yes]",1)
          response.should have_tag("img[alt=No]")
        end
      end
    end


end

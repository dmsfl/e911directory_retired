# Use this file to easily define all of your cron jobs.
# Learn more: http://github.com/javan/whenever
#
require 'yaml'

RAILS_ROOT = File.dirname(__FILE__)
current_path = RAILS_ROOT.gsub(/\/releases\/.*$/, '/current')

raw_config = File.read(current_path + "/config/config.yml")
APP_CONFIG = YAML.load(raw_config)['production']

load_profile = ". /export/home/webmgr/.profile"
# for some reason the env function isn't generating the correct characters for cron jobs
# and since our root doesn't have the correct env variables, we have to do this manually
rake_command = "#{load_profile} && cd #{current_path} && /opt/ruby-enterprise/bin/rake RAILS_ENV=production"



every 1.day, :at => APP_CONFIG['mail']['people_first_send_time'] do
  #rake requires special env location (see extra_paths for more explanation)
  command "#{rake_command} e911:send_user_changes_to_people_first"
end


every 1.day, :at => '5:30 pm' do
  command "#{rake_command} e911:update_employees_from_people_first"
end


every 1.day, :at => '7:30 pm' do
  command "#{rake_command} e911:send_911_daily_update"
end

every 1.day, :at => '7:40 pm' do
  command "#{rake_command} e911:update_directory_from_voip"
end

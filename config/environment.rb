# Be sure to restart your server when you modify this file

# Uncomment below to force Rails into production mode when
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.16' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

#Load application and environment specific constants
require 'yaml'
raw_config = File.read(RAILS_ROOT + "/config/config.yml")
APP_CONFIG = YAML.load(raw_config)[RAILS_ENV]

Rails::Initializer.run do |config|

  config.gem 'mislav-will_paginate', :version => '~> 2.3.8', :lib => 'will_paginate', :source => 'http://gems.github.com'
  config.gem 'ruport', :version => '1.6.1' # also requires 'ruport-util'
  config.gem 'mime-types', :version => '1.16', :lib => false
  config.gem 'javan-whenever', :version => '0.3.0', :lib => false, :source => 'http://gems.github.com'
  config.gem 'mailfactory', :version => '1.4.0'
  config.gem 'fastercsv', :version => '1.2.3'
  config.gem 'net-ssh', :version => '2.0.11', :lib => false
  config.gem 'net-sftp', :version => '2.0.2', :lib => false
  config.gem 'rails_sql_views', :version => '0.7.0'
  config.gem 'legacy_migrations', :version => '0.3.7'
  config.gem 'rubyzip', :version => '0.9.1', :lib => false
  config.action_controller.session = {
    :session_key => '_test_session'
  }

  config.action_controller.session_store = :active_record_store

  config.active_record.observers = :user_observer

  config.time_zone = 'Eastern Time (US & Canada)'

  config.action_mailer.smtp_settings = {
    :address  => APP_CONFIG['mail']['address'],
    :port  => APP_CONFIG['mail']['port'],
    :domain => APP_CONFIG['mail']['domain']
  }

  config.cache_store= :file_store, "#{Rails.root}/tmp/cache"

end
require 'ruport/util'

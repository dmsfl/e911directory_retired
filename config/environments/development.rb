config.cache_classes = false
config.whiny_nils = true

# Show full error reports and disable caching
config.action_controller.consider_all_requests_local = true
config.action_view.debug_rjs                         = true
config.action_controller.perform_caching             = false
#ActiveRecord::Base.logger = Logger.new(STDOUT)
#ActionController::Base.logger = Logger.new(STDOUT)
config.log_level = :debug
# Don't care if the mailer can't send
config.action_mailer.raise_delivery_errors = true



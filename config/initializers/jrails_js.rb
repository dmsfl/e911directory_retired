
# This sets up the correct javascript defaults for the jrails plugin to work.
ActionView::Helpers::AssetTagHelper::JAVASCRIPT_DEFAULT_SOURCES = ['vendor/jquery','vendor/jquery-ui','vendor/jrails', 'vendor/form', 'vendor/corner']
ActionView::Helpers::AssetTagHelper::reset_javascript_include_default
ActionController::Routing::Routes.draw do |map|
  map.root :controller => "welcome"
  map.forgot_password '/forgot_password',       :controller => 'users',    :action => 'forgot_password'
  map.reset_password  '/reset_password/:id',    :controller => 'users',    :action => 'reset_password'  
  map.change_password '/change_password/:id',   :controller => "users",    :action => 'change_password'
  map.logout          '/logout',                :controller => 'sessions', :action => 'destroy'
  map.login           '/login',                 :controller => 'sessions', :action => 'new'
  map.search          '/search/:action',                :controller => "search"
  map.activate        '/activate/:activation_code', :controller => 'users', :action => 'activate', :activation_code => nil
  map.employees_search_results '/employees/search_results', :controller => "employees", :action => "search_results"
  map.locations_search_results '/locations/search_results', :controller => "locations", :action => "search_results"
  map.phones_search_results    '/phones/search_results',    :controller => "phones",    :action => "search_results"
  map.save_primaries  '/save_primaries', :controller => 'master_relations', :action => 'save_primaries'  
  map.edit_component '/edit_component/:id/:component', :controller => 'master_relations', :action => 'edit_component'
  map.delete_component '/delete_component/:id/:component', :controller => 'master_relations', :action => 'delete_component'
  map.add_component '/add_component/:id/:component', :controller => 'master_relations', :action => 'add_component'
  map.unsent_changes  '/audits/unsent_changes', :controller => 'audits', :action => 'unsent_changes'
  map.send_changes    '/audits/send_changes',   :controller => 'audits', :action => 'send_changes'
  map.reports         '/reports/:action',               :controller => 'reports'
  map.reports_search_results '/reports/search_results', :controller => 'reports', :action => 'search_results'
  map.division_employees '/division_employees/:id', :controller => 'employees', :action => 'division_employees'
  map.incorrect_information '/incorrect_information', :controller => 'feedbacks', :action => 'incorrect_information'
  map.add_employee_search_results '/add_employee_search_results', :controller => 'master_relations', :action => 'add_employee_search_results'
  map.add_location_search_results '/add_location_search_results', :controller => 'master_relations', :action => 'add_location_search_results'
  map.add_phone_search_results '/add_phone_search_results', :controller => 'master_relations', :action => 'add_phone_search_results'
  map.delete_phone    '/phones/:id/delete', :controller => 'phones', :action => 'destroy'
  map.connect         '/search/find_employees', :controller => 'search',    :action => 'find_employees'
  map.connect         '/users/search_results_for_user_add',  :controller => 'users', :action => 'search_results_for_user_add'
  map.connect         '/users/add_employee/:id',:controller => 'users',     :action => 'add_employee'
  map.connect         '/employees/last_names',  :controller => "employees", :action => "last_names"
  map.connect         '/employees/first_names', :controller => "employees", :action => "first_names"
  map.connect         '/counties/names',        :controller => "counties",  :action => "names"
  map.connect         '/buildings/cities',      :controller => "buildings", :action => "cities"  
  map.connect         '/buildings/zip_codes',   :controller => "buildings", :action => "zip_codes"  
  map.connect         '/locations_manager',     :controller => 'locations_manager', :action => 'index'
  map.connect         '/employees/search.:format',     :controller => 'employees', :action => 'search'
  map.full_data_report '/reports/full_data_report.:format',     :controller => 'reports', :action => 'full_data_report'
  map.resources :locations
  map.resources :phones
  map.resources :campuses, :singular => :campus
  map.resources :buildings
  map.resources :counties
  map.resources :employees
  map.resources :master_relations
  map.resources :users, :except => [:new], :member => {:delete => :get}
  map.resource :session
  map.connect '/:controller/:action/:id'
end

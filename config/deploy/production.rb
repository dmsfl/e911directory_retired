set :local_repository,  "ssh://webmgr@sun113z1.dms.state.fl.us/export/home/webmgr/rubyapps/repositories/E911.git"
set :location, "sun113z1.dms.state.fl.us"
role :app, location
role :web, location
role :db,  location, :primary => true

current_path = File.expand_path(File.dirname(__FILE__).gsub(/\/releases\/.*$/, '/current'))
load_profile = ". /export/home/webmgr/.profile"
# for some reason the env function isn't generating the correct characters for cron jobs
# and since our root doesn't have the correct env variables, we have to do this manually
set :cron_update, "#{load_profile} && cd /opt/common/html/railsapps/directory/current && RAILS_ENV=production && export RAILS_ENV && /opt/ruby-enterprise/bin/whenever --update-crontab #{application}"


require 'capistrano/ext/multistage'
RAILS_ROOT = "/opt/common/html/railsapps/directory/current"

set :application, "directory.dms.myflorida.com"
set :user, "webmgr"
set :use_sudo, false

set :repository,  "/export/home/webmgr/rubyapps/repositories/E911.git"
set :deploy_to, "/opt/common/html/railsapps/directory"
set :git_shallow_clone, 1
set :scm, :git
set :local_git, "/usr/bin/git"
set :git, "/usr/local/bin/git" 
set :git_ls_remote_options, "--upload-pack=/usr/local/bin/git-upload-pack" 

set :git_enable_submodules, 1         # Make sure git submodules are populated

set :port, 22                      # The port you've setup in the SSH setup section

namespace :deploy do
#  desc "Push master branch code to origin"
#  task :push do
#    `git push origin master`
#  end
#  desc "Set the branch in Capistrano to current commit"
#  task :set_branch do
#    commit = `git show --format=%H`.split("\n")[0]
#    set :branch, commit
#  end
  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

  desc "Make symlink for database.yml" 
  task :symlink_dbyaml do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml" 
  end

  desc "Create empty database.yml in shared path" 
  task :create_dbyaml do
    run "mkdir -p #{shared_path}/config" 
    put '', "#{shared_path}/config/database.yml" 
  end
  
  desc "Update the crontab file"
  task :update_crontab, :roles => :db do
   #run cron_update
  end

end

after 'deploy:setup', 'deploy:create_dbyaml'
after 'deploy:update_code', 'deploy:symlink_dbyaml', 'deploy:update_crontab'

after "deploy", "deploy:cleanup"

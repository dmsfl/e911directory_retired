
// Add Array.indexOf functionality to IE
if(!Array.indexOf){
    Array.prototype.indexOf = function(obj){
        for(var i=0; i<this.length; i++){
            if(this[i]==obj){
                return i;
            }
        }
        return -1;
    }
}
/* 
 * IE doesn't submit the value of an image input field  when 
 * it is clicked, so we have to do it manually.
 */

function fixIEImageSubmit(){
	$('input[type=image]').live('click', function(event){
		var fieldId = $(this).attr('name');
		var fieldValue = $(this).val();
		$(this).
		  closest('form').
		  append('<input type=\'hidden\' name=\''+fieldId+'\' id=\''+fieldId+'\' value=\''+fieldValue+'\'/>');
	});
}

function simpleAjaxDialogInitializer(){
  $('.simple_ajax_dialog').live('click', function(event){
	  event.preventDefault();
		url = $(this).attr('href');
    $.get(url, function(data, textStatus){
      simpleDialogFromAjaxResponse(data);
    });
  });
$('a.mid_ajax_dialog').live('click', function(event){
	  event.preventDefault();
		url = $(this).attr('href');
    $.get(url, function(data, textStatus){
      simpleDialogFromAjaxResponse(data, {'width' : 530} );
    });
  });

	$('a.wide_ajax_dialog').live('click', function(event){
	  event.preventDefault();
		url = $(this).attr('href');
    $.get(url, function(data, textStatus){
      simpleDialogFromAjaxResponse(data, {'width' : 780} );
    });
  });
};

function autocompleteInitializer(){
  $('.autocomplete').each( function(){
		var classes = $(this).attr('class').split(' ');
    var action = classes[classes.indexOf('autocomplete')+1].replace(/\-/,'/');
    classes[2] == 'omit_users' ? omitUsers = '?omit_users=true' : omitUsers = '';
		$(this).autocomplete('/'+action+omitUsers,{ 'delay':200, 'cacheLength': 100, 
      'max': 100 ,
      'scrollHeight': 100,
      'selectFirst': false});
  });
  $('.autocomplete').closest('form').live('click', function(e) {
      $(this).find('.autocomplete').unautocomplete();
      autocompleteInitializer();
      });
};

function confirmActionDialogInitializer(){
  $('a.confirm_action_dialog').live('click', function(event){
    event.preventDefault();
		url = $(this).attr('href');
		
		if( $(this).attr('title').length == 0){
		   confirmString = 'Are you sure?';
		} else {
		   confirmString = 'Are you sure you want to '+$(this).attr('title')+'?';
		}
		$('<p>'+confirmString+'</p>').dialog({
		  'modal': true,
		  'title': 'Confirm:',
		  'buttons': {
		    'No': function(){$(this).dialog('close'); return false;},
		    'Yes': function(){ window.location = url;}
		  }
		});
    return false;
  });
};

function ajaxFormDialogInitializer(){
  $("form.ajax_form_dialog input[type=submit]").live('click',function(event) { 
    $(this).closest('form').ajaxSubmit({
      'success': function(data, textStatus){
        simpleDialogFromAjaxResponse(data);
      }
    });
    return false;
  }); 
};

function replaceResultsWithData(rawData){
    data = $(rawData).find('#bd').attr('id','');
	$('#search_results').empty().append(data);
};

function replaceCustomResultsWithData(data_selector,replace_selector,data){
  $(replace_selector).empty().append($(data).find(data_selector));
};

function replaceResultsInitializer(){
  $("form.replace_results input[type=submit]").live('click',function(event) {
		setLoadingPicture('#search_results')
    event.preventDefault();
	$(this).closest('form').ajaxSubmit({
      'success': function(data, textStatus){
        replaceResultsWithData(data);
      }
    });
    return false;
  }); 
};

function addFloorSelector(data){
  if ($('tbody tr', $(data)).size()> 1 ){
    optionValues = null
    optionValues = eval($('#floors', $(data)).val());
	optionTags = $.map(optionValues, function(e){return "<option value=\""+(e || '')+"\">"+e+"</option>";});
	
	$('#search_floor_equals').empty().append(optionTags.join(' '));
  }
}

function setLoadingPicture(des){
	$(des).empty().append("<p  style='text-align:center'><img src='/images/loadingAnimation.gif' alt='loading...'/></p>");
}

function otherPageInteractions(xhr, data){
  // Adds floor selector in 'building/location search and enables fields
  if(xhr.url.indexOf('add_location_search_results')){
    if ($('th:contains(Room)', $(data)).size() == 1) {
      $('.initially_hidden').show();
      $('#search_building_id').val($('#limiting_building_id', data).val());
      addFloorSelector(data);
    } else {
      $('.initially_hidden').hide();
    }
  }
};

/**
 * replace the contents of a div with 
 * results from a form. The form's 'rel'
 * attribute should have the selector for
 * the destination.
 *
 **/ 
function replaceCustomResultsInitializer(){
  $("form.custom_replace_results input[type=submit], form.custom_replace_results input[type=image]").live('click',function() { 
    if( $(this).closest('form').attr('rel') == null || $(this).closest('form').attr('rel').length == 0){
      var selectorContentsToReplace = '.search_results';
    } else {
      var selectorContentsToReplace = $(this).closest('form').attr('rel')
    }
    if ($(this).attr('type') == 'image') {
      $('form').append("<input type='hidden' name='employee' value='"+$(this).val()+"' />");
    }
		setLoadingPicture(selectorContentsToReplace);
		$(this).closest('form').ajaxSubmit({
			'success': function(data, textStatus){
        replaceCustomResultsWithData('#bd > *', selectorContentsToReplace, data);
        otherPageInteractions(this,data);
      }
    });
    return false;
  }); 
  $("a.custom_replace_results").live('click',function() { 
    var selectorContentsToReplace = getSelectorFromRel($(this));
		setLoadingPicture(selectorContentsToReplace);
		$.get($(this).attr('href'), function( data, textStatus){
        replaceCustomResultsWithData('#bd > *',selectorContentsToReplace,data);
        otherPageInteractions(this,data);
    });
    return false;
  }); 
};

function getSelectorFromRel(element, closest) {
  if(typeof(closest) != 'undefined'){
    if(element.closest(closest).attr('rel') == null || element.closest(closest).attr('rel').length == 0){
      var selectorContentsToReplace = '.search_results';
    } else {
      var selectorContentsToReplace = element.closest(closest).attr('rel');
    }
  } else if(element.attr('rel') == null || element.attr('rel').length == 0){
      var selectorContentsToReplace = '.search_results';
    } else {
      var selectorContentsToReplace = element.attr('rel');
    }
  return selectorContentsToReplace;
}
function simpleDialogFromAjaxResponse(data, options){
  var content;
  $("#bd", data).size() == 1 ? content = $("#bd", data).attr('id', '') : content = $('<div>'+data+'</div>');  
  if ( $('h1:first', content).is('h1')){
    var title = $('h1:first', content).text();
    $('h1:first', content).remove();
  } else {
    var title = '';
  }
  var settings = {
    'modal': true,
    'title': title,
    'width': 400,
    'close': function(event, ui){ $('body > .ui-dialog').empty().remove(); }
  }
  $.extend(settings, options);
  content.dialog(settings);
  disableFieldsInitializer();
  autocompleteInitializer();
  tabsInitializer();
  $("input").blur();
};

function disableFieldsInitializer(){

  //Update existing fields to match the default settings.
  $('.disabler').each( function(){
    var classToDisable = $(this).attr('id');
    updateDisabled(classToDisable);
  });
  
  //Add events for disablers
  $('.disabler').unbind().click( function(){
    var classToDisable = $(this).attr('id');
      updateDisabled(classToDisable);
    });
};

function updateDisabled(classToDisable){
  if ($('#'+classToDisable).attr('checked') == true || $('#'+classToDisable).attr('checked') == 'checked'){
    $('.'+classToDisable).removeAttr('disabled');
  } else {
    $('.'+classToDisable).attr("disabled", "disabled");
  }
};

function handleAjaxPagination(){
  $('.pagination.custom_replace_results a').live('click', function(e){
    if( $(this).closest('.custom_replace_results').attr('rel') == null || $(this).closest('.custom_replace_results').attr('rel').length == 0){
      var selectorContentsToReplace = '.search_results';
    } else {
      var selectorContentsToReplace = $(this).closest('.custom_replace_results').attr('rel')
    }
    setLoadingPicture(selectorContentsToReplace);
		$.get($(this).attr('href'), function(data, textStatus){
        replaceCustomResultsWithData('#bd > *', selectorContentsToReplace,data);
      });
    return false;
  });
  $('#search_results .pagination.custom_replace_results a').live('click', function(e){
    setLoadingPicture('#search_results');
    $.get($(this).attr('href'), function(data, textStatus){
        replaceCustomResultsWithData('#bd > *','#search_results',data);
      });
    return false;
  });
};

function showSaveInitializer() {
  $('input[type=radio][name^=primary], input[type=radio][id^used_for]').live('click', function(e){
    showSaveButton();
  });
  $('form[action*=phones] input,form[action*=phones] select, form[action*=phones] textarea').bind('keyup change', function(){
  	showSaveButton();
  });
};

function showSaveButton(){
    $('#save_button').show();
}

function invalidTagsInitializer() {
  $("td > img[src*=invalid.png]").live('click', function(e){
    var title = $(this).attr('title');
    var content = $(this).attr('alt');

    $('<p>'+content+'</p>').dialog({ 
      'buttons': { "Ok": function() { $(this).dialog("close"); }},
      'title': title
    });
  });
};

function roundCorners(){

  DD_roundies.addRule("div.form", '8px');
  DD_roundies.addRule('#doc2', '8px');
  DD_roundies.addRule('#hd', '8px 8px 0 0');
  DD_roundies.addRule('#bd', '0 0 8px 8px');
}

function clearRadiosByClassInitializer(){
	$('th.clear a').live('click', function(e){
		var classToClear = $(this).attr('class');
		var elements = $(this).closest('table').find('td input[id^='+classToClear+']').removeAttr('checked').size();
    $(this).closest('table').find("img[rel^='"+classToClear+"']").attr({'src': '/images/no.png', 'alt': 'No'});
		if (elements > 0) {
			showSaveButton();
		}
		return false;
	});
};

function highlightInitializer() {
	$('.highlight').effect('pulsate', {'times':2}, 700);
}

function formResetInitializer(){
	$('input.force_reset').live('click', function(e){
		$(this).closest('form').find('input[type=text],input[type=radio], select, textarea').val('');
		return false;
	});
}

function newPhoneButtonInitializer(){
	$('button.new_phone').live('click', function(){
		document.location = '/phones/new';
	})
}

function fixInlineFieldErrorBox(){
	$('.fieldWithErrors:has(input[id^=building_zip])').css('display', 'inline');
}

var INCORRECT_INFO_EMAILS = [];
function feedbackFormInitializer(){
  $("#incorrect_information_dialog input[type='checkbox']").live('click', function(e) {
      $(this).closest('#incorrect_information_dialog').find(':checkbox').each( function(item) {
        var checked = $(this).attr('checked');
        checked = (checked == true || checked == 'CHECKED');
        if(checked) {
          if (($.inArray($(this).val(), INCORRECT_INFO_EMAILS)) < 0){
            INCORRECT_INFO_EMAILS.push($(this).val());

          }
        } else
          if ($.inArray($(this).val(),INCORRECT_INFO_EMAILS) >= 0){
          INCORRECT_INFO_EMAILS.splice($.inArray($(this).val(),INCORRECT_INFO_EMAILS), 1);
        }
        setup_incorrect_info_mail_link();
      });
  });
  $('a#email_incorrect_info').live('click', function(e){
     if($(this).attr('href').match(/@/) == null){
       e.preventDefault();
       alert("Please select at least one of the checkboxes (above the pictures).");
     } else {
       $('*').dialog('close');
       $('.ui-dialog').remove();
       $('#incorrect_information_dialog').parent().remove();
     }
  });

}
function setup_incorrect_info_mail_link() {
  $('#email_incorrect_info').attr('href', "mailto:"+INCORRECT_INFO_EMAILS.join(';') +
    "?subject=[Central Directory] Information Change Request&body=Please make the following changes to employee "+
    $("h1.page_header").text()+":");

}
function pageLoadFocusDefaults() {
  $('#user_password').focus();
  $('#old_password').focus();
  $('#login').focus();
}
function checkBoxImageReplacementInitializer(){
  $('table.associations img[alt=Yes], table.associations img[alt=No]').live('click', function(e){
      var id_to_toggle = $(this).attr('rel');
      var field_name = id_to_toggle.split(/_\d+$/)[0]
      $('table.associations img[rel^='+field_name+']').attr({'src': '/images/no.png', 'alt': 'No' });
      $(this).attr({'src': '/images/yes.png', 'alt': 'Yes'});
      $("table.associations [name='"+field_name+"']").removeAttr('checked');
      $("#"+id_to_toggle).attr('checked','checked').click();
  });
}
function toggleInitializer() {
  $('[rel^=toggle]').live('click', function(e) {
      var class_to_toggle = $(this).attr('rel').split('toggle_')[1];
      $('.'+class_to_toggle).toggle();
  });
}

/*
 * Used when submiting a form whose element x's value is 
 * controlled by element 'y' which is outside the form.
 * This verifies that the value of x and y are the same
 * and overwrites element 'x' with the value from 'y' if it is out of date.
 * To use, add class 'verify_value_of_<element x selector> and
 * set y's name attribute to 'select_value_for_<element x selector>'
 *
 */
function verifyValueOfOtherFieldInitializer() {
  $('[class=verify_value_of]').live('click', function(e){
    var element_to_change = $(this).attr('name').split('verify_value_of_')[1];
    var updated_value = $('[name='+element_to_change+']').val();
    $(element_to_change).val(updated_value);
  });
}
function selectValueForOtherFieldInitializer() {
  $('input[id^=select_value_for]').live('change', function(e){
    var element_to_change = $(this).attr('name').split('select_value_for_')[1];
    $(element_to_change).val($(this).val());
  });
}
function hideClosestInitializer() {
  $('[class^=hide_closest]').live('click', function(e) {
      var element_to_hide = $(this).attr('class').split('hide_closest_')[1];
      $(this).closest(element_to_hide).hide();
   });
  $("[class*='hide_id_']").live('click', function(e) {
      var element_to_hide = $(this).attr('class').split('hide_id_')[1];
      if(element_to_hide.match(/ /)) {
        element_to_hide = element_to_hide.substring(0, element_to_hide.indexOf(" "));
      };
      $("#"+element_to_hide).hide();
  });
}
function tabsInitializer(){
   $('#employee_location_tabs').tabs({
      'show': function(event, ui) {
        var indexOfSelected = ui['index']
        var newPrefixText = ui['tab'].innerHTML.toLowerCase();
        var titleBarElement = $(ui['panel']).closest('.ui-dialog').find('span.ui-dialog-title');
        var suffix = titleBarElement.text().substring(titleBarElement.text().indexOf(':')+1, 200);
        titleBarElement.text('Add '+newPrefixText+' to: '+suffix);
      }
    });
}
function init(){
  roundCorners();
  highlightInitializer();
  simpleAjaxDialogInitializer();
  confirmActionDialogInitializer();
  ajaxFormDialogInitializer();
  disableFieldsInitializer();
  autocompleteInitializer();
  replaceResultsInitializer();
  replaceCustomResultsInitializer();
  handleAjaxPagination();
  showSaveInitializer();
  invalidTagsInitializer();
  fixIEImageSubmit();
  clearRadiosByClassInitializer();
  formResetInitializer();
  newPhoneButtonInitializer();
	fixInlineFieldErrorBox();
  feedbackFormInitializer();
  pageLoadFocusDefaults();
  checkBoxImageReplacementInitializer();
  toggleInitializer();
  selectValueForOtherFieldInitializer();
	hideClosestInitializer();
  tabsInitializer();
}

$(document).ready(init);

# This script reads lines from a tab-delimited file
# whose values are addresses and cities, and obtains their
# addresses from Yahoo's geocode service. The script then places 
# results in an xml file called "results.xml" in the same directory.

require 'mechanize'
require 'hpricot'

API_KEY = 'USE YOUR OWN API KEY HERE'
YAHOO_URL = 'http://local.yahooapis.com/MapsService/V1/geocode'
agent = WWW::Mechanize.new
URL = YAHOO_URL + '?appid=' + API_KEY
my_file = File.new('results.xml', 'w')
my_file.puts("<?xml version=\"1.0\" encoding=\"UTF-8\"?><results>")
File.open("buildings.txt", "r") do |infile|

    while (line = infile.gets)
        
      address = line.split(/\t/)
      address_query = URL + "&street=#{address[0]}&city=#{address[1]}&state=fl"
      puts address_query
	  page = agent.get address_query
      result = Hpricot.parse(page.body).search('//result')
	  result.append("<given><address>#{address[0]}</address><city>#{address[1]}</city></given>")
	  my_file.puts(result)
    end
end
my_file.puts("</results>")
my_file.close
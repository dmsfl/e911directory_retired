  class Employee
    acts_as_audited
  end

module EmployeeSpecHelper
  def valid_employee_attributes
    {
      :email => 'heyoyayo@orme.com',
      :last_name => 'dayaieo',
      :first_name  => 'josephero',
      :people_first_id => '1234567890',
      :contractor_flag => false,
      :active_flag => true,
      :people_first_user => '123456'
    }
  end
end
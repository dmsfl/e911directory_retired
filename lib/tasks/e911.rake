
begin
  require 'active_record/fixtures'
  require 'CSV'
  require 'FasterCSV'
  require 'ruport'
rescue MissingSourceFile
  module E911
    def initialize(name)
      task name do
        raise <<-MSG

#{"*" * 80}
*  You are trying to run a rake task defined in
*  #{__FILE__},
*  but one of the required gems can not be found in vendor/gems, vendor/plugins or system gems.
*
#{"*" * 80}
MSG
      end
    end
  end
end

namespace :e911 do
  desc 'Retrieve data from VOIP data file & transfer to director'
  task :update_directory_from_voip => :load do
    require 'ip_phone_updater'
    phone_updater = IpPhoneUpdater.new
    phone_updater.run
  end

  desc 'load judy_masters'
  task :judy_masters => :load do
    puts 'Deleting all JudyMaster data'
    JudyMaster.delete_all
    puts 'Loading new JudyMaster data'
    Fixtures.create_fixtures(@data_dir, "judy_masters")
  end

  desc 'Update employee data from people first.'
  task :update_employees_from_people_first => :load do
    audit_user = APP_CONFIG['settings']['cron_auditor']
    Employee.audit_as(audit_user) do
      Rake::Task['e911:people_firsts'].invoke

      #Eager loading of has_one relationships with the :primary_key option has a bug, therefore find_by_sql is used throughout this rake task.
      #See https://rails.lighthouseapp.com/projects/8994/tickets/1756-has_one-with-foreign_key-primary_key-bug

      #Report new employees
      new_employees = PeopleFirst.find_by_sql("SELECT people_firsts.*, employees.people_first_id
                                               FROM people_firsts 
                                               LEFT OUTER JOIN employees ON employees.people_first_id = people_firsts.employee_id 
                                               WHERE 
                                                 employees.people_first_id IS NULL AND
                                                (people_firsts.vacancy_indicator NOT IN ('Y') OR people_firsts.vacancy_indicator IS NULL)
                                              ")

      puts "Employees with the following People First IDs are new in People First:"
      new_employees.each do |employee|
        puts employee.people_first_id
      end
      #Report inactive employees
      employees_not_in_pf = Employee.find_by_sql("select employees.people_first_id, people_firsts.employee_id 
                                               from employees 
                                               left outer join people_firsts on employees.people_first_id = people_firsts.employee_id 
                                               where people_firsts.employee_id is null")

      puts "Employees with the following People First IDs are not in People First:"
      employees_not_in_pf.each do |employee|
        puts employee.people_first_id
      end
      employees_not_in_pf_ids = employees_not_in_pf.map(&:people_first_id)

      already_inactive_employees = Employee.find(:all, :conditions => {:active_flag => false})
      already_inactive_employees_ids = already_inactive_employees.map(&:people_first_id)
      newly_terminated_ids = employees_not_in_pf_ids - already_inactive_employees_ids

      puts "Employees with the following People First IDs are newly terminated in People First:"
      newly_terminated_ids.each do |employee|
        puts employee
      end

      unless newly_terminated_ids.empty?
        newly_terminated_employees = Employee.find(:all, :conditions => "people_first_id in (#{newly_terminated_ids.join(',')})")

        newly_terminated_employees.each do |employee| 
         employee.active_flag = false
         employee.save(false)
         employee.audits.each {|employee_audit| employee_audit.update_attribute(:sent, true)}
        end
      end

      puts 'Setting all employees to inactive that are not in PF (only do this for employees that are not already inactive)'

      puts 'Creating employees that are new in People First'
      new_employees.each {|people_first_employee| people_first_employee.create_employee}

      puts 'Updating changed employees from people first.'
      #Report and update changed employees
        #For test, change last name from cherry to cherri in people_firsts, emp_id 886189
      changed_employees = Employee.find_by_sql("
                             select employees.* from employees
                             left outer join people_firsts on employees.people_first_id = people_firsts.employee_id
                             where (
                            (people_firsts.employee_id is not null and people_firsts.employee_id != 0) AND
                            (employees.people_first_id is not null and employees.people_first_id != 0) AND
                            (people_firsts.last_name         != employees.last_name OR
                             people_firsts.first_name        != employees.first_name OR
                             people_firsts.middle_name       != employees.middle_name OR
                             people_firsts.email             != employees.email OR
                             people_firsts.confidential      != employees.confidential OR
                             people_firsts.pos_num           != employees.position_number OR
                             people_firsts.people_first_user != employees.people_first_user OR
                             people_firsts.division_name     != employees.division_name OR
                             (people_firsts.last_name         is null and employees.last_name          is not null ) OR
                             (people_firsts.first_name        is null and employees.first_name         is not null ) OR
                             (people_firsts.middle_name       is null and employees.middle_name        is not null ) OR
                             (people_firsts.email             is null and employees.email              is not null ) OR
                             (people_firsts.confidential      is null and employees.confidential       is not null ) OR
                             (people_firsts.pos_num           is null and employees.position_number    is not null ) OR
                             (people_firsts.people_first_user is null and employees.people_first_user  is not null ) OR
                             (people_firsts.division_name     is null and employees.division_name      is not null ) OR
                             (people_firsts.last_name         is not null and employees.last_name          is null ) OR
                             (people_firsts.first_name        is not null and employees.first_name         is null ) OR
                             (people_firsts.middle_name       is not null and employees.middle_name        is null ) OR
                             (people_firsts.email             is not null and employees.email              is null ) OR
                             (people_firsts.confidential      is not null and employees.confidential       is null ) OR
                             (people_firsts.pos_num           is not null and employees.position_number    is null ) OR
                             (people_firsts.people_first_user is not null and employees.people_first_user  is null ) OR
                             (people_firsts.division_name     is not null and employees.division_name      is null ) OR

                             (people_firsts.contractor = '' AND employees.contractor_flag = 1) OR
                             (people_firsts.contractor != '' AND employees.contractor_flag = 0)
                            ))")
        changed_employees.each do |employee|
          # Again, eager loading of has_one relatioships is broken.
          employee_with_changes_from_people_first = PeopleFirst.find(:first, :conditions => {:employee_id => employee.people_first_id})
          
          #collect changed attributes
          changed_attributes = %w{last_name first_name middle_name email confidential people_first_user contractor_flag division_name}.inject(Hash.new) do |new_atts, attr|
            employee.send(attr) != employee_with_changes_from_people_first.send(attr) ? new_atts.merge!({attr => employee_with_changes_from_people_first.send(attr)}) : new_atts
          end
          changed_attributes.merge!({:position_number => employee_with_changes_from_people_first.pos_num}) if employee.position_number != employee_with_changes_from_people_first.pos_num 
          employee.update_attributes(changed_attributes)
          employee.save(false)
        end
      
        employee_changes = Audit.find(:all, :conditions => ["auditable_type = ? AND username = ? AND auditable_id in (?)", 'employee', audit_user, changed_employees.map(&:id).join(',')])
   
    end
  end  
  desc 'load people_firsts'
  task :people_firsts => :load do
    FileUtils.cp(APP_CONFIG['pf_data_paths']['src'], "#{RAILS_ROOT}/#{APP_CONFIG['pf_data_paths']['dest']}")
    table = Ruport::Data::Table.load("#{RAILS_ROOT}/#{APP_CONFIG['pf_data_paths']['dest']}")
    table.rename_columns({
      'Location County'      => 'county_id',
      'Location Room'        => 'location_room',
      'Location State'       => 'location_state',
      'Location Bldg'        => 'building_name',
      'Location Addr'        => 'location_address',
      'Vacancy Ind'          => 'vacancy_indicator',
      'Last Name'            => 'last_name',
      'First Name'           => 'first_name',
      'Middle Name'          => 'middle_name',
      '411 Phone Number'     => 'four_11_phone',
      '411 Phone Number Ext' => 'four_11_phone_number_ext',
      'Location Bldg Num'    => 'location_bldg_number',
      'Pos Num'              => 'pos_num',
      'Olo Code'             => 'olo_code',
      'Location City'        => 'location_City',
      'Email Address'        => 'email',
      'Emp Id'               => 'employee_id',
      'Contractor Flag'      => 'contractor',
      'Name Suffix'          => 'name_suffix',
      'Mail Zip'             => 'mail_zip',
      'Privacy Flags'        => 'confidential',
      'User Id'              => 'people_first_user',
      'Org Name'             => 'division_name',
      'CONFID, SWORN, EXEMPT OR PROTECT FLAG' => 'confidential',
      'Restricted Relative Ind' => 'restricted_relative_ind',
      'Restricted Employee Ind' => 'restricted_employee_ind'
    })
    table.as(:csv, :file => "#{RAILS_ROOT}/#{APP_CONFIG['pf_data_paths']['fixtures']}")
    PeopleFirst.delete_all
    Fixtures.create_fixtures(File.dirname("#{RAILS_ROOT}/#{APP_CONFIG['pf_data_paths']['fixtures']}"), "people_firsts")
  end
  
  desc 'put judy info in employees, locations, phones, and master_relations'
  task :initial_load => :judy_masters do
    Audit.delete_all
    Rake::Task['e911:reference'].invoke
    Rake::Task['e911:employees_from_people_first'].invoke
    Rake::Task['e911:phones_from_judy'].invoke
    Rake::Task['e911:faxes_from_judy'].invoke
    Rake::Task['e911:locations_from_judy'].invoke
    Rake::Task['e911:remove_duplicate_phones'].invoke
    Rake::Task['e911:remove_duplicate_locations'].invoke
    Rake::Task['e911:connections_from_judy'].invoke
  end
  
  desc 'Create master relations from Judy master file'
  task :connections_from_judy => :load do
    puts 'Creating Master Relations'
    MasterRelation.delete_all
    re = JudyMaster.all.inject({:e => [], :l => [], :p => [], :multiple_locations_conditions => []}) do |r, jm|
      faulty_stuff = jm.create_connections 
      unless faulty_stuff == true
        r[:e] << faulty_stuff[:faulty_employees] || nil
        r[:l] << faulty_stuff[:faulty_locations] || nil
        r[:p] << faulty_stuff[:faulty_phones] || nil
        r[:multiple_locations_conditions] << faulty_stuff[:multiple_locations_conditions] || nil
      end
      r
    end
    re = re.each_value(&:compact!)
    JudyMaster.report_table(re[:multiple_locations_conditions]).as(:csv, :file => 'Telecom Locations Matching More Than One Building.csv')
    JudyMaster.report_table(re[:l]).as(:csv, :file => 'Building Mismatches in Telecom File.csv')
    JudyMaster.report_table(re[:e]).as(:csv, :file => 'Employees in Telecom but Not in People First.csv')
    JudyMaster.report_table(re[:p]).as(:csv, :file => 'Invalid Phone Numbers in Telecom File.csv')
    puts "Added master relations."
    deleted = MasterRelation.delete_all('location_id is not null and employee_id is null and phone_id is null')
    deleted += MasterRelation.delete_all('location_id is null and employee_id is not null and phone_id is null')
    deleted += MasterRelation.delete_all('location_id is null and employee_id is null and phone_id is not null')
    #    puts "Removed #{deleted} single-item master relations"
    Rake::Task['e911:set_primary_phone_location'].invoke
    Rake::Task['e911:set_primary_employee_location'].invoke
    Rake::Task['e911:set_primary_phone_for_employee'].invoke
    Rake::Task['e911:set_primary_employee_for_phone'].invoke
    Rake::Task['e911:set_411_phone_numbers'].invoke
  end
  
  
  task :employees_from_people_first => :people_firsts do
    Employee.delete_all
    results = PeopleFirst.all.inject(0) do |c, noo|
      noo.create_employee ? c + 1 : c 
    end
    puts "Created #{results} employees"
  end
  
  
  task :phones_from_judy => :load do
    Phone.delete_all
    results = JudyMaster.all.inject(0) do |c, jm|
      jm.create_phone ? c + 1 : c 
    end
    puts "Created #{results} phones"
    Rake::Task['e911:remove_duplicate_phones'].invoke
  end
  
  task :faxes_from_judy => :load do
    Phone.delete_all(['phone_category_id = ?', PhoneCategory.find(:first, :conditions => "name = 'Fax'")])
    Fixtures.create_fixtures(@data_dir, "judy_faxes")
    results = JudyFax.all.inject(0) do |c, jf|
      jf.create_phone ? c + 1 : c 
    end
    puts "Created #{results} phones"
    Rake::Task['e911:remove_duplicate_phones'].invoke
    
  end
  
  
  task :locations_from_judy  => :load do
    Location.delete_all
    @unknown_locations = Array.new
    results = JudyMaster.all.inject(0) do |c, jm|
      if jm.create_location == true
        c + 1
      else
        @unknown_locations << jm.create_location
        c
      end
    end
    puts "Created #{results} locations"
    if @unknown_locations.empty?
      puts 'All Lookup addresses were valid.'
    else
      puts 'Not all Lookup addresses were valid. check the report at the application root.'
      JudyMaster.report_table(@unknown_locations).as(:csv, :file => 'addresses_not_found.csv')
    end
  end
  
  
  desc 'Remove duplicate phones (used by e911:phones_from_judy)'
  task :remove_duplicate_phones => :load do
    dup_phones = Phone.find(:all, 
      :select => 'count(*), phone_category_id, area_code, main_number, id', 
      :group => 'concat(area_code, main_number)',
      :having => 'count(*) > 1')
    
    dup_ids = dup_phones.map {|dp| dp.id }
    
    dup_phones.each do |dp|
      phones = Phone.find(:all, :conditions => ['phone_category_id = ? and area_code = ? and main_number = ?', dp.phone_category_id, dp.area_code, dp.main_number])
      phones[1..-1].each {|p| p.delete}
    end
    puts "Deleted #{dup_ids.size} duplicate phones"
  end
  
  
  desc 'Remove locations with exactly the same information'
  task :remove_duplicate_locations => :load do
    dup_locations = Location.find(:all,
      :select => 'count(*), id, building_id, floor, room, building_identifier',
      :group => 'building_id, floor, room, building_identifier',
      :having => 'count(*) > 1'
    )
    dup_ids = dup_locations.map {|dp| dp.id }
    puts "Found #{dup_ids.size} duplicate locations"
    dup_ids.each do |dp|
      d = Location.find(dp)
      search = Search.new
      locations = Location.find(:all, :conditions => {'building_id' => d.building_id, 'floor' => d.floor, 'room' => d.room, 'building_identifier' => d.building_identifier})
      locations[1..-1].each {|p| p.delete}
    end
    puts "Deleted #{dup_ids.size} duplicate locations"
    
  end
  
  
  desc 'If phone has only one master relation and has a location, set the location/phone primary as true'
  task :set_primary_phone_location => :remove_duplicate_phones do
    table = MasterRelation.report_table(:all, 
      :select => 'phone_id, location_id, count(*) as total_copies', 
      :include => {:phone => {:methods => 'to_label', :only => 'to_label'}}, 
      :having => ['(count(*) > 1 or phone_id is null or location_id is null)'], 
      :group => 'phone_id')
    table.rename_columns('phone.to_label' => 'Full Name', 'total_copies' => 'Total Appearances')
    table.add_column('New Primary Location', :default => '')
    table.as(:csv, :file => 'Phones with multiple locations.csv')
    
    puts 'Setting primary locations for phones (for 911)'
    dup_ids = MasterRelation.find(:all, 
      :select => 'phone_id, location_id, count(*)', 
      :include => 'phone',
      :having => ['(count(*) > 1 or phone_id is null or location_id is null)'], 
      :group => 'phone_id').map {|e| e.phone_id}.reject { |e| e.blank? }
    
    omit_dup_phones_condition = dup_ids.empty? ? 'phone_id is not null and location_id is not null' : "phone_id not in (#{dup_ids.uniq.join(',')}) and phone_id is not null and location_id is not null"
    MasterRelation.find(:all, :conditions => omit_dup_phones_condition).each {|ml| ml.primary_phone_location = 1; ml.save!}
  end
  
  
  desc 'If employee has only one master relation and has a location, set the employee/location primary as true'
  task :set_primary_employee_location => :load do
    table = MasterRelation.report_table(:all, 
      :select => 'employee_id, location_id, count(*) as total_copies', 
      :include => {:employee => {:methods => 'full_name', :only => 'full_name'}}, 
      :having => ['(count(*) > 1 AND employee_id IS NOT NULL AND location_id IS NOT NULL)'], 
      :group => 'employee_id',
      :only => 'total_copies')
    table.rename_columns('employee.full_name' => 'Full Name', 'total_copies' => 'Total Appearances')
    table.add_column('New Primary Location', :default => '')
    table.as(:csv, :file => 'Employees with multiple locations.csv')
    
    puts 'Setting primary employee locations for 911'
    dup_ids = MasterRelation.find(:all, 
      :select => 'location_id, employee_id, count(*)',
      :having => ['(count(*) > 1 or employee_id is null or location_id is null)'], 
      :group => 'employee_id').map {|e| e.employee_id}.reject { |e| e.blank? }
    
    omit_dup_locations_condition = dup_ids.empty? ? 'location_id is not null and employee_id is not null' : "employee_id not in (#{dup_ids.uniq.join(',')}) and location_id is not null and employee_id is not null"
    MasterRelation.find(:all, :conditions => omit_dup_locations_condition).each {|ml| ml.primary_employee_location = 1; ml.save!}
    #MasterRelation.update_all('primary_employee_location = 1', omit_dup_locations_condition)
  end
  
  
  desc 'If employee has only one phone, set the primary_phone_for_employee to true'
  task :set_primary_phone_for_employee => :remove_duplicate_phones do
    table = MasterRelation.report_table(:all, 
      :select => 'employee_id, phone_id, count(*) as total_copies', 
      :include => {:employee => {:methods => 'full_name', :only => 'full_name'}}, 
      :having => ['(count(*) > 1 AND employee_id IS NOT NULL AND phone_id IS NOT NULL)'], 
      :group => 'employee_id ',
      :only => 'total_copies')
    table.rename_columns('employee.full_name' => 'Full Name', 'total_copies' => 'Total Appearances')
    table.add_column('New Primary Phone', :default => '')
    table.as(:csv, :file => 'Employees with multiple phones.csv')
    
    puts 'Setting primary phone for employees'
    dup_ids = MasterRelation.find(:all, 
      :select => 'phone_id, employee_id, count(*)',
      :having => ['(count(*) > 1 or employee_id is null or phone_id is null)'], 
      :group => 'employee_id').map {|e| e.employee_id}.reject { |e| e.blank? }
    omit_dup_locations_condition = dup_ids.empty? ? 'phone_id is not null and employee_id is not null' : "employee_id not in (#{dup_ids.uniq.join(',')}) and phone_id is not null and employee_id is not null"
    MasterRelation.find(:all, :conditions =>  omit_dup_locations_condition).each {|ml| ml.primary_phone_for_employee = 1;ml.save!}
  end
  
  
  desc 'If phone has only one employee, set the primary_employee_for_phone to true'
  task :set_primary_employee_for_phone => :remove_duplicate_phones do
    table_phones_with_multiple_employees = MasterRelation.report_table(:all, 
      :select => 'employee_id, phone_id, count(*) as total_copies', 
      :include => {:phone => {:methods => 'to_label', :only => 'to_label'}}, 
      :having => ['(count(*) > 1 AND employee_id IS NOT NULL AND phone_id IS NOT NULL)'], 
      :group => 'phone_id ',
      :only => 'total_copies')
    table_phones_with_multiple_employees.rename_columns('phone.to_label' => 'Phone', 'total_copies' => 'Total Appearances')
    table_phones_with_multiple_employees.add_column('New Primary Employee', :default => '')
    table_phones_with_multiple_employees.as(:csv, :file => 'Phones with multiple employees.csv')
    
    
    puts 'Setting primary employees for phones'
    dup_ids = MasterRelation.find(:all, 
      :select => 'phone_id, employee_id, count(*)',
      :having => ['(count(*) > 1 or employee_id is null or phone_id is null)'], 
      :group => 'phone_id').map {|e| e.phone_id}.reject { |e| e.blank? }
    omit_dup_locations_condition = dup_ids.empty? ? 'phone_id is not null and employee_id is not null' : "phone_id not in (#{dup_ids.uniq.join(',')}) and phone_id is not null and employee_id is not null"
    MasterRelation.find(:all, :conditions =>  omit_dup_locations_condition).each {|ml| ml.primary_employee_for_phone = 1;ml.save!}
  end
  
  desc "If the people first phone number matches one of the employee's phone numbers, set the 411 number for that master relation to true."
  task :set_411_phone_numbers => :remove_duplicate_phones do
    puts 'Setting 411 phones for employees'
    ids_for_411 = MasterRelation.find_by_sql("
    select 
      master_relations.id as id
      from master_relations, employees, phones, people_firsts
      where
        phones.id = master_relations.phone_id        and
        concat(phones.area_code,'-',
             substring(phones.main_number,1,3),'-', 
             substring(phones.main_number,4,7) 
            ) = people_firsts.four_11_phone          and
        employees.id = master_relations.employee_id  and
        people_firsts.employee_id = employees.people_first_id
    ").map {|e| e.id}.reject { |e| e.blank? }
    MasterRelation.find(:all, :conditions => "id in (#{ids_for_411.uniq.join(',')})").each {|ml| ml.used_for_411 = 1; ml.save!}
    MasterRelation.find(:all, :conditions => "id not in (#{ids_for_411.uniq.join(',')})").each {|ml| ml.used_for_411 = 0; ml.save!}
    
  end
  
  desc 'Report duplicate employees for 911 and Judy files (MySQL-dependent)'
  task :dup_employees => :load do
    dup_employees = PeopleFirst.find_by_sql("
    select distinct
      people_firsts.first_name  as _911_first_name,
      people_firsts.last_name   as _911_last_name,
      people_firsts.middle_name as _911_middle_name,
      employees.first_name           as judy_first_name,
      employees.last_name            as judy_last_name,
      employees.middle_name          as judy_middle_name
    from people_firsts 
      left outer join employees on 
      concat(employees.first_name,employees.last_name,employees.middle_name)=concat(people_firsts.first_name,people_firsts.last_name,people_firsts.middle_name)
    where
      concat(employees.first_name,employees.last_name,employees.middle_name) is null
      or
      concat(people_firsts.first_name,people_firsts.last_name,people_firsts.middle_name) is null
      or
      concat(employees.first_name,employees.last_name,employees.middle_name) = ''
      or
      concat(people_firsts.first_name,people_firsts.last_name,people_firsts.middle_name) = ''
    ")
    PeopleFirst.report_table(dup_employees, :select => '_911_first_name, _911_last_name, _911_middle_name, judy_first_name, judy_last_name, judy_middle_name').as(:csv, :file => 'duplicate_employees.csv')
    
    Rake::Task['e911:add_nonexisting_people_first'].invoke
  end
  
  
  desc 'Add employees from the people first table that do not asready exist'
  task :add_nonexisting_judy_master => :load do
    missing = JudyMaster.find(:all, :select => 'judy_masters.id as id', 
      :joins => 'left outer join employees on concat(employees.first_name,employees.last_name,employees.middle_name)=concat(judy_masters.first_name,judy_masters.last_name,judy_masters.middle_name)',
      :conditions => 
        "
        concat(employees.first_name,employees.last_name,employees.middle_name) is not null
        or
        concat(employees.first_name,employees.last_name,employees.middle_name) != ''
        "
    ).map {|e| e.id}
    puts "Found #{missing.size} employees that exist in EMPLOYEES but not in JUDY_MASTERS\nAdding them:"
    JudyMaster.find(:all, :conditions => "id in (#{missing})").each do |jm|
      jm.create_employee
      print '.'
    end
    
  end
  
  
  desc 'load default users'
  task :users => :load do
    User.delete_all
    if Employee.count == 0
      raise "Please add employees before loading the default users."
    end
    admin_role = Role.find_by_name('Administrator')
    
    #Create initial users
    admin_users_condition = APP_CONFIG['settings']['admin_users'].values.join(",")
    Employee.find(:all, :conditions =>"people_first_id IN ("+admin_users_condition+")").each do |employee|
      user = User.new
      user.employee = employee
      user.login = employee.people_first_user
      user.password = user.password_confirmation = APP_CONFIG['settings']['initial_pwd']
      user.roles << admin_role
      user.save(false)
    end
    
  end
  
  
  #############################  REFERENCE TABLES ############################################ 
  
  
  desc 'load all reference data (counties, roles, phone_categories, street_abbreviations)'
  task :reference => :load do
    
    Building.delete_all
    Fixtures.create_fixtures(@data_dir, "buildings")
    Building.all.each do |b| 
      b.update_full_street_address
      b.save(false)
    end
    
    Campus.delete_all
    Fixtures.create_fixtures(@data_dir, 'campuses')
    
    puts 'Removing existing campus relations'
    Building.update_all('campus_id = null')
    data_file = @data_dir + '/building_campuses.csv'
    reader = CSV.open(data_file, 'r')
    header = reader.shift
    all_data = []
    puts "Starting to read: #{data_file}"
    reader.each do |row|
      begin 
        campus = Campus.find_by_name(row[1]).id
        Building.update_all("campus_id = #{campus}", ["lookup_address like ? and campus_id is null", "%#{row[0]}%"])
        print '.'
      rescue
        puts "Unfound campus in campus_buildings file: #{row[1]}"
      end
    end
    
    County.delete_all
    Fixtures.create_fixtures(@data_dir, "counties")
    
    PhoneCategory.delete_all
    Fixtures.create_fixtures(@data_dir, 'phone_categories')
    
    StreetAbbreviation.delete_all
    Fixtures.create_fixtures(@data_dir, "street_abbreviations")
    
    Role.delete_all
    Fixtures.create_fixtures(@data_dir, "roles")
  end
  
  task :load => :environment do
    @data_dir = File.join(RAILS_ROOT, "/db/migrate/data")
  end
  
  
  #***********  SYNCHRONIZE PEOPLE FIRST DATA  *************
  
  desc '911 Initial load' 
  task :a11_init => :load do
    require 'fixed_text'
    tallahassee_locations = MasterRelation.find_by_sql("SELECT master_relations.id FROM master_relations, locations, buildings, counties WHERE 
                             master_relations.location_id = locations.id AND
                             locations.building_id = buildings.id AND
                             buildings.county_id = counties.id AND
                             counties.name = 'Leon' AND
                             phone_id is not null AND 
                             primary_phone_location = 1 AND
                             location_id is not null").map(&:id)
    table = MasterRelation.report_table(tallahassee_locations,
    :include => {:phone    => {:only => ['area_code','main_number']}, 
                 :location => {:only => ['room', 'floor','building_identifier']}, 
                 :employee => {:only => ['first_name', 'middle_name', 'last_name']}, 
                 :building => {:only => ['street_number','street_number_suffix','prefix_directional','street_name','street_suffix','post_directional','city','state','zip_code_5','zip_code_4']}},
    :only =>[])
    
    # These columns are aranged to facilitate comparison between this list and the NENA 2.1 data format
    table.add_column                                    'function_code', :default => 'I'
    table.rename_column 'phone.area_code',              'npa'
    table.rename_column 'phone.main_number',            'calling_number'
    table.rename_column 'building.street_number',       'house_number'
    table.rename_column 'building.street_number_suffix','house_number_suffix'
    table.rename_column 'building.prefix_directional',  'prefix_directional'
    table.rename_column 'building.street_name',         'street_name'
    table.rename_column 'building.street_suffix',       'street_suffix'
    table.rename_column 'building.post_directional',    'post_directional'
    table.rename_column 'building.city',                'community_name'
    table.rename_column 'building.state',               'state'
    table.add_column('location'){|r| 
      f = r['location.floor'].nil? ? nil : "Flr #{r['location.floor']}"
      rm = r['location.room'].nil? ? nil : "Rm #{r['location.room']}"
      bi = r['location.building_identifier'].nil? ? nil : "Building #{r['location.building_identifier']}"
      [bi, f, rm].compact.join(', ')
    }
    table.add_column('customer_name'){|r|
      [r['employee.first_name'], 
      r['employee.middle_name'], 
      r['employee.last_name']].
      compact.collect {|e| e.capitalize}.join(' ')
    }
    table.add_column                                    'class_of_service', :default => '5'
    table.add_columns                                  ['type_of_service', 
                                                        'exchange',  
                                                        'esn'], :default => ''
    table.add_column('main_npa')     {|r| r['npa']}
    table.add_column('main_number')  {|r| r['main_number']}
    table.add_column                                    'order_number',                  :default => ''
    table.add_column                                    'extract_date',                  :default => Date.today.strftime('%m%d%y')
    table.add_column                                    'county_id',                     :default => '073'
    table.add_column                                    'company_id',                    :default => 'FLDMS'
    table.add_column                                    'source_id',                     :default => 'C'
    table.rename_column 'building.zip_code_5',          'zip_code'
    table.rename_column 'building.zip_code_4',          'zip_code_4'
    table.add_columns                                  ['general_use', 
                                                        'customer_code',
                                                        'comments',
                                                        'x_coordinate',
                                                        'y_coordinate',
                                                        'z_coordinate',
                                                        'cell_id',
                                                        'sector_id',
                                                        'tar_code',
                                                        'reserved',
                                                        'alt_num'],              :default => ''
    table.add_column                                    'expanded_extract_date', :default => Date.today.strftime('%Y%m%d')
    table.add_columns                                  ['nena_reserved',
                                                        'data_provider_id',
                                                        'reserved_2'], :default => ''
    ind = -1
    columns = Hash[*table.column_names.map do |e| 
      ind = ind + 1
      [e, ind]
      end.flatten]
    table.reorder(
                    columns['function_code'],
      columns['npa'],
      columns['calling_number'],
      columns['house_number'],
      columns['house_number_suffix'],
      columns['prefix_directional'],
      columns['street_name'],
      columns['street_suffix'],
      columns['post_directional'],
      columns['community_name'],
      columns['state'],
      columns['location'],
      columns['customer_name'],
      columns['class_of_service'],
      columns['type_of_service'],
      columns['exchange'],
      columns['esn'],
      columns['main_npa'],
      columns['main_number'],
      columns['order_number'],
      columns['extract_date'],
      columns['county_id'],
      columns['company_id'],           
      columns['source_id'],         
      columns['zip_code'],
      columns['zip_code_4'],
      columns['general_use'],
      columns['customer_code'],
      columns['comments'],
      columns['x_coordinate'],
      columns['y_coordinate'],
      columns['z_coordinate'],
      columns['cell_id'],
      columns['sector_id'],
      columns['tar_code'],
      columns['reserved'],
      columns['alt_num'],
      columns['expanded_extract_date'],
      columns['nena_reserved'],
      columns['data_provider_id'],
      columns['reserved_2']
     )
     ind = -1
     new_columns = Hash[*table.column_names.map do |e| 
       ind = ind + 1
       [e, ind]
     end.flatten]
    file_name =  "STFL_initial_911_load_#{Date.today.strftime('%Y-%m-%d')}.txt"
    full_path = "#{RAILS_ROOT}/lib/tasks/sent_911/#{file_name}"
    table.as(:fixed_text, 
             :file => full_path, 
             :ignore_table_width => true,
             :alignment => :left,
             :show_table_headers => false,
             :col_width => {
                new_columns['function_code'] => 1, 
                new_columns['npa'] => 3,
                new_columns['calling_number'] => 7,
                new_columns['house_number'] => 10,
                new_columns['house_number_suffix'] => 4,
                new_columns['prefix_directional'] => 2,
                new_columns['street_name'] => 60,
                new_columns['street_suffix'] => 4,
                new_columns['post_directional'] => 2,
                new_columns['community_name'] => 32,
                new_columns['state'] => 2,
                new_columns['location'] => 60,
                new_columns['customer_name'] => 32,
                new_columns['class_of_service'] => 1,  
                new_columns['type_of_service'] => 1, 
                new_columns['exchange'] => 4,  
                new_columns['esn'] => 5,  
                new_columns['main_npa'] => 3,  
                new_columns['main_number'] => 7, 
                new_columns['order_number'] => 10,  
                new_columns['extract_date'] => 6,  
                new_columns['county_id'] => 4,  
                new_columns['company_id'] => 5, 
                new_columns['source_id'] => 1,  
                new_columns['zip_code'] => 5,
                new_columns['zip_code_4'] => 4,
                new_columns['general_use'] => 11, 
                new_columns['customer_code'] => 3, 
                new_columns['comments'] => 30, 
                new_columns['x_coordinate'] => 9,
                new_columns['y_coordinate'] => 9,
                new_columns['z_coordinate'] => 5,
                new_columns['cell_id'] => 6,
                new_columns['sector_id'] => 1,
                new_columns['tar_code'] => 6,
                new_columns['reserved'] => 21,
                new_columns['alt_num'] => 10,
                new_columns['expanded_extract_date'] => 8,
                new_columns['nena_reserved'] => 81,
                new_columns['data_provider_id'] => 5,
                new_columns['reserved_2'] => 31
              }) 
    table.each do |row|
      Current911Data.create(row.data)
    end
    #send_file_to_911(full_path, file_name)
  end
 

    
    #This task sends all changes that took place since the last 911 update to 911
    #Settings for 911 transfer destination are stored in config.yml under akassociates
  desc 'Send changes to 911'
  task :send_911_daily_update => :load do
    require 'fixed_text'

    include AuditHelper 
    
    @changes_needing_sending = AuditE911.find(:all, 
                          :select => ['concat(auditable_type, auditable_id) as auds,
                            id,auditable_type,auditable_id,action,user_type,version,sent_to_911, username'],
                          :group => ['auds,id,auditable_type,auditable_id,action,user_type,username,version'], 
                          :order => 'id desc', 
                          :having => ["sent_to_911 = ? AND (user_type IS NOT NULL 
                                                            OR username = '#{APP_CONFIG['settings']['cron_auditor']}') 
                            AND auditable_type in ('Employee', 'Phone', 'MasterRelation', 'Location', 'Building')", false])

    @phone_changes_and_function_codes = {}
    @changes_needing_sending.each do |audit_change|
        type = audit_change.auditable_type
        case 
          when type == 'Phone'
            process_phone(audit_change)
          when type == 'MasterRelation'
            process_master_relation(audit_change)
          when type == 'Location'
            process_location(audit_change)
          when type == 'Employee'
            process_employee(audit_change)
          when type == 'Building'
            process_building(audit_change)
        end
      end
      unless @phone_changes_and_function_codes.empty?
        if send_file_to_911(generate_changes_file)
          AuditE911.find(:all, :conditions => "(user_id is not null OR username = '#{APP_CONFIG['settings']['cron_auditor']}') and sent_to_911 = 0").each{|a| a.update_attribute('sent_to_911', true)}
          puts "Sent #{@phone_changes_and_function_codes.size} changes to 911."
          puts "They were:"
          @phone_changes_and_function_codes.each do |phone_id, function_code|
            puts "phone_id #{'%6s' % phone_id}, function code #{function_code}"
          end
        end
      else
        puts "No changes needed to be sent to 911 at #{Time.now}"
    end
  end
    
  desc 'Send changes that users made to HR'
  task :send_user_changes_to_people_first => :load do
    @unsent_changes = AuditE911.all_unsent_user_changes
    
    @unsent_changes = AuditE911.select_only_people_first_changes(@unsent_changes)
    
    unless @unsent_changes.empty?
      if AuditMailer.deliver_user_changes_to_people_first
        
        Audit.update_all('sent = 1')
        @unsent_changes_count = 0
        @success =true
        @message = "#{@unsent_changes.size} changes were sent from the DMS Directory to HR at #{Time.now}."
      else
        @success = false
        @message = "ERROR! We were unable to send changes from the DMS Directory to HR at #{Time.now}. <br />"+
               "Please check the application log on #{`hostname`} at the directory #{RAILS_ROOT}"
      end
    else
      @message = "No changes from the DMS Directory needed to be sent to HR at #{Time.now}."
    end
    AuditMailer.deliver_cron_rake_message(@message)
  end
    
  desc 'Audit all'
  task :start_auditing => :load do
    Building.all.each do |bldg|
      bldg.send(:audit_create)
    end
    Location.all.each do |bldg|
      bldg.send(:audit_create)
    end
    Employee.all.each do |bldg|
      bldg.send(:audit_create)
    end
    MasterRelation.all.each do |bldg|
      bldg.send(:audit_create)
    end
    Phone.all.each do |bldg|
      bldg.send(:audit_create)
    end

  end

  def process_master_relation(master_relation_change)
    case
      when master_relation_change.action == 'update'
        handle_master_relation_update(master_relation_change)
    end
  end
  def handle_master_relation_update(master_relation_update)

    changes = master_relation_update.changes
    
    revision = master_relation_update.revision

    case
      when changes.keys.include?('primary_phone_location')
        if changes['phone_id'].nil? && changes['primary_phone_location'][0] == '1' && changes['primary_phone_location'][1] == '0'
          if phone_has_primary_location?(revision)
            @phone_changes_and_function_codes[revision.phone_id] = 'U'
          elsif revision.phone_id && Phone.exists?(revision.phone_id)
            @phone_changes_and_function_codes[revision.phone_id] = 'D'
          else
            phone_id = AuditE911.find(:first, :conditions => ["auditable_type = 'MasterRelation' and auditable_id = ? and version = ?",master_relation_update.auditable_id, master_relation_update.version - 1]).revision.phone_id
            @phone_changes_and_function_codes[phone_id] = 'D'
          end
        elsif changes['primary_phone_location'][1] == '1'
          if phone_had_a_previous_primary_location?(revision, master_relation_update)
            @phone_changes_and_function_codes[revision.phone_id] = 'U'
          else
            master_relation = MasterRelation.find(revision.id)
            phone_for_911 = (master_relation.phone && master_relation.primary_phone_location rescue false)
            if phone_for_911
              @phone_changes_and_function_codes[revision.phone_id] = 'I'
            end
          end
        elsif changes['location_id'] && changes['location_id'][1].nil? && changes['primary_phone_location'][1].nil? && !phone_has_primary_location?(revision)
            @phone_changes_and_function_codes[revision.phone_id] = 'D' unless @phone_changes_and_function_codes[revision.phone_id]
        end
      when primary_location?(revision, changes) && changes['primary_employee_for_phone'] && changes['employee_id'] && changes['employee_id'][0] != changes['employee_id'][1] 
        @phone_changes_and_function_codes[revision.phone_id] = 'U' unless @phone_changes_and_function_codes[revision.phone_id]
      when primary_location?(revision, changes) && changes['primary_employee_for_phone'] && changes['primary_employee_for_phone'][0] != changes['primary_employee_for_phone'][1]
        @phone_changes_and_function_codes[revision.phone_id] = 'U' unless @phone_changes_and_function_codes[revision.phone_id]
    end
  end
  
  def primary_location?(revision, changes)
    if revision.location_id && revision.phone_id && revision.primary_phone_location
      true
    else
      false
    end
  end
  def phone_had_a_previous_primary_location?(revision, change)
    if revision.phone_id && Phone.exists?(revision.phone_id) 
      audit_ids = AuditChange.find(:all, :conditions => "field = 'phone_id' and new_value = #{revision.phone_id}", :include => 'audit', :group => 'audit_id').map(&:audit_id)
      master_relation_ids = Audit.find(:all, :select => 'auditable_id', :conditions => {:id => audit_ids}, :group => 'auditable_id').map(&:auditable_id)
      if AuditChange.find(:all, 
                          :select => 'audits.auditable_id, audits.created_at, audit_changes.field, audit_changes.new_value', 
                          :from => 'audits', 
                          :joins => 'inner join audit_changes on audit_changes.audit_id = audits.id', 
                          :conditions => ["audits.auditable_id in ('#{master_relation_ids.join("','")}') and field = 'primary_phone_location' and new_value = '1' and DATE(created_at) < DATE(?)",
                            Audit.find(change.id).created_at]).empty?
        false
      else
        true
      end
    else
      false
    end
  end
  
  def phone_has_primary_location?(revision)
    if revision.phone_id && phone = (Phone.find(revision.phone_id) rescue false)
      phone.master_relations.detect {|ml| ml.primary_phone_location}
    else
      false
    end
  end
  
  def process_employee(employee_change)
    employee = employee_change.revision
    case employee_change.action
      when 'update'
        employee.get_primary_labels
        phone_id = employee.primary_labels[:phone_id]
       @phone_changes_and_function_codes[phone_id] = 'U' unless @phone_changes_and_function_codes[phone_id] || phone_id.blank?
      when 'create'
        #Should not do anything if an employee is created (they don't have a phone yet)
      when 'destroy'
        if master_relation_change = AuditChange.find(:first, :conditions => "field = 'employee_id' and old_value = #{employee_change.auditable_id} and new_value is null", :order => 'id desc').audit rescue false
          ml_changes = master_relation_change.changes
        else
          ml_changes = {}
        end
        if ml_changes['primary_employee_for_phone'] && ml_changes['primary_employee_for_phone'][0] = '1' && master_relation_change.revision.phone_id
          phone_id = master_relation_change.revision.phone_id
          @phone_changes_and_function_codes[phone_id] = 'U' unless @phone_changes_and_function_codes[phone_id] || phone_id.blank?
        end
    end
  end
  
  def process_location(location_change)
    location = location_change.revision
    case location_change.action
      when 'update'
        primary_phone_ids = location.master_relations.select {|ml| ml.primary_phone_location && ml.phone}.map(&:phone_id)
        primary_phone_ids.each do |phone_id|
          @phone_changes_and_function_codes[phone_id] = 'U' unless @phone_changes_and_function_codes[phone_id] || phone_id.blank?
        end
      when 'create'
        #Should not do anything if an location is created (they don't have a phone yet)
      when 'destroy'
        if AuditChange.find(:first, :conditions => "field = 'location_id' and old_value = #{location_change.auditable_id} and new_value is null", :order => 'id desc')
          master_relation_change = AuditChange.find(:first, :conditions => "field = 'location_id' and old_value = #{location_change.auditable_id} and new_value is null", :order => 'id desc').audit
          ml_changes = master_relation_change.changes
          if ml_changes['primary_phone_location'] && ml_changes['primary_phone_location'][0] = '1' && master_relation_change.revision.phone_id
            phone_id = master_relation_change.revision.phone_id
            @phone_changes_and_function_codes[phone_id] = 'D' unless @phone_changes_and_function_codes[phone_id] || phone_id.blank?
          end
        end
      end

  end
  
  def process_building(building_change)
    building = building_change.revision
    case building_change.action
      when 'update'
        primary_phone_ids = building.locations.collect {|l| l.master_relations.select {|ml| ml.primary_phone_location && ml.phone}.map(&:phone_id)}.flatten

        primary_phone_ids.each do |phone_id|
          @phone_changes_and_function_codes[phone_id] = 'U' unless @phone_changes_and_function_codes[phone_id] || phone_id.blank?
        end
      when 'create'
        #Should not do anything if an building is created (they don't have a phone yet)
      when 'destroy'
        #Building destroys are handled in location destroys.
    end

  end
  
  def process_phone(phone_change)
    phone = phone_change.revision

    if phone.has_master_relation_with_primary_phone_location? 
      case phone_change.action
        when 'create'
          #should not do anything for phone creations
        when 'destroy'
          #phone deletions are handled in master_relation changes
        when 'update'

          @phone_changes_and_function_codes[phone.id] = 'U'
      end
    end
  end
    
  def generate_changes_file
    E911TableFormatter.new(@phone_changes_and_function_codes, active_911_file_path)
    active_911_file_path
  end
     
  def new_change_for_911?(new_item)
    if new_item.instance_of? MasterRelation
      if !(@master_relation_changes.map(&:id).include?(new_item.id)) && new_item.location_id && new_item.phone_id && !(@processed_phones.map(&:id).include?(new_item.phone_id)) && new_item.location.building.city == 'TALLAHASSEE'
        confirmed_new_911_changes = E911Change.find(:all, :conditions => ['id = ?', new_item.id])
        unless confirmed_new_911_changes.size > 1
          confirmed_new_911_changes[0]
        end
      end
    else
      unless new_item.master_relations.empty? 
        updated_master_relations = new_item.master_relations.select {|ml| ml.location_id && ml.phone_id && ml.primary_phone_location}
        updated_master_relations.map {|updated_master_relation| new_change_for_911?(updated_master_relation)}.compact[0]
      end
    end
  end
          
  def send_file_to_911(file_path_to_send)

    if File.exist?(file_path_to_send) && File.size(file_path_to_send) > 0
      begin
        require 'net/sftp'
        puts 'Connecting to AK Associates...'
        Net::SFTP.start(APP_CONFIG['akassoc']['ftp'],APP_CONFIG['akassoc']['username'], :password => APP_CONFIG['akassoc']['password']) do |sftp|

          puts 'Uploading daily update file...'
          sftp.upload!(file_path_to_send, remote_file_path(file_path_to_send))

        end
        puts "Backing up daily update file..."
        FileUtils.mv(file_path_to_send, archive_911_file_path)
      rescue
        message = "Unable to send data to #{APP_CONFIG['akassoc']['ftp']}:22/#{APP_CONFIG['akassoc']['dir']}"
        message += "<br /> We did left the changes that were not sent, marked as 'sent_to_911 = 0' in the audits table"
        AuditMailer.deliver_cron_rake_message("")
        return false
      end
    end
    puts 'No changes needed to be sent to 911.'
    return true
  end
  
  def active_911_file_path
    file_name = APP_CONFIG['active_911_file_name'].gsub(/date/, Date.today.strftime('%Y-%m-%d'))
    file_path = APP_CONFIG['active_911_file_path'].gsub(/RAILS_ROOT/, RAILS_ROOT)
    file_path+'/'+file_name
  end
  
  def archive_911_file_path
    APP_CONFIG['archive_911_files'].gsub(/RAILS_ROOT/, RAILS_ROOT)
  end
  
  def remote_file_path(file_path_to_send)
    APP_CONFIG['akassoc']['dir'] +'/'+ File.basename(file_path_to_send)
  end
  
  def change_column_headers(file_path)
    file_stats = File::Stat.new(file_path)
    @reports << "ERROR: The file #{file_path} was not writable." if !file_stats.writable?
    @reports << "ERROR: The file #{file_path} is stale. It was created before #{Date.today}" if file_stats.atime > Date.yesterday
    @modified_headers_count = 0
    pf_read = FasterCSV.read(file_path,{:headers => :first_row, :header_converters =>
      lambda do |current_header| 
        header_replacements = {
      'Location County' => 'county_id',
      'Location Room' => 'location_room',
      'Location State' => 'location_state',
      'Location Bldg' => 'building_name',
      'Location Addr' => 'location_address',
      'Vacancy Ind' => 'vacancy_indicator',
      'Last Name' => 'last_name',
      'First Name' => 'first_name',
      'Middle Name' => 'middle_name',
      '411 Phone Number' => 'four_11_phone',
      '411 Phone Number Ext' => 'four_11_phone_number_ext',
      'Location Bldg Num' => 'location_bldg_number',
      'Pos Num' => 'pos_num',
      'Olo Code' => 'olo_code',
      'Location City' => 'location_City',
      'Email Address' => 'email_address',
      'Emp Id' => 'employee_id',
      'Contractor Flag' => 'contractor_flag',
      'Name Suffix' => 'name_suffix',
      'Mail Zip' => 'mail_zip',
      'CONFID, SWORN, EXEMPT OR PROTECT FLAG' => 'confidential',
      'User Id' => 'people_first_user'
        }
        new_header = ''
        header_replacements.each do |old_header, replacement_header|
          if current_header =~ Regexp.new(old_header) 
            new_header = replacement_header
            @modified_headers_count += 1
          end
        end
        if new_header.blank?
          current_header
        else
          new_header
        end
      end
    })
    @reports << "ERROR: Only #{@modified_headers_count} of 22 column headers were changed when writing to the file #{file_path}" unless @modified_headers_count == 22
    begin
      pf_write = File.open(file_path, 'w')
      pf_write.write pf_read.to_csv
      pf_write.close
    rescue
      @reports << "ERROR: There was a problem writing column header names to the file #{file_path}"
    end
    puts @reports
  end
  
end

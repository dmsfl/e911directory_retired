require 'fixed_text'
Phone.class_eval do
  acts_as_reportable
  attr_accessor :function_code
  def get_master_relation_primary_phone_location
    master_relations.select {|ml| ml.primary_phone_location}
  end
end



class E911TableFormatter
  attr_accessor :table_created
  def initialize(phone_and_function_code, file_path)
    file_to_send = File.open(file_path, 'w')
    
    leon_county_id = County.first(:conditions => "name = 'LEON'").id.to_s

    new_rows = get_full_phone_information(phone_and_function_code)
    new_rows.each do |row|
       row.function_code = phone_and_function_code[row.id] unless row.function_code
       next if row.function_code != 'D' && row.county_id != leon_county_id
       fill_in_bogus_info(row) if row.function_code == 'D'
       formatted = [[:function_code ,         row.function_code                   ],
                    [:npa ,                   row.area_code                       ],
                    [:calling_number ,        row.main_number                     ],
                    [:house_number ,          '%10s'  % row.street_number         ],
                    [:house_number_suffix ,   '%4s'   % row.street_number_suffix  ],
                    [:prefix_directional ,    '%2s'   % row.prefix_directional    ],
                    [:street_name ,           '%60s'  % row.street_name           ],
                    [:street_suffix ,         '%4s'   % row.street_suffix         ],
                    [:post_directional ,      '%2s'   % row.post_directional      ],
                    [:community_name ,        '%32s'  % row.city                  ],
                    [:state ,                 '%2s'   % row.state                 ],
                    [:location ,              '%60s'  % location_in_building(row) ],
                    [:customer_name ,         '%32s'  % employee_full_name(row)   ],
                    [:class_of_service ,      '%1s'   % '5'                       ],
                    [:type_of_service ,       '%1s'   % ''                        ],
                    [:exchange ,              '%4s'   % ''                        ],
                    [:esn ,                   '%5s'   % ''                        ],
                    [:main_npa ,              '%3s'   % row.area_code             ],
                    [:main_number ,           '%7s'   % row.main_number           ],
                    [:order_number ,          '%10s'  % ''                        ],
                    [:extract_date ,          Date.today.strftime('%m%d%y')       ],
                    [:county_id ,             '%4s'   % '073'                     ],
                    [:company_id ,            '%5s'   % 'FLDMS'                   ],
                    [:source_id ,             '%1s'   % 'C'                       ],
                    [:zip_code ,              '%5s'   % row.zip_code_5            ],
                    [:zip_code_4 ,            '%4s'   % row.zip_code_4            ],
                    [:general_use ,           '%11s'  % ''                        ],
                    [:customer_code ,         '%3s'   % ''                        ],
                    [:comments ,              '%30s'  % "#{row.name} phone: #{row.comment}"[0..29]],
                    [:x_coordinate ,          '%9s'   % ''                        ],
                    [:y_coordinate ,          '%9s'   % ''                        ],
                    [:z_coordinate ,          '%5s'   % ''                        ],
                    [:cell_id ,               '%6s'   % ''                        ],
                    [:sector_id ,             '%1s'   % ''                        ],
                    [:tar_code ,              '%6s'   % ''                        ],
                    [:reserved ,              '%21s'  % ''                        ],
                    [:alt_num ,               '%10s'  % ''                        ],
                    [:expanded_extract_date , Date.today.strftime('%Y%m%d')       ],
                    [:nena_reserved ,         '%81s'  % ''                        ],
                    [:data_provider_id ,      '%5s'   % ''                        ],
                    [:reserved_2 ,            '%31s'  % ''                        ],
                    [:record_end_delimiter,   '*'                                 ]
                   ]
        file_to_send.puts formatted.collect {|key_value| key_value[1]}.join('')
    end
    file_to_send.close
    @table_created = true
  end

  def location_in_building(row)
      f = row.floor.nil? ? nil : "Flr #{row.floor}"
      r = row.room.nil? ? nil : "Rm #{row.room}"
      bi = row.building_identifier.nil? ? nil : "Building #{row.building_identifier}"
      [bi, f, r].compact.join(', ')
  end
  
  def get_full_phone_information(phone_and_function_code)
    hashed_deleted_phones = Hash.new
    hashed_existing_phones = Hash.new

    deleted_phones = phone_and_function_code.select {|k,v| !Phone.exists?(k) && v == 'D'}
    deleted_phones.each { |key_value| hashed_deleted_phones[key_value[0]] = key_value[1] }

    existing_phones = phone_and_function_code.select {|k,v| !deleted_phones.include?(k)}
    existing_phones.each { |key_value| hashed_existing_phones[key_value[0]] = key_value[1] }
    unless hashed_existing_phones.empty?
      hashed_existing_phones = Phone.find(:all, 
        :select => 'phones.id, phones.area_code, phones.main_number, phones.comment, phone_categories.name, 
                    locations.room, locations.floor, locations.building_identifier,
                    buildings.street_number, buildings.street_number_suffix, buildings.prefix_directional, buildings.street_name, 
                    buildings.street_suffix, buildings.post_directional, buildings.city, buildings.state, 
                    buildings.zip_code_5, buildings.zip_code_4,buildings.county_id,
                    employees.first_name, employees.middle_name, employees.last_name, employees.active_flag',
        :joins  => 'left outer join master_relations on master_relations.phone_id = phones.id and master_relations.primary_phone_location = 1
                    left outer join locations on master_relations.location_id = locations.id 
                    left outer join phone_categories on phones.phone_category_id = phone_categories.id 
                    left outer join buildings on locations.building_id = buildings.id
                    left outer join employees on master_relations.employee_id = employees.id',
        :conditions => "phones.id in (#{hashed_existing_phones.keys.join(',')})")
    else
      hashed_existing_phones = []
    end
    hashed_missing_phones = get_missing_phones(hashed_deleted_phones)

    hashed_existing_phones + hashed_missing_phones
  end

  def get_missing_phones(deleted_phones)
    deleted_phones.collect do |phone_id, function_code|
      audit = AuditE911.find(:first, :conditions => "auditable_type = 'Phone' and auditable_id = #{phone_id}", :order => 'created_at DESC')
      FullPhoneAttributes.new(audit.revision.attributes.merge({'function_code' => function_code}))
    end
  end

  def employee_full_name(row)
    if row.active_flag.to_s == '1'
      [row.first_name, 
       row.middle_name, 
       row.last_name].
      compact.collect {|e| e.capitalize}.join(' ')
    else 
      'Recently Moved'
    end
  end
end

def fill_in_bogus_info(row)
  row.street_number ='4050'
  row.street_name = 'ESPLANADE'
  row.street_suffix= 'WAY'
  row.city = 'TALLAHASSEE'
  row.state = 'FL'
end

class FullPhoneAttributes
  attr_accessor   :area_code, :main_number, :comment, :name, :room, :floor, :building_identifier,
                  :street_number, :street_number_suffix, :prefix_directional, :street_name, 
                  :street_suffix, :post_directional, :city,:state, 
                  :zip_code_5, :zip_code_4, :function_code, :first_name, :last_name, :middle_name,
                  :active_flag, :county_id
                  
  def initialize(args)
    args.each do |k,v|
      eval("self.#{k.to_s} = \"#{v.to_s}\"") if respond_to?(k.to_sym)
    end
  end
end

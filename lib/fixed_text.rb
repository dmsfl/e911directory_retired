#Renders a text width-delimited table with no header.                         
class FixedText < Ruport::Formatter::Text                                     
                                                                              
  renders :fixed_text, :for => Ruport::Controller::Table                      
                                                                              
  def build_table_header                                                      
                                                                              
    return unless should_render_column_names?                                 
                                                                              
    c = data.column_names.enum_for(:each_with_index).map { |f,i|              
      f.to_s.ljust(options.max_col_width[i])                                  
    }                                                                         
                                                                              
    output << fit_to_width("#{c.join('')}\n")                                 
  end                                                                         
                                                                              
  def build_table_body                                                        
    return if data.empty?                                                     
                                                                              
    calculate_max_col_widths unless options.max_col_width                     
                                                                              
    data.each { |row| build_row(row) }                                        
                                                                              
  end                                                                         
                                                                              
  def build_row(data = self.data)                                             
    data.enum_for(:each_with_index).inject(line=[]) { |s,e|                   
      field,index = e                                                         
      if options.alignment.eql? :center                                       
        line << field.to_s.center(options.col_width[index])                   
      elsif options.alignment.eql? :left                                      
        line << field.to_s.ljust(options.col_width[index])                    
      else                                                                    
        align = field.is_a?(Numeric) ? :rjust : :ljust                        
        line << field.to_s.send(align, options.col_width[index])              
      end                                                                     
  }                                                                           
                                                                              
  output << fit_to_width("#{line.join('')}*\n")                               
  end                                                                         
end                                                                           
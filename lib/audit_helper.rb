
# AuditHelper helps figure out which records should be sent to 911
# It is included in Phone, Location, and Employee 
# Note that for this plugin to work, the class in which it is included should have_many :master_relations 
module AuditHelper

  class TooMany911LocationsError < StandardError
  end

  def has_master_relation_with_primary_phone_location?
    master_relations.detect { |ml|  ml.primary_phone_location && ml.location_id && ml.phone_id} ? true : false
  end
  
  def select_associated_911_phones
    nine_11_master_relations = master_relations.select {|ml| ml.primary_phone_location && ml.location_id && ml.phone_id}
    if self.instance_of?(Phone) && nine_11_master_relations.size > 1
        raise TooMany911LocationsError
    else
        nine_11_master_relations.map(&:phone)
    end
  end
  
end
class Phone
  acts_as_audited
end

module PhoneSpecHelper
  def valid_phone_attributes
    {
      :area_code => '222',
      :main_number => '1234567',
      :phone_category  => phone_categories(:voice),
      :comment => 'HP 353'
    }
  end
end
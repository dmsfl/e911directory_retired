require 'CSV'

module LocationParser
  def LocationParser.parse_file(file_path, output = nil)
    reader = CSV.open(file_path, 'r')
    header = reader.shift
    all_data = []
    puts "Starting to read: #{file_path}"
    reader.each do |row|
      all_data += [parse_row(row, header)]
    end
    # Used for testing output
    if !output.nil?
      File.delete(output)
      file = File.new(output, 'w')
      all_data.each do |row|
        file.puts(row)
      end
      file.close
    end
    all_data
  end
  
  def LocationParser.parse_row(row, header)
    data = {}
    row.each_with_index do |cell, j| 
      case header[j].to_s.strip
        when 'room_suite' 
          if !cell.to_s.nil?
            rs = get_room_suite(cell.to_s)
            data.merge!(rs)
          end
        else
          data[header[j].to_s.strip] = cell.to_s.strip 
      end
    end
    data
  end
  
  def LocationParser.get_room_suite(room_suite)
    parsed_columns = {}
    s = room_suite.strip
    if s =~ /\,/
      parsed_columns['building_identifier'] = s.match(/Bldg (\w+)/)[1] rescue ""
      parsed_columns['room'] = s.match(/, (\w+)$/)[1] rescue "" unless !parsed_columns['room'].nil?
      parsed_columns['floor'] = s.match(/Flr (\w+)/)[1] rescue ""
    else
      parsed_columns['room'] = s
    end
    
    return parsed_columns
  end
end

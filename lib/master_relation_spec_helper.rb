class MasterRelation
  acts_as_audited
end


module MasterRelationSpecHelper
  def valid_master_relation_attributes
    {
      :employee => employees(:quentin),
      :phone => phones(:voice),
      :location  => locations(:one)
    }
  end
end
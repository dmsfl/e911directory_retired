class Location
  acts_as_audited
end

module LocationSpecHelper
  def valid_location_attributes
    {
      :floor => '2',
      :room => '215z8',
      :building_identifier  => 'A',
      :building_id => 2,
    }
  end
end
require 'CSV'

module PhoneParser
  def PhoneParser.parse_file(file_path, output = nil)
    reader = CSV.open(file_path, 'r')
    header = reader.shift
    all_data = []
    puts "Starting to read: #{file_path} for phone parsing"
    reader.each do |row| 
      all_data += parse_row(row, header) 
    end

    # Used for testing output
    #output = './phones.txt'
    if !output.nil?
      File.delete(output) if File.exists?(output)
      file = File.new(output, 'w')
      all_data.each do |row|
        file.puts(row)
      end
      file.close
    end
    all_data
  end
  
  def PhoneParser.parse_row(row, header)
    business = {}
    fax = {}
    row.each_with_index do |cell, j| 
      case header[j].to_s.strip
        when 'business_phone' 
          if !cell.to_s.strip.nil?
            business = get_business_phone(cell.to_s)
          end
        when 'fax_number'
          if !cell.to_s.strip.nil?
            fax =  get_fax_phone(cell.to_s)
          end
      end
    end
    [business, fax].compact
  end
  
  def PhoneParser.get_business_phone(phone)
    parsed_columns = {}
    if matches = phone.match(/\((\d{3})\) (\d{3})-(\d{4})/)
      parsed_columns['area_code'] = matches[1]
      parsed_columns['main_number'] = matches[2]+matches[3]
      parsed_columns['extension'] = ''
      parsed_columns['phone_category'] = 'Voice'
    else
      parsed_columns = nil
    end
    return parsed_columns
  end
  
  def PhoneParser.get_fax_phone(phone)
    parsed_columns = {}
    if matches = phone.match(/\((\d{3})\) (\d{3})-(\d{4})/)
      parsed_columns['area_code'] = matches[1]
      parsed_columns['main_number'] = matches[2]+matches[3]
      parsed_columns['extension'] = ''
      parsed_columns['phone_category'] = 'Fax'
    else
      parsed_columns = nil
    end
    return parsed_columns
  end
end

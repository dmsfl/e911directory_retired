require 'fastercsv'
require 'legacy_migrations'
require 'message'

class IpPhoneUpdater
  def initialize
    @message = Message.instance
    @message.add_header
    @updates = FasterCSV.open("#{Rails.root}/#{APP_CONFIG[:ip_phone_file]}") if File.exist?("#{Rails.root}/#{APP_CONFIG[:ip_phone_file]}")
  end
  def run
    if @updates
      MasterRelation.audit_as(User.find_by_login('795871')) do 
        update_or_create_buildings
        update_or_create_locations
        update_or_create_phones
        status_report = update_or_create_master_relations
      end

      AuditMailer.deliver_user_friendly_ip_phone_changes status_report
    else
      @message.add_sentence "Updates were not necessary."
    end
  end

  def results
    @message.to_html
  end

  private

  def update_or_create_buildings
    update_from @updates, :to => Building, :source_type => :csv do
      based_on do
        street_number        == from['bldg_num']
        street_number_suffix == from['bldg_num_suffix']
        street_name          == from['street']
        street_suffix        == from['suffix']
        post_directional     == from['post_dir']
        city                 == from['city']
        zip_code_5           == from['zip'][0..4]
        state                == from['state']
      end

      from  :bldg_num,        :to => :street_number 
      from  :bldg_num_suffix, :to =>  :street_number_suffix 
      from  :street,          :to =>  :street_name 
      from  :suffix,          :to => :street_suffix 
      from  :post_dir,        :to => :post_directional 
      from  :city,            :to => :city 
      from  :state,           :to => :state 
      from  :zip,             :to => :zip_code_5 do |zip| 
        zip[0..4]
      end
      from  :zip,             :to => :zip_code_4 do |zip| 
        zip[5..8]
      end
    end
  end

  def update_or_create_locations
    update_from @updates, :to => Location, :source_type => :csv do

      based_on do
        building.street_number        == from['bldg_num']
        building.street_number_suffix == from['bldg_num_suffix']
        building.street_name          == from['street']
        building.street_suffix        == from['suffix']
        building.post_directional     == from['post_dir']
        building.city                 == from['city']
        building.state                == from['state']
        building.zip_code_5           == from['zip'][0..4]
        room                          == from['room']
      end

      from :room, :to => :room
      from :from_record, :to => :building do |from_record|

        buildings_matched = Building.find(:all) do 
          street_number        == from_record['bldg_num']
          street_number_suffix == from_record['bldg_num_suffix']
          street_name          == from_record['street']
          street_suffix        == from_record['suffix']
          post_directional     == from_record['post_dir']
          city                 == from_record['city']
          state                == from_record['state']
          zip_code_5           == from_record['zip'][0..4]
        end
        if buildings_matched.size == 1
          buildings_matched[0]
        else
          nil
        end
      end
    end
  end

  def update_or_create_phones
    update_from @updates, :to => Phone, :source_type => :csv do
      based_on do
        area_code   == from['voip'][0..2]
        main_number == from['voip'][3..9]
      end

      from :voip, :to => :area_code do |voip|
        voip[0..2]
      end
      from :voip, :to => :main_number do |voip|
        voip[3..9]
      end
    end
  end

  def update_or_create_master_relations
    update_from @updates, :to => MasterRelation, :source_type => :csv do
      based_on do
        phone.area_code   == from['voip'][0..2]
        phone.main_number == from['voip'][3..9]
      end

      from :from_record, :to => :phone do |from|
        phone = Phone.find(:all, :conditions => {:area_code => from['voip'][0..2],
                                     :main_number => from['voip'][3..9]}
                           )
        if phone.size == 1
          phone
        else
          nil
        end
      end

      from :from_record, :to => :location do |from_record|
        Location.first do 
          building.street_number        == from['bldg_num']
          building.street_number_suffix == from['bldg_num_suffix']
          building.street_name          == from['street']
          building.street_suffix        == from['suffix']
          building.post_directional     == from['post_dir']
          building.city                 == from['city']
          building.state                == from['state']
          building.zip_code_5           == from['zip'][0..4]
          room                          == from['room']
        end
      end

      from :voip_name, :to => :employee do |voip_name|
        first_last = voip_name.split(',')
        first = first_last[1].strip unless first_last[1].blank?
        last  = first_last[0].strip unless first_last[0].blank?
        matched_employees = Employee.find(:all, 
                                          :conditions => {:first_name => first,
                                                          :last_name => last})
        if matched_employees.size != 1
          nil
        else
          matched_employees[0]
        end
      end
    end
  end

end

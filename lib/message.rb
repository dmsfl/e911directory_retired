require 'singleton'
require 'active_support'
class Message < Array
  include Singleton

  def add_sentence(sentence)
    self.<< sentence
  end
  def to_html
    add_footer
    join '<br />'
  end

  def add_header(*args)
    unless @header_done
      start_processing_datetime = Time.now()
      self.<< "Begin Processing: #{start_processing_datetime}"
      self.<< "--------------------------------"
      @header_done = true
    end
  end
  def add_footer(*args)
    unless @footer_done
      end_processing_datetime = Time.now()
      self.<< "--------------------------------"
      self.<< "End Processing: #{end_processing_datetime}"
      @footer_done =true 
    end

  end
end

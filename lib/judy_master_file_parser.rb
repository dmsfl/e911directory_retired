require 'CSV'

module JudyMasterFileParser
  def JudyMasterFileParser.parse_file(file_path, output = nil)
    reader = CSV.open(file_path, 'r')
    header = reader.shift
    all_data = []
    puts "Starting to read: #{file_path} for judy file parsing"
    reader.each do |row| 
      all_data += parse_row(row, header) 
    end

    # Used for testing output
    #output = './phones.txt'
    if !output.nil?
      File.delete(output) if File.exists?(output)
      file = File.new(output, 'w')
      all_data.each do |row|
        file.puts(row)
      end
      file.close
    end
    all_data
  end
  
  def JudyMasterFileParser.parse_row(row, header)
    fax = {}
    row.each_with_index do |cell, j| 
      if !cell.to_s.strip.nil?
        case header[j].to_s.strip
          when 'First Name' 
              first_name = cell.to_s.strip || ''
          when 'Last Name'
              last_name =  cell.to_s.strip || ''
          when 'Middle Name'
              middle_name =  cell.to_s.strip || ''
        end
      end
    end
    [first_name, middle_name, last_name]
  end
  
  def JudyMasterFileParser.get_business_phone(phone)
    parsed_columns = {}
    if matches = phone.match(/\((\d{3})\) (\d{3})-(\d{4})/)
      parsed_columns['area_code'] = matches[1]
      parsed_columns['main_number'] = matches[2]+matches[3]
      parsed_columns['extension'] = ''
      parsed_columns['phone_category'] = 'Voice'
    else
      parsed_columns = nil
    end
    return parsed_columns
  end
  
  def PhoneParser.get_fax_phone(phone)
    parsed_columns = {}
    if matches = phone.match(/\((\d{3})\) (\d{3})-(\d{4})/)
      parsed_columns['area_code'] = matches[1]
      parsed_columns['main_number'] = matches[2]+matches[3]
      parsed_columns['extension'] = ''
      parsed_columns['phone_category'] = 'Fax'
    else
      parsed_columns = nil
    end
    return parsed_columns
  end
end

module AuthenticatedTestHelper
  # Sets the current user in the session from the user fixtures.
  def login_as(user)
    if @request
      @request.session[:user_id] = user ? (user.is_a?(User) ? user.id : users(user).id) : nil
    else
      session[:user_id] = user ? (user.is_a(User) ? user.id : users(user).id) : nil
    end
  end

  def authorize_as(user)
    @request.env["HTTP_AUTHORIZATION"] = user ? ActionController::HttpAuthentication::Basic.encode_credentials(users(user).login, 'monkey') : nil
  end
  
  # rspec
  def mock_user
    user = mock_model(User, :id => 1,
      :login  => 'user_name',
      :to_xml => "User-in-XML", :to_json => "User-in-JSON", 
      :name => 'U. Surname',
      :errors => [])
    user
  end  
end

module AuthenticatedSystem
  def current_user
    if self.is_a?(ApplicationController) || self.is_a?(Spec::Rails::Example::ControllerExampleGroup)
      @current_user ||= (login_from_session || login_from_basic_auth || login_from_cookie) unless @current_user == false
    else
      @current_user = nil
    end
  end
end



module EmployeesHelper
  def primary_employee_location(ml, *args)
    unless ml.location.blank?
      checked = ml.primary_employee_location ? true : false
      yes_no_image_radio(checked, 'primary_employee_location', ml.id, args.inject({}) {|h,i| h.merge i})
    else
      ''
    end
  end
  def primary_phone_for_employee(ml, *args)
    unless ml.phone.blank?
      checked = ml.primary_phone_for_employee ? true : false
      yes_no_image_radio( checked, 'primary_phone_for_employee', ml.id, args.inject({}) {|h,i| h.merge i})
    else
      ''
    end
  end
  def used_for_411(ml, *args)
    if !ml.phone.blank? 
      checked = ml.used_for_411 ? true : false
      if !@employee.confidential.blank?
        image_tag('no_bk.png', :alt => 'Not available for People First')
      else
        yes_no_image_radio( checked, 'used_for_411', ml.id, args.inject({}) {|h,i| h.merge i})
      end
    else
      ''
    end
  end

  def used_for_411_images(ml, *args)
    if !ml.phone.blank? 
      if !@employee.confidential.blank?
        yes_no_image(false)
      else
        yes_no_image((ml.phone.id && ml.used_for_411))
      end
    else
      ''
    end
  end

  def confidential_flag(employee)
    if !employee.confidential.blank?
      'Yes'
    else
      'No'
    end
  end

  def confidential_row(employee)
    !employee.confidential.blank? ? ' confidential' : ''
  end

  def confidential(employee)
    if !employee.confidential.blank?
      'Yes.<br />'+
      'This person will not appear in the 411 directory.'
    else
      'Not confidential'
    end
  end

  def phone_category(employee)
    employee.primary_labels[:phone_category]
  end

  def employee_phone(employee, admin)
    unless employee.primary_labels[:phone].blank?
      if admin
        link_to employee.primary_labels[:phone], edit_phone_path(employee.primary_labels[:phone_id])
      else
        link_to employee.primary_labels[:phone], phone_path(employee.primary_labels[:phone_id])
      end
    end
  end

  def employee_link(employee, admin)
    if admin
      link_to employee.full_name, edit_employee_path(employee)
    else
      link_to employee.full_name, employee_path(employee)
    end
  end

  def add_remove_location_link(ml)
    if !ml.location.blank? 
      link_to ml.location.to_label, edit_master_relation_path(ml.id, {:component => 'location', :from => 'employee'}), {:class => 'mid_ajax_dialog'}
    else
      link_to '<span>None</span>', edit_component_path(:id => ml.id, :from => 'employee', :component => 'location'), {:class => 'mid_ajax_dialog'}
    end
  end
  
  def add_remove_phone_link(ml)
    if !ml.phone.nil?
      link_to ml.phone.to_label, edit_master_relation_path(ml.id, {:component => 'phone', :from => 'employee'}), {:class => 'simple_ajax_dialog'}
    else
      link_to '<span>None</span>', edit_component_path(:id => ml.id, :component => 'phone', :from => 'employee'), {:class => 'simple_ajax_dialog'}
    end
  end
end

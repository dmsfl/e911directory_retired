module CampusesHelper
  def select_campus
    select :building, :campus_id, Campus.all.collect {|c| [c.name, c.id]}, {:include_blank => true}, {:style => 'width:265px'}
  end
  def select_county
    select :building, :county_id, County.all.collect {|c| [c.name, c.id]}, {:include_blank => true}
  end
end

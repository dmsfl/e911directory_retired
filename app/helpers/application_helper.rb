# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  
  def autocomplete(model,field,value)
    conditions = ["#{model.tabelize}.#{field} LIKE ?","%#{value}%"]
    @items = model.singularize.classify.constantize.find(:all, :select => [field], :conditions => conditions, :group => field ).map {|record| record.send(field)}
    render :partial => 'shared/autocomplete', :layout => 'blank'
  end
  
def unsent_changes
  if @unsent_changes_count && @unsent_changes_count > 0
    '<li id="unsent_changes">'+
    link_to("<span class='highlight'>#{@unsent_changes_count} Unsent Changes</span>", unsent_changes_path, :class => 'wide_ajax_dialog')+
    '</li>'    
  end
end

  def yes_no_image(truthiness = false, *args)
    if truthiness
      image_tag 'yes.png', args.inject({}) {|m,a| m.merge(a)}
    else
      image_tag 'no.png', args.inject({}) {|m,a| m.merge(a)}
    end
  end
  def yes_no_image_radio(checked = false, group_name = 'default_group', item_id = '1', *args)
    result = ''
    result += radio_button_tag(group_name, item_id, checked, args.inject({:class => 'hide'})  {|m,a| m.merge(a)})
    result += yes_no_image(checked, {:rel => "#{group_name}_#{item_id}"})
  end

  def html_attributes_for_building_locations(html_results)
    html_results.merge({
     :id => [html_results[:id], 'new_search'].compact.join(' '),
     :class => [html_results[:class], 'initially_hidden'].compact.join(' '),
     :style => [html_results[:style], 'display:none;'].compact.join(' ')
    })
  end

end

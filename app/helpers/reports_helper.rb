module ReportsHelper
  include AuditsHelper
  
  def select_nightlies(existing_nightlies)
    select 'nightly_change','date', existing_nightlies.map {|ec| [ec.strftime("%m-%d-%Y"), reports_path(:nightly_changes => ec)]}
  end

  def link_to_employee_edit(record)
    if employee_id = record[:pf_employee_id] || record[:c_employee_id]
      if emp = Employee.find(:first, :conditions => ["people_first_id = ?",employee_id])
        link_to emp.full_name, {:controller => 'employees', :action => 'edit', :id => emp.id}
      else
        last = record[:pf_last_name] || record[:c_last_name]
        first = record[:pf_first_name] || record[:c_first_name]
        middle = record[:pf_middle_name] || record[:c_middle_name]
        [last, middle, first].compact.join(', ')
      end
    end
  end
end

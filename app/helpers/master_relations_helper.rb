module MasterRelationsHelper
  def location_finder(element_name_of_selected_location = 'location', html_results = {})
    render :partial => 'find_locations',
      :locals => {
               :destination => {:controller => 'master_relations', 
                                :action => 'location_search_results'}, 
               :element_with_location_id => element_name_of_selected_location,
               :html_results => html_results}
  end
  def building_select_campus
    select :search, :campus_id, Campus.all.collect {|c| [c.name, c.id]}, {:include_blank => true}, {:style => 'width:265px'}
  end
  def select_building_link building
    link_to( building.name, 
      {:action => 'add_location_search_results', 
        :location_query => 'true', 
        :search => {:building_id => building.id}}, 
      :class => 'custom_replace_results hide_id_building_query', 
      :rel => "#search_results")
  end
  def sort_link_for text, field
    link_to( text, add_location_search_results_path(@order_params[field]), 
      :class => 'custom_replace_results', 
      :rel => '#search_results')
  end

  def edit_component_link(ml_id, component_to_change, *args)
    if component_to_change == 'location'
      classes = 'fancy_button ui-state-default ui-corner-all mid_ajax_dialog'
    else
      classes = 'fancy_button ui-state-default ui-corner-all mid_ajax_dialog'
    end
    link_to "Change #{component_to_change}", 
      edit_component_path(ml_id, component_to_change),
      :class => classes
  end
  def delete_component_link(ml_id, component_to_remove)
    link_to "Remove #{component_to_remove}", 
      delete_component_path(ml_id, component_to_remove),
      :class => 'fancy_button ui-state-default ui-corner-all confirm_action_dialog'
  end
end

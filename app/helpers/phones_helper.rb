module PhonesHelper
  def primary_employee_for_phone(ml, *args)
    unless ml.employee.blank?
      checked = ml.primary_employee_for_phone ? true : false
      yes_no_image_radio(checked, 'primary_employee_for_phone', ml.id)
    else
      ''
    end
  end
  def is_prim_emp_for_phone?(ml)
    !ml.employee.blank? && ml.primary_employee_for_phone
  end
  def is_prim_phone_location?(ml)
    !ml.location.blank? && ml.primary_phone_location
  end
  def add_remove_employee_link_from_phone(ml)
    if !ml.employee.nil?
      link_to employee_name(ml), edit_master_relation_path(ml.id, {:component => 'employee', :from => 'phone'}), {:class => 'mid_ajax_dialog'}
    else
      link_to '<span>None</span>', edit_component_path(:id => ml.id, :from => 'phone', :component => 'employee'), {:class => 'mid_ajax_dialog'}
    end
  end
  
  def primary_phone_location(ml, *args)
    if !ml.location.blank?
      checked = ml.primary_phone_location ? true : false
      result = ''
      result += radio_button_tag 'primary_phone_location', ml.id, checked, args.inject({:class => 'hide'})  {|m,a| m.merge(a)} 
      result += yes_no_image(checked, {:rel => "primary_phone_location_#{ml.id}"})
    else
      ''
    end
  end
  
  def add_remove_location_link_from_phone(ml)
    if !ml.location.blank? 
      link_to location_label(ml), edit_master_relation_path(ml.id, {:component => 'location', :from => 'phone'}), {:class => 'mid_ajax_dialog'}
    else
      link_to '<span>None</span>', edit_component_path(:id => ml.id, :from => 'phone', :component => 'location'), {:class => 'mid_ajax_dialog'}
    end
  end
  
  def employee_name(ml)
    ml.employee.blank? ?  '' :ml.employee.full_name
  end
  
  def primary_name(phone)
    phone.primary_labels[:employee] || ''
  end

  def location_label(ml)
    ml.location.blank? ? '' :  ml.location.to_label
  end

   def phone_employee(phone, admin)
    unless phone.primary_labels[:employee].blank?
      if admin
        link_to phone.primary_labels[:employee], edit_employee_path(phone.primary_labels[:employee_id])
      else
        link_to phone.primary_labels[:employee], employee_path(phone.primary_labels[:employee_id])
      end
    end
  end

  def phone_link(phone, admin)
    if admin
      link_to phone.to_label, edit_phone_path(phone)
    else
      link_to phone.to_label, phone_path(phone)
    end
  end
end

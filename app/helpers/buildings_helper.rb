module BuildingsHelper
  def directionals
    Building.DIRECTIONALS.collect {|e| [e.strip.upcase,e.strip.upcase]}
  end
  def street_abbreviations
    StreetAbbreviation.all.collect {|name_pair| ["#{name_pair.name} (#{name_pair.standard_abbreviation})", name_pair.standard_abbreviation]}
  end
  def create_or_save(form)
    form.new_record? ? 'Create' : 'Save'
  end
end

require 'set'

module AuditsHelper
  def item_label(audit, four_11_only = true)
    case 
    when audit.ancestors.last && audit.auditable_type != 'MasterRelation'
      audit.action.capitalize + '<br />' + audit.ancestors.last.revision.to_label
    when audit.auditable_type == 'MasterRelation'
      parent = audit.parent
    	items_in_result = which_item_labels(audit, parent, four_11_only)
      result = []
      #Assigned a new relation
      if items_in_result.include?('phone')
        begin
          result << 'Phone: <br />'+ Phone.find(parent.phone_id).to_label  
        rescue
          result << 'Recently deleted phone. Will likely show up in one of the rows below.'
        end
      end
      
      if items_in_result.include?('employee')
        begin
          result << 'Employee: <br />'+ Employee.find(parent.employee_id).to_label 
        rescue
          result << 'Recently deleted employee. Will likely show up in one of the rows below.'
        end
      end
      if items_in_result.include?('location')
        begin
          result << 'Location <br />'+ Location.find(parent.location_id).to_label 
        rescue
          result << 'Recently deleted location. Will likely show up in one of the rows below.'
        end
      end
      result.join('<br />')
    else
      audit.action.capitalize
    end
  end
  def which_item_labels(audit, parent, four_11_only = true)
  	  result = []
  	  revision = audit.revision
  	  changes = audit.changes
      #Assigned a new relation
      result << 'phone'    if changes['location_id'] && !changes['phone_id']    && parent.phone_id    && !changes['location_id'][1].blank?
      result << 'phone'    if changes['employee_id'] && !changes['phone_id']    && parent.phone_id && !changes['employee_id'][1].blank?
      result << 'employee' if changes['phone_id']    && !changes['employee_id'] && parent.employee_id && !changes['phone_id'][1].blank?
      result << 'employee' if changes['location_id'] && !changes['employee_id'] && parent.employee_id && !changes['location_id'][1].blank?
  
      #Removed an existing relation
      result << 'location'    if changes['location_id'] && !changes['phone_id']    && revision.phone_id    && changes['location_id'][1].blank?
      result << 'employee'    if changes['employee_id'] && !changes['phone_id']    && revision.phone_id    && changes['employee_id'][1].blank?
      result << 'phone' if changes['phone_id']    && !changes['employee_id'] && revision.employee_id && changes['phone_id'][1].blank?
      result << 'employee' if changes['location_id'] && !changes['employee_id'] && revision.employee_id && changes['location_id'][1].blank?
      result << 'phone'    if changes['phone_id']    && !changes['location_id'] && revision.location_id && changes['phone_id'][1].blank?
      result << 'location'    if changes['employee_id']    && !changes['location_id'] && revision.location_id && changes['employee_id'][1].blank?

      #Changed 411 phone number
      result += ['employee','phone' ] if changes['used_for_411'] && (changes['used_for_411'].instance_of?(Array) ? changes['used_for_411'][1] == '1' : changes['used_for_411'] == '1')
      result += ['employee','phone' ] if changes['used_for_411'] && (changes['used_for_411'].instance_of?(Array) ? changes['used_for_411'][0] == '1' : changes['used_for_411'] == '1')
      
      unless four_11_only
        #Report changnes
        result += ['employee','phone' ] if changes['primary_employee_for_phone'] && (changes['primary_employee_for_phone'].instance_of?(Array) ? changes['primary_employee_for_phone'][1] == '1' : changes['primary_employee_for_phone'] == '1')
        result += ['employee','phone' ] if changes['primary_employee_for_phone'] && (changes['primary_employee_for_phone'].instance_of?(Array) ? changes['primary_employee_for_phone'][0] == '1' : changes['primary_employee_for_phone'] == '1')
      end
      
      #People First primary employee location change
      result += ['employee','location' ] if changes['primary_employee_location'] && (changes['primary_employee_location'].instance_of?(Array) ? changes['primary_employee_location'][1] == '1' : changes['primary_employee_location'] == '1')
      result += ['employee','location' ] if changes['primary_employee_location'] && (changes['primary_employee_location'].instance_of?(Array) ? changes['primary_employee_location'][0] == '1' : changes['primary_employee_location'] == '1')
     
  	  result.uniq
  end
  def old_attribute_value_label(uc, attribute)
    if uc.action == 'create'
      ''
    else
      old_attributes = uc.old_attributes
      attribute_value_label(old_attributes, attribute)
    end
  end
  
  def new_attribute_value_label(uc, attribute)
    if uc.action == 'destroy'
      ''
    else
      new_attributes = uc.new_attributes
      attribute_value_label(new_attributes, attribute)
    end
  end
  
  def attribute_value_label(value, attribute)
    if attribute.to_s =~ /_id$/

      attribute.gsub(/_id$/,'').classify.constantize.find(value[attribute]).to_label rescue ''
    else
      value[attribute].to_s || ''
    end
  end
  
  def many_changes(uc, odd_even, options = {:scheduled_send => true, :standard_changes => false})
    result = ''
    unsent_change = uc

    new_changes = uc.changes.reject {|key, value| 
                                    value.nil? || 
                                    (value.class == Array && (value[0].blank? && value[1].blank?)) ||
                                    (uc.action == 'create' && value == false) ||
                                    (Set[key].subset?(AuditE911.non_people_first))
                                    } 
    size = new_changes.size rescue 0
    if size > 0
      size = 1 unless options[:standard_changes]
    	new_changes.each do |k,change|
      
	      if k == new_changes.keys[0]
	        result += "<tr class='#{odd_even}'>\n"
          result += "  <td rowspan='#{size}'>#{uc.change_description}</td>\n"
	        result += "  <td rowspan='#{size}'>#{item_label(uc) || '&nbsp;'}</td>\n"
	        result += standard_change_columns(unsent_change, k) if options[:standard_changes]
          result += "  <td rowspan='#{size}'>#{uc.scheduled_send}</td>\n" if options[:scheduled_send]
	        result += "</tr>\n"
	      elsif options[:standard_changes]
	        result += "<tr class='#{odd_even}'>\n"
	        result += standard_change_columns(unsent_change, k)
	        result += "</tr>\n"
	      else
	      end
	    end
	  end
    result
  end
  
  def standard_change_columns(uc, attribute)
      '  <td>' + label('',attribute) + "</td>\n" +
      '  <td>' + old_attribute_value_label(uc, attribute) + "</td>\n" +
      '  <td>' + new_attribute_value_label(uc, attribute) + "</td>\n"
  end
end
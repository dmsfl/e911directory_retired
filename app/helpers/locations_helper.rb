module LocationsHelper
  
  def get_buildings(id = nil)
    Building.all.collect {|b| [b.name, b.id]}
  end
  
end

class LocationsController < ApplicationController
  prepend_before_filter :login_required
  require_role 'Administrator'
  # GET /locations
  # GET /locations.xml
  def index
    @location_search_params = Search.new
    @location_destination = {:action => 'search_results'}
    @location_html_results = {:class => 'custom_replace_results'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @locations }
    end
  end

  # GET /locations/1
  # GET /locations/1.xml
  def show
    @location = Location.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @location }
    end
  end

  # GET /locations/new
  # GET /locations/new.xml
  def new
    @location = Location.new
    @location.building = Building.find(params[:building_id]) if params[:building_id]
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @location }
    end
  end

  # GET /locations/1/edit
  def edit
    @location = Location.find(params[:id])
  end

  # POST /locations
  # POST /locations.xml
  def create
    @location = Location.new(params[:location])

    respond_to do |format|
      if @location.save
        flash[:notice] = 'Location was successfully created.'
        format.html { redirect_to(@location) }
        format.xml  { render :xml => @location, :status => :created, :location => @location }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /locations/1
  # PUT /locations/1.xml
  def update
    @location = Location.find(params[:id])

    respond_to do |format|
      if @location.update_attributes(params[:location])
        flash[:notice] = 'Location was successfully updated.'
        format.html { redirect_to(@location) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.xml
  def destroy
    @location = Location.find(params[:id])
    @location.destroy
    flash[:notice] = 'Deleted the location successfully.'
    respond_to do |format|
      format.html { redirect_to(:action => 'index') }
      format.xml  { head :ok }
    end
  end
  
  def search_results
    if params[:search]
      search_params = session[:search] = params[:search]
      params[:page] = params[:location_page]
      @extra_params = {:search => session[:search]}
      @floors = "['"+ get_floors(search_params).join("','")+"']"
      @locations  = parse_params_and_find('location', search_params)
    elsif params[:building] || params[:building_page]
      if params[:building]
        session[:building].clear unless session[:building].nil?
        session[:building] = params[:building]
      end
      building_to_search = Building.new(session[:building])
      @buildings = Building.paginate :all, :page => params[:building_page], :per_page => 10, :include => [:county, :campus], :conditions => building_to_search.conditions
    elsif params[:locations_from_building] || params[:locations_from_building_page]
      search_params = params[:locations_from_building]
      @extra_params = {}
      @floors = "['"+get_floors({:building_id => search_params}).join("','")+"']"
      @building = Building.find(search_params)
      @locations = Location.paginate :all, :page => params[:location_page], :per_page => 10, :conditions => {:building_id => search_params}
    end
  
  end
  
  
  def associtated_employees_and_phones
    @location = Location.find(params[:id], :include => ['employees', 'phones', 'building'])
    @employees = @location.employees
    @phones = @location.phones
  end
end

class AuditsController < ApplicationController
  
  def unsent_changes
    @unsent_changes = AuditE911.find(:all, :conditions => {:user_id => current_user.id, :sent => false})
    @unsent_changes = AuditE911.select_only_people_first_changes(@unsent_changes)
  end
  def send_changes
    @unsent_changes = AuditE911.find(:all, :conditions => {:user_id => current_user.id, :sent => false})
    @unsent_changes = AuditE911.select_only_people_first_changes(@unsent_changes)
    if AuditMailer.deliver_user_changes_to_people_first(current_user)
       Audit.update_all('sent = 1',"id in (#{@unsent_changes.map(&:id).join(',')})")
       @unsent_changes_count = 0
        @success =true
    else
      @success = false
    end
  end
end
class UsersController < ApplicationController
  prepend_before_filter :login_required
  skip_before_filter :login_required, :only => [ :forgot_password, :reset_password, :create, :activate]
  require_role 'Administrator', :for_all_except => [:index, :forgot_password, :reset_password, :change_password, :create, :activate]
  # TODO: write tests to ensure before filter and require role work independently.
  
  def index    
    @users = User.find :all, :include => :employee
    @users.each do |u|
      employee = u.employee
      four_11_relations = employee.master_relations.select {|ml| ml.used_for_411}
      u.four_11_phone = four_11_relations[0].phone unless four_11_relations.empty?
    end
    @search_params = Employee.new
    @search_results_for_user_add = {:controller => "users", :action => "search_results_for_user_add" }
    @html_results = {:class => 'ajax_form_dialog'}
  end
  
  def show
    @user = User.find(params[:user])
  end
  
  def new
    @user = User.new
  end
  
  def edit
    @user = User.find(params[:id], :include => 'roles')
    @roles = Role.find(:all, :order => 'name')
  end
  
  def create
    employee = Employee.find(:first, :joins => 'left outer join users on employees.id = users.employee_id',  :conditions => ['users.employee_id is null and email = ?', params[:email].strip])
    if employee
      add_employee(employee)
    else
      flash[:error] = "The e-mail you entered either (1) already has an account or (2) does not exist in the system. If you are certain that neither is the case,
      please double-check that your e-mail is entered correctly in your People First account."
      redirect_to({:controller => 'welcome', :action => 'index'})
    end
  end
  
  def add_employee(employee = nil)
    @employee = employee || Employee.find(params[:id])
    if @employee.valid?
      @user = User.create_from @employee
      flash[:notice] = "Added #{@user.employee.full_name} as a user to the #{APP_CONFIG['settings']['name']}. A notification e-mail was sent to #{@user.employee.email}."+
                       " Please follow the instructions in the e-mail we sent you to complete the user account creation process."
      redirect_to({:controller => 'welcome', :action => "index"})
    else
      flash[:error] = "We were not able to add this employee because they are invalid."
      redirect_to({:controller => 'welcome', :action => "index"})
    end
  end
  
  def update
    @user = User.find(params[:id], :include => 'roles' )
    if params[:user].nil?
      @user.roles = []
    else
      params[:user][:role_ids] ||= []
      @user.role_ids = params[:user][:role_ids]
    end
    
    success ||= @user.save!
    if success && @user.errors.empty?
      flash[:notice] = "Saved #{@user.employee.full_name} successfully."
      redirect_to :protocol => 'https', :controller => 'search', :action => "index"
    else
      flash[:error] = "Failed to save user. Please contact the system administrator."
      redirect_to :action => "index"
    end
    # TODO: write tests for update
  end
  
  def search_results_for_user_add
    
    search = Search.new
    
    @employees = search.find_employees(params[:employee])
  end
  
  
  def delete
    if @user = User.find(params[:id]) rescue false
      name = @user.employee.full_name
      success = User.delete(@user)
      if success
        flash[:notice] = "Deleted user #{name} successfully."
        redirect_to :protocol => 'https', :action => 'index'
      else
        flash[:error] = "Failed to delete #{name}. Please contact IT to fix the problem."
        redirect_to :protocol => 'https', :action => 'index'
      end
      # TODO: write tests for delete action.
    else
      flash[:error] = "Sorry, that user does not exist. They may have already been deleted."
      redirect_to :protocol => 'https', :action => 'index'
    end
  end
  
  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    
    respond_to do |format|
      format.html { redirect_to(:protocol => 'https', :controller => 'users', :action => 'index') }
      format.xml  { head :ok }
    end
  end
  
  
  def change_password
    return unless request.put?
    if ((params[:password] == params[:password_confirmation]) && 
      !params[:password_confirmation].blank?)
      current_user.password_confirmation = params[:password_confirmation]
      current_user.password = params[:password]
      if current_user.save
        current_user.activation_code = nil unless current_user.activation_code.blank?
        current_user.activated_at = Date.today if current_user.activated_at.blank?
        current_user.save
        flash[:notice] = "Password successfully updated" 
        redirect_to :protocol => 'https', :controller => 'search', :action => "index"
      else
        flash[:alert] = "Password not changed because:<br />#{current_user.errors.full_messages}" 
      end
      
    else
      flash[:alert] = "New Password mismatch" 
    end
  end
  
  #gain email address
  def forgot_password
    return unless request.post?
    if @user = User.find(:first, :include => 'employee', :conditions => ['employees.email = ?',params[:user][:email]])
      @user.forgot_password
      @user.save
      redirect_to :protocol => 'https', :controller => 'welcome', :action => "index"
      flash[:notice] = "A password reset link has been sent to your e-mail address" 
    else
      flash[:error] = "Could not find a user with that e-mail address" 
    end
  end
  
  #reset password
  def reset_password
    @user = User.find_by_password_reset_code(params[:id]) unless params[:id].blank?
    if params[:user] && @user
      attempt_reset
    elsif @user.nil?
      @mail_subject = 'DMS Directory Password Reset Problem'
      @mail_body = "Hello,"+
        "\n My name is **type your name here** and I am having trouble changing my password for the #{APP_CONFIG['settings']['name']} application."+
        "\n \n **please describe in more detail here**  "
    end
  end
  
  def attempt_reset
    if ((params[:user][:password] == params[:user][:password_confirmation]) && 
      !params[:user][:password_confirmation].blank?)
      self.current_user = @user #for the next two lines to work
      @user.password_confirmation = params[:user][:password_confirmation]
      @user.password = params[:user][:password]
      @user.activation_code = nil unless current_user.activation_code.blank?
      @user.activated_at = Date.today if current_user.activated_at.blank?
      @user.reset_password
      flash[:notice] = @user.save ? "Password reset success." : "Password reset failed." 
      redirect_to :protocol => 'https', :controller => 'search', :action => 'index'
    else
      flash[:alert] = "Password mismatch or no new password was given." 
    end  
  end

  def activate
    logout_keeping_session!
    user = User.find_by_activation_code(params[:activation_code]) unless params[:activation_code].blank?
    case
      when (!params[:activation_code].blank?) && user && !user.active?
      user.activate!
      flash[:notice] = "Signup complete! Please sign in to continue."
      redirect_to :protocol => 'https', :controller => 'sessions', :action => 'new'
      when params[:activation_code].blank?
      flash[:error] = "The activation code was missing.  Please follow the URL from your e-mail."
      redirect_back_or_default('/')
    else 
      flash[:error]  = "We couldn't find a user with that activation code. Please check your e-mail for the correct link. Or maybe you've already activated; try signing in."
      redirect_to :protocol => 'https', :controller => 'search', :action => 'index'
    end
  end
  
end

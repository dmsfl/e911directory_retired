class LocationsManagerController < ApplicationController
  prepend_before_filter :login_required
  require_role 'Administrator'
  
  # GET /locations
  # GET /locations.xml
  def index
  end
end

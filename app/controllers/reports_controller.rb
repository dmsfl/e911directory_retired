class ReportsController < ApplicationController
  prepend_before_filter :login_required, :setup_report_options
  require_role 'Administrator'

  def index
    set_nightly_report_entries(1.day.ago.to_date)
  end
  
  def search_results
    if params[:day]
      set_nightly_report_entries(params[:day].to_date)
    elsif params[:month]
      set_monthly_report_entries(params[:month].to_date)
    end
  end
  
  def full_data_report
    
    @all_master_relations = MasterRelation.report_table(:all,
     :only => [],
     :include => {
       :employee => {:only => ['last_name','first_name', 'middle_name', 'email']},
       :phone => {:only => ['area_code', 'main_number']},
       :phone_category => {:only => ['name']},
       :location => {:only => ['building_identifier', 'floor', 'room']},
       :building => {:only => ['full_street_address', 'city']}
     }
    )
    @all_master_relations.sort_rows_by! %w{employee.last_name employee.middle_name employee.first_name}
    @all_master_relations.add_column('phone') {|r| "(#{ r.data['phone.area_code']}) #{ r.data['phone.main_number'][0..2]}-#{r.data['phone.main_number'][3..6]}" unless r.data['phone.area_code'].blank?}
    @all_master_relations.reorder %w{
                               employee.last_name 
                               employee.middle_name 
                               employee.first_name 
                               phone_category.name
                               phone
                               building.full_street_address 
                               building.city 
                               location.building_identifier 
                               location.floor 
                               location.room 
                               employee.email } 
    
    
    @all_master_relations.rename_columns({
       'employee.last_name' => 'last name',
       'employee.first_name' => 'first name',
       'employee.middle_name' => 'middle',
       'phone_category.name' => 'phone category',
       'building.full_street_address' => 'street address',
       'building.city' => 'city',
       'location.building_identifier' => 'bldg id', 
       'location.floor' => 'floor',
       'location.room' => 'room',
       'employee.email' => 'email'
    })

    respond_to do |wants|
      wants.html do
        render :layout => false
      end
      wants.csv do
        csv_string = @all_master_relations.as(:csv)
        send_data csv_string,
                  :type => 'text/csv; charset=iso-8859-1; header=present',
                  :disposition => "attachment; filename=dms_central_directory.csv"
      end
    end
    
  end
  
  def four_one_one_differences
   @differences = PeopleFirst.find_by_sql("
      -- Employees in PeopleFirst that do not already have a 411 phone number in Directory
      select 
        central_dir_formatted_for_people_first_differences.first_name as c_first_name,
        central_dir_formatted_for_people_first_differences.last_name as c_last_name,       
        central_dir_formatted_for_people_first_differences.middle_name as c_middle_name,  
        central_dir_formatted_for_people_first_differences.four_11_phone as c_phone,
        people_firsts.first_name as pf_first_name,  
        people_firsts.last_name as pf_last_name,  
        people_firsts.middle_name as pf_middle_name,
        people_firsts.four_11_phone as pf_phone,  
        people_firsts.employee_id as pf_employee_id,  
        central_dir_formatted_for_people_first_differences.employee_id as c_employee_id
      from people_firsts
      left join central_dir_formatted_for_people_first_differences on people_firsts.employee_id = central_dir_formatted_for_people_first_differences.employee_id
      where central_dir_formatted_for_people_first_differences.employee_id is null and people_firsts.employee_id != 0
      
      union
      
      -- Employees in Directory that do exist in PeopleFirst (should rarely return results)
      select 
        central_dir_formatted_for_people_first_differences.first_name as c_first_name,
        central_dir_formatted_for_people_first_differences.last_name as c_last_name,       
        central_dir_formatted_for_people_first_differences.middle_name as c_middle_name,  
        central_dir_formatted_for_people_first_differences.four_11_phone as c_phone,
        people_firsts.first_name as pf_first_name,  
        people_firsts.last_name as pf_last_name,  
        people_firsts.middle_name as pf_middle_name,
        people_firsts.four_11_phone as pf_phone,  
        people_firsts.employee_id as pf_employee_id,  
        central_dir_formatted_for_people_first_differences.employee_id as c_employee_id
      from central_dir_formatted_for_people_first_differences
      left join people_firsts on people_firsts.employee_id = central_dir_formatted_for_people_first_differences.employee_id
      where people_firsts.employee_id is null
      
      union
      
      -- Employees with different phone values
      select 
        central_dir_formatted_for_people_first_differences.first_name as c_first_name,
        central_dir_formatted_for_people_first_differences.last_name as c_last_name,       
        central_dir_formatted_for_people_first_differences.middle_name as c_middle_name,  
        central_dir_formatted_for_people_first_differences.four_11_phone as c_phone,
        people_firsts.first_name as pf_first_name,  
        people_firsts.last_name as pf_last_name,  
        people_firsts.middle_name as pf_middle_name,
        people_firsts.four_11_phone as pf_phone,  
        people_firsts.employee_id as pf_employee_id,  
        central_dir_formatted_for_people_first_differences.employee_id as c_employee_id
      from central_dir_formatted_for_people_first_differences
      left join people_firsts on people_firsts.employee_id = central_dir_formatted_for_people_first_differences.employee_id
      where people_firsts.four_11_phone != central_dir_formatted_for_people_first_differences.four_11_phone and people_firsts.employee_id !=''
      order by pf_last_name, pf_middle_name, pf_first_name;
   ")
  end
  
  private
  def setup_report_options
    @last_thirty_nightlies = AuditE911.find(:all, :select => [:created_at],
                                                  :conditions => ["username = ?", APP_CONFIG['settings']['cron_auditor']], 
                                                  :group => 'DATE(created_at)', 
                                                  :order => 'created_at DESC', 
                                                  :limit => 30).map(&:created_at) || []

   @last_twelve_monthly_nightlies = AuditE911.find(:all, :select => [:created_at],
                                                  :conditions => ["username = ?", APP_CONFIG['settings']['cron_auditor']], 
                                                  :group => 'MONTH(created_at)', 
                                                  :order => 'created_at DESC', 
                                                  :limit => 12).map(&:created_at) || []
    
  end
  
  def set_nightly_report_entries(date)
    @nightly_changes = AuditE911.nightly_changes_from_people_first(date)
    @guery_statement = "Changes from People First for #{date.strftime("%m-%d-%Y")}"
  end
  
  def set_monthly_report_entries(date)
    @nightly_changes = AuditE911.monthly_changes_from_people_first(date)
    @guery_statement = "People First changes for #{date.strftime("%B %Y")}"
  end
end

# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base

  include AuthenticatedSystem
  # You can move this into a different controller, if you wish.  This module gives you the require_role helpers, and others.
  include RoleRequirementSystem

  include ExceptionNotifiable
  ExceptionNotifier.exception_recipients = APP_CONFIG['mail']['apps_support']
  ExceptionNotifier.email_prefix = "[ERROR! DMS Directory Application] "

  helper :all # include all helpers, all the time

  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  filter_parameter_logging :password
  before_filter :get_unsent_changes
  before_filter :administrator?

  audit Phone, Location, Employee, MasterRelation => { :except => [:primary_employee_location, :primary_phone_location,:primary_employee_for_phone, :primary_phone_for_employee]}
  
  def parse_params_and_find(search_type, search_parameters)
    search = Search.new
    page = params[:page] || search_parameters[:page] || 1
    search_options = eval("search.#{search_type}_conditions(search_parameters)")
    search_options.merge!({:page => page, :per_page => 10})
    search_type.classify.constantize.paginate(:all, search_options)
  end

  #Both the search functionality and the phone 
  def get_phone_categories
    PhoneCategory.all.collect {|category| [category.name, category.id]}
  end

  def get_floors(building_id)
    floors = Location.all(:conditions => ['building_id = ?', building_id], :select => 'distinct(floor)')
  end
  
  def get_unsent_changes
    if logged_in? && unsent_changes = AuditE911.find(:all, :conditions => {:user_id => current_user.id, :sent => false})
      @unsent_changes_count = AuditE911.select_only_people_first_changes(unsent_changes).size
    else
      @unsent_changes_count = 0
    end
  end
  
  def administrator?
    @administrator = (current_user.has_role?('Administrator') rescue false)
  end
end

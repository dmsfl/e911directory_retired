class SearchController < ApplicationController
  prepend_before_filter :login_required
  
  def index
    @employee_search_params = Search.new
    @employee_destination = employees_search_results_path
    @employee_html_results = {:class => 'replace_results'}
    @phone_search_params = Search.new
    @phone_categories = get_phone_categories
    @phone_destination = phones_search_results_path
    @phone_html_results = {:class => 'replace_results'}

    if session[:main_search]
      case session[:main_search][:type]
      when 'phone'
        @phones = Search.new.find_phones(session[:main_search][:params]).paginate({:page => session[:main_search][:page], :per_page => 10})
        @phones.each { |phone| phone.get_primary_labels}
        @phone_search_params = Search.new(session[:main_search][:params])
      when 'employee'
        search_params = session[:main_search][:params]
        search_params[:page] = session[:main_search][:page]
        @employees = parse_params_and_find('employee',search_params)
        @employees.each { |e| e.get_primary_labels}
        @employee_search_params = Search.new(session[:main_search][:params])
      end
    end
  end

  def counties
    @items = County.find(:all, :select => ['name'], :conditions => ac_condition('name')).map(&:name)
    render(:partial => 'shared/autocomplete', :layout => 'blank')
  end

  def cities
    @items = Building.find(:all, :select => ['city'], :conditions =>  ac_condition('city'), :group => ['city']).map(&:city)
    render(:partial => 'shared/autocomplete', :layout => 'blank')
  end

  def campuses
    @items = Campus.find(:all, :select => ['name'], :conditions =>  ac_condition('name')).map(&:name)
    render(:partial => 'shared/autocomplete', :layout => 'blank')
  end

  def buildings
    @items = Building.find(:all, :select => ['name'], :conditions =>  ac_condition('name'), :group => 'name').map(&:name)
    render(:partial => 'shared/autocomplete', :layout => 'blank')
  end
  def street_addresses
    @items = Building.find(:all, :select => ['full_street_address'], :conditions =>  ac_condition('full_street_address'), :group => 'full_street_address').map(&:full_street_address)
    render(:partial => 'shared/autocomplete', :layout => 'blank')
  end
  
  def zip_codes
    @items = Building.find(:all, :select => ["zip_code_5"], :conditions =>  ac_condition("zip_code_5"), :group => "zip_code_5").map(&:zip_code_5)
    render(:partial => 'shared/autocomplete', :layout => 'blank')
  end

  private
    def ac_condition(field)
      ["#{field} LIKE ?", "%#{params[:q]}%"]
    end
end

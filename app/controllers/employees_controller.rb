class EmployeesController < ApplicationController
  caches_action :search, :expires_in => 1.day
  prepend_before_filter :login_required, :except => [:search]
  require_role 'Administrator', :for => [:edit, :update]
  def index    
      @employees = Employee.find :all
      @employee_to_search = Employee.new
    respond_to do |format|
      format.html
      format.xml { render :xml => @employees}
    end
  end
  def search
    @employees = Search.new.find_limited_employees(params['employee'])
    respond_to do |format|
      format.html { render :nothing => true }
      format.xml  { render :xml => @employees.to_xml }
    end
  end
  def show
    if request.format != 'xml'
      @employee = Employee.find(params[:id])
    else
      employee ||= Employee.find(params[:id], :order => 'updated_at desc')
      phones_and_locations = employee.try(:get_primary_labels)
      xml_output = {:employee => employee || '', :locations_and_phones => phones_and_locations || ''}
    end
    respond_to do |format|
      format.html
      format.xml { render :xml => xml_output }
    end
  end

  def edit
    @employee = Employee.find(params[:id], :include => ['master_relations', 'locations', 'phones'])
  end
  
  def update
    @employee = Employee.find(params[:id], :include => 'roles' )
    params[:employee][:role_ids] ||= []
    @employee.update_attributes(params[:employee])
    success = @employee.save!
    if success && @employee.errors.empty?
      flash[:notice] = "Saved #{@employee.full_name} successfully."
      redirect_to :action => "index"
    else
      flash[:error] = "Failed to save employee. Please contact the system administrator."
      redirect_to :action => "index"
    end
    # TODO: write tests for update
  end
  
  def first_names
    query = params[:q]
    params[:omit_users] ? omit_users = ' AND users.id is null ' : omit_users = ''
    @items = Employee.find(:all, :select => ['employees.first_name'], :include => [:user], :conditions => ['employees.first_name LIKE ? ?', "%#{query}%", omit_users], :group => 'first_name').map(&:first_name)
    render :partial => 'shared/autocomplete', :layout => 'blank'
  end
  
  def last_names
    query = params[:q]
    params[:omit_users] ? omit_users = ' AND users.id is null ' : omit_users = ''
    @items = Employee.find(:all, :select => ['employees.last_name'], :include => [:user], :conditions => ['employees.last_name LIKE ? ?', "%#{query}%", omit_users], :group => 'last_name').map(&:last_name)
    render :partial => 'shared/autocomplete', :layout => 'blank'
  end
 
  def why_invalid
    @employee = Employee.find_by_id(params[:id])
    @employee.valid?
  end
  
  def division_employees
    @division_employees = Employee.with_same_division_as(params[:id]).paginate(:page => params[:page])
    if @division_employees.empty?
      flash[:error] = "We could not find employees with the same division as the given employee."
    else
      @division_employees.each {|e| e.get_primary_labels}
    end
  end
  
  def search_results
    if params[:search]
      session[:search].clear if session[:search]
      session[:search] = params[:search]
      session[:main_search] = {}
      session[:main_search][:type] = 'employee'
      session[:main_search][:params] = params[:search]
      search_params = params[:search]
      page = session[:main_search][:page] = params[:page] || 1
    elsif session[:main_search][:type] == 'employee'
      search_params = session[:main_search][:params]
      page =   session[:main_search][:page] = params[:page] || session[:main_search][:page]
    else
      search_params = {}
      page = params[:page] || 1
    end
    search_params[:page] = page
    @employees = parse_params_and_find('employee',search_params)
    @employees.each { |e| e.get_primary_labels}
  end
end

class PhonesController < ApplicationController
  prepend_before_filter :login_required
  require_role 'Administrator', :except => ['search_results', 'area_codes', 'main_numbers', 'show']
  # GET /phones
  # GET /phones.xml
  def index
    @phones = Phone.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @phones }
    end
  end

  # GET /phones/1
  # GET /phones/1.xml
  def show
    @phone = Phone.find(params[:id], :include => ['master_relations', 'locations', 'employees'])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @phone }
    end
  end

  # GET /phones/new
  # GET /phones/new.xml
  def new
    @phone = Phone.new
    @phone.phone_category = PhoneCategory.find(:first, :conditions => "name = 'Voice'")
    @phone_categories = get_phone_categories
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @phone }
    end
  end

  # GET /phones/1/edit
  def edit
    @phone = Phone.find(params[:id], :include => ['master_relations', 'locations', 'employees'])
    @phone_categories = get_phone_categories
  end


  # POST /phones
  # POST /phones.xml
  def create
    @phone = Phone.new(params[:phone])

    respond_to do |format|
      if @phone.save
        flash[:notice] = 'Phone was successfully created.'
        format.html { redirect_to(:action => 'edit', :id => @phone.id) }
        format.xml  { render :xml => @phone, :status => :created, :location => @phone }
      else
        @phone_categories = get_phone_categories
        format.html { render :action => "new" }
        format.xml  { render :xml => @phone.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /phones/1
  # PUT /phones/1.xml
  def update
    @phone = Phone.find(params[:id])
      if (@phone.comment == params[:phone][:comment] || (@phone.comment.blank? && params[:phone][:comment].blank?)) && @phone.phone_category_id.to_s ==  params[:phone][:phone_category_id]
        flash[:notice] ||= {}
        flash[:params] = params
        redirect_to :controller => 'master_relations', :action => 'save_primaries'
      elsif @phone.update_attributes(params[:phone])
        flash[:notice] ||= {}
        flash[:notice][:a_phone] = 'Phone was successfully updated.'
        #The only way to pass parameters to another controller (since we're updating phones and master_relations by using redirects
        # as opposed to using renders)
        # that I know of is by passing flash.
        flash[:params] = params
        redirect_to :controller => 'master_relations', :action => 'save_primaries'
      else
        flash[:alert] = @phone.errors || "We failed to save the phone. Please check that you entered the correct information and try again. If this is a persistent problem, please contact our administrators."
        redirect_to :controller => 'phones', :action => 'edit', :id => @phone.id
      end
  end

  # DELETE /phones/1
  # DELETE /phones/1.xml
  def destroy
    @phone = Phone.find(params[:id], :include => 'master_relations')
    if !@phone.master_relations.nil? &&  @phone.master_relations.select {|ml| ml.primary_phone_location || ml.primary_employee_for_phone || ml.primary_phone_for_employee}.size > 0
      flash[:alert] = "You've attempted to delete a phone number that belongs to a primary employee or location. Try removing the phone number as the primary phone number for the corresponding location or employee and then try deleting again. If the problem persists, please take note of the problematic phone number and contact an administrator."
      redirect_to :controller => 'search'
    else
      @phone.destroy
      flash[:notice] = "You've deleted the phone number."
      respond_to do |format|
        format.html { redirect_to(:controller => 'search') }
        format.xml  { head :ok }
      end
    end
  end
  
  def search_results
    if params[:search]
      session[:search].clear if session[:search]
      session[:search] = params[:search]
      session[:main_search] = {}
      session[:main_search][:type] = 'phone'
      session[:main_search][:params] = params[:search]
      search_params = params[:search]
      page = session[:main_search][:page] = params[:page] || 1
    elsif session[:main_search][:type] == 'phone'
      search_params = session[:main_search][:params]
      page =   session[:main_search][:page] = params[:page] || session[:main_search][:page]
    else
      search_params = {}
      page = params[:page] || 1
    end
    search = Search.new
    @phones = search.find_phones(search_params).paginate({:page => page, :per_page => 10})
    @phones.each { |phone| phone.get_primary_labels}
  end
  
  def area_codes
    @items = Phone.find(:all, :select => ['area_code'], :conditions => ['area_code LIKE ?', "%#{params[:q]}%"], :group => 'area_code').map(&:area_code)
    render :partial => 'shared/autocomplete', :layout => 'blank'
  end

  def main_numbers
    @items = Phone.find(:all, :select => ['main_number'], :conditions => ['main_number LIKE ?', "%#{params[:q]}%"], :group => 'main_number').map(&:main_number)
    render :partial => 'shared/autocomplete', :layout => 'blank'
  end

end

class MasterRelationsController < ApplicationController
  prepend_before_filter :login_required
  require_role 'Administrator', :for => [ :add_location_search_results, 
    :add_employee_search_results, 
    :add_phone_search_results, 
    :new,
    :create, 
    :delete_component,
    :edit,
    :update,
    :destroy]

  def new
    session[:master_relation] = nil
    @from = session[:from] = params[:from] if %w{employee phone location}.include? params[:from]
    @from_id = session[:from_id] = params[:id]
    @from_object = @from.classify.constantize.find(@from_id)
    @component = params[:component]
    @search_form_partial = 'find_' + @component.pluralize
    @phone_categories = get_phone_categories if @component == 'phone'
    @locals =  {"#{@component}_to_search".to_sym => @component.classify.constantize.search,
               :destination => eval("add_#{@component}_search_results_path(:new => 'true')"),
               :html_results => {:rel => '#search_results', :class => 'custom_replace_results'}}
  end

  def edit
    setup_search_and_from
    @master_relation = MasterRelation.find(params[:id], 
                                           :include => [params[:component].to_sym, params[:from].to_sym])
    @component = @master_relation.send(params[:component])
    @from = params[:from].to_sym
  end

  def delete_component
    @master_relation = MasterRelation.find(params[:id])
    get_master_relation_components_from_params
    %w{phone employee location}.each do |component|
      eval("@master_relation.#{component} = nil") if component == params[:component]
    end
    if @master_relation.one_relation_left?
      if @master_relation.destroy
        flash[:notice] = "Successfully deleted the entire row."
        redirect_to main_editor
      else
        flash[:error] = "Tried to but failed to delete the entire row."
        redirect_to main_editor
      end
    elsif @master_relation.save
      flash[:notice] = "Deleted #{params[:component]} successfully."
      redirect_to main_editor
    else
      flash[:notice] = "There was a problem deleting this #{params[:component]}. Please contact an administrator."
      redirect_to main_editor
    end
  end

  def edit_component
    setup_search_and_from
    @component = params[:component]
    @master_relation = MasterRelation.find(params[:id], :include => [@from, @component])
    @results_id = 'search_results'
    case @component
    when 'employee'
      @search_form_partial = 'find_employees'
      @locals = {:employee_to_search => Employee.search,
                 :destination => add_employee_search_results_path,
                 :html_results => {:rel => '#'+@results_id, :class => 'custom_replace_results'}}
    when 'phone'
      @search_form_partial = 'find_phones'
      @locals = {:phone_to_search => Phone.search,
                 :destination => add_phone_search_results_path,
                 :html_results => {:rel => '#'+@results_id, :class => 'custom_replace_results'}}
      @phone_categories = get_phone_categories
    when 'location'
      @search_form_partial = 'find_locations'
      @locals = {:phone_to_search => Location.search,
                 :destination => add_location_search_results_path,
                 :html_results => {:rel => '#'+@results_id, :class => 'custom_replace_results'}}
    end
  end

  def add_location_search_results
    setup_search_and_from
    search = session[:search]
    location_table_columns = %w{floor room building_identifier}
    building_table_columns = %w{city full_street_address name}
    location_sort_fields = possible_sort_values_for location_table_columns 
    building_sort_fields = possible_sort_values_for building_table_columns 
    if params[:building_query]
      @buildings = Building.send(search[:order].to_sym) if search[:order] && building_sort_fields.include?(search[:order].to_s)
      @buildings = (@buildings ? @buildings.name_like(search[:name_like]) : Building.name_like(search[:name_like]))
      @buildings = @buildings.campus_id_equals(search[:campus_id]) unless search[:campus_id].blank?
      @buildings = @buildings.county_id_equals(search[:county_id]) unless search[:county_id].blank?
      @buildings = @buildings.city_like(search[:city_like])
      @buildings = @buildings.full_street_address_like(search[:full_street_address_like])
      @buildings = @buildings.paginate( :page => params[:page], :per_page => 5)
      order_params_for building_table_columns, {:building_query => 'true'}
    elsif params[:location_query]
      @master_relation = new_or_existing_master_relation
      @locations = Location.send(search[:order].to_sym) if search[:order] && location_sort_fields.include?(search[:order].to_s)
      @locations =  (@locations ? @locations.building_id_equals(search[:building_id]) : Location.building_id_like(search[:building_id]))
      @locations = @locations.room_like(search[:room_like])
      @locations = @locations.floor_equals(search[:floor_equals]) unless search[:floor_equals].blank?
      @locations = @locations.paginate( :page => params[:page], :per_page => 5)
      @extra_params = (!search[:floor_equals].blank? || !search[:room_like].blank?) ? {:search => search, :location_query => 'true'} : {:location_query => 'true'}
      @floors = "['"+get_floors(search[:building_id]).map(&:floor).join("','")+"']"
      order_params_for location_table_columns, {:location_query => 'true'}
    end
  end

  def add_employee_search_results
    setup_search_and_from
    @master_relation = new_or_existing_master_relation
    search = session[:search]
    sort_fields = possible_sort_values_for %w{last_name email}

    @employees = Employee.send(search[:order].to_sym) if search[:order] && sort_fields.include?(search[:order].to_s)
    @employees = @employees ? @employees.last_name_like(search[:last_name_like]) : Employee.last_name_like(search[:last_name_like])
    @employees = @employees.first_name_like(search[:first_name_like])
    @employees = process_search_radio_buttons([:active_flag_blank, :contractor_flag_blank], search, @employees)
    @employees = @employees.paginate( :page => params[:page], :per_page => 5)
    order_params_for [:last_name, :email]
  end

  def add_phone_search_results
    setup_search_and_from
    @master_relation = new_or_existing_master_relation
    search_params = params[:search] || session[:search]
    search = Search.new
    @phones = search.find_phones(search_params).paginate({:page => params[:search_page], :per_page => 5})
  end

  def create
    @from = session[:from]
    @from_id = session[:from_id]
    get_master_relation_components_from_params
    master_relation = MasterRelation.new
    eval("master_relation.#{@from}= #{@from.classify}.find(#{@from_id})")
    set_master_relation_components(master_relation)
    if master_relation.save
      flash[:notice] = "Saved the changes successfully."
      redirect_to main_editor
    else
      flash[:error] = "There was a problem saving the relationship. Please notify an administrator."
      redirect_to main_editor
    end
  end

  def update
    @from = session[:from]
    @from_id = session[:from_id]
    get_master_relation_components_from_params
    master_relation = MasterRelation.find(params[:id])
    set_master_relation_components(master_relation)
    if master_relation.save
      flash[:notice] = "Saved the changes successfully."
      redirect_to main_editor
    else
      flash[:error] = "There was a problem saving the relationship. Please notify an administrator."
      redirect_to main_editor
    end
  end

  # We can update primaries through phones or employees
  # Therefore we make it possible to update them through a
  # Redirect and flash parameters.
  def save_primaries
    @from = params[:from] || flash[:params][:from]
    @from_id = params[:from_id] || flash[:params][:from_id]
    @from_object = @from.classify.constantize.find(:first, :include => 'master_relations', :conditions => {:id => @from_id})
    case @from
    when 'employee'
      params[:primary_employee_location] ||= (flash[:params][:primary_employee_location] rescue false)
      flash.deep_merge!(save_primary_employee_location) 

      params[:primary_phone_for_employee] ||= (flash[:params][:primary_phone_for_employee] rescue false)
      flash.deep_merge!(save_primary_phone_for_employee)    

      params[:used_for_411] ||= (flash[:params][:used_for_411]  rescue false)
      flash.deep_merge!(save_used_for_411)    
    when 'phone'
      params[:primary_phone_location] ||= (flash[:params][:primary_phone_location]  rescue false)
      flash.deep_merge!(save_primary_phone_location)   

      params[:primary_employee_for_phone] ||= (flash[:params][:primary_employee_for_phone]  rescue false)
      flash.deep_merge!(save_primary_employee_for_phone)    
    end

    #Need to persist the flash in case none of the above statements are used. 
    flash[:notice] = flash[:notice] 
    redirect_to main_editor
  end

  private
  def possible_sort_values_for(fields)
    fields.map {|v| ["ascend_by_#{v}", "descend_by_#{v}"]}.flatten
  end

  def order_params_for(field_symbols, additional_params = {})
   @order_params = field_symbols.inject({}) do |new_hash, column|
      if session[:search][:order] =~ /descend_by_#{column}/
        new_hash[column.to_sym] = additional_params.merge({:search => session[:search].merge({:order => "ascend_by_#{column}"}),
                                                           :current => 'descending'})
      elsif session[:search][:order] =~ /ascend_by_#{column}/
        new_hash[column.to_sym] = additional_params.merge({:search => session[:search].merge({:order => "descend_by_#{column}"}),
                                                           :current => 'ascending'})
      else
        new_hash[column.to_sym] = additional_params.merge({:search => session[:search].merge({:order => "descend_by_#{column}"})})
      end
      new_hash
    end
  end

  def new_or_existing_master_relation
    if params[:new] || session[:master_relation].nil?
      @master_relation = MasterRelation.new
    else
      @master_relation = MasterRelation.find(session[:master_relation])
    end
  end

  def get_master_relation_components_from_params
    @master_relation_components =  %w{phone employee location}
    @master_relation_components.each do |i|
      eval("@#{i} = #{i.capitalize}.find(params[:#{i}])") unless params[i.to_sym].blank?
    end
  end

  def set_master_relation_components(master_relation)
    @master_relation_components.each do |i|
      master_relation.send(i+'=', eval('@'+i)) if eval('@'+i)
    end
  end
  
  # The save_primary_* methods are horribly repetitious. Next chance I get I should remove the repetition.
  def save_primary_employee_location
    @success = true
    @relation_type = :primary_employee_location
    @from_object.master_relations.each do |ml| 
      @from == 'employee' ? to = 'location' : to = 'employee'
      save_primary(ml)
    end
    create_alert({
      :positive => "Successfully saved the main location change for the employee.", 
      :negative => "Failed to save main location/employee. Please contact a system adiministrator."
    })
  end
  
  def save_primary_employee_for_phone
    @success = true
    @relation_type = :primary_employee_for_phone
    @from_object.master_relations.each do |ml| 
      @from == 'employee' ? to = 'phone' : to = 'employee'
      save_primary(ml)
    end
    create_alert({
      :positive => "Successfully saved the main employee change for the phone.", 
      :negative => "Failed to save a new main employee for the phone. Please contact a system adiministrator."
    })

  end
  
  def save_primary_phone_for_employee
    @success = true
    @relation_type = :primary_phone_for_employee
    @from_object.master_relations.each do |ml| 
      @from == 'employee' ? to = 'phone' : to = 'employee'
      save_primary(ml)
    end
    create_alert({
      :positive => "Successfully saved the main phone change for the employee.", 
      :negative => "Failed to save a new main phone for the employee. Please contact a system adiministrator."
    })
  end
  
  def save_primary_phone_location
    @success = true
    @relation_type = :primary_phone_location
    @from_object.master_relations.each do |ml| 
      save_primary(ml)
    end
    create_alert({
      :positive => "Successfully saved the main location change for the phone.", 
      :negative => "Failed to save main phone location/phone. Please contact a system adiministrator."
    })
  end

  def save_used_for_411
    @success = true
    @relation_type = :used_for_411
    @from_object.master_relations.each do |ml|
      @from == 'phone' ? to = 'location' : to = 'phone'
      save_primary(ml)
    end
    create_alert({
      :positive => "Successfully saved People First phone number.", 
      :negative => "Failed to save People First phone number. Please contact a system adiministrator."
    })
  end
  
  def create_alert messages
    if @success
      @success == :new_save ? {:notice => {@relation_type => messages[:positive]}} : {} 
    else
      {:alert => {@relation_type =>  messages[:negative]}}
    end
  end
  
  def save_primary master_relation
    if params[@relation_type] && master_relation.id == params[@relation_type].to_i
      unless master_relation[@relation_type] == true
        master_relation[@relation_type] = true
        @success = :new_save
      end
    else
      unless master_relation[@relation_type] == false || master_relation[@relation_type].blank? || master_relation[@relation_type] == '0'
        master_relation[@relation_type] = false
        @success = :new_save
      end
    end
    master_relation.save ? @success : false
  end

  # Sets all necessary variables for a search form of the particular 'instance' type.
   def setup_search_and_from
    session[:master_relation] = params[:id] unless (params[:id].blank? && params[:action] != 'new' && !params[:new])
    !params[:from].blank? ? @from = session[:from] = params[:from] : @from = session[:from]
    if !params[:id].blank? && params[:action] != 'new' && !params[:new]
      @from_id = session[:from_id] = MasterRelation.find(params[:id]).send(@from.to_s+'_id')
    else
      @from_id = session[:from_id]
    end
    if params[:search]
      session[:search] = params[:search]
    end
  end

  def process_search_radio_buttons(radio_buttons, search_params, search_object)
    radio_buttons.each do |radio_button_name|
      case search_params[radio_button_name]
      when 'true'
        search_object = search_object.send(radio_button_name)
      when 'false'
        search_object = search_object.contractor_flag_not_blank
      end
    end
    search_object
  end
  
  def main_editor
    @from ||= session[:from]
    @from_id ||= session[:from_id]
    eval("edit_#{@from}_path(@from.classify.constantize.find(@from_id))")
  end
end

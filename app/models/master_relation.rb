class MasterRelation < ActiveRecord::Base
  belongs_to :employee
  belongs_to :phone
  belongs_to :location
  has_one :building, :through => :location
  has_one :phone_category, :through => :phone

  acts_as_audited
  acts_as_reportable

  before_save :remove_left_over_primaries

  named_scope :with_components, {:include => [:employee, :phone, :location]}

  def remove_left_over_primaries
    self.primary_employee_location  = '0' if (employee.nil? || location.nil?)
    self.primary_phone_location     = '0' if (phone.nil? || location.nil?)
    self.primary_employee_for_phone = '0' if (employee.nil? || phone.nil?)
    self.primary_phone_for_employee = '0' if (employee.nil? || phone.nil?)
    self.used_for_411               = '0' if (employee.nil? || phone.nil?)

    #So that the record gets saved (returning false causes save to fail.)
    true
  end

  def one_relation_left?
    %w{employee phone location}.reject {|relation|  send(relation+"_id").nil?}.size <= 1
  end

  def phone_to_label
    case
    when phone && !extension.blank?
      "#{phone.to_label} ext. #{extension}"
    when phone
      phone.to_label
    else
      ''
    end
  end
end

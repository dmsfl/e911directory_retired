class StreetAbbreviation < ActiveRecord::Base
  def self.name_pairs
    self.all.collect {|e| [e.name, e.standard_abbreviation]}
  end
  def self.abbs
    self.all.collect {|e| e.standard_abbreviation}
  end
end

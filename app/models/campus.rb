class Campus < ActiveRecord::Base
  set_table_name :campuses
  default_scope :order => 'campuses.name ASC'
  has_many :buildings
  has_many :locations, :through => :buildings
  has_many :master_relations, :as => :location_entity
  
  def to_label
    name
  end
end

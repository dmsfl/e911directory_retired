class UserMailer < ActionMailer::Base
  def new_user(user)
    setup_email(user)
    @recently_saved = false
    @subject    += 'DMS Directory: Please activate your new account'
    @body[:url]  = "https://#{APP_CONFIG['settings']['domain']}/activate/#{user.activation_code}"
  end
  
  def forgot_password(user)
    setup_email(user)
    @subject    += 'You have requested to change your password'
    @body[:url]  = "https://#{APP_CONFIG['settings']['domain']}/reset_password/#{user.password_reset_code}" 
    @recently_forgot = false
  end

  def reset_password(user)
    setup_email(user)
    @subject    += 'Your password has been reset.'
    @recently_reset = false
  end

  def activation(user)
    setup_email(user)
    @subject    += 'Your account has been activated!'
    @body[:url]  = "https://#{APP_CONFIG['settings']['domain']}/"
  end

  protected

  def setup_email(user)
   recipients "#{user.employee.email}"
   from       %("DMS Directory" ) # Sets the User FROM Name and E-mail
   subject    "[DMS Directory] Account information "
   body       :user => user
   sent_on    Time.now
  end
end

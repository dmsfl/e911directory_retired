class PeopleFirst < ActiveRecord::Base
  acts_as_reportable
  belongs_to :employee
  
  def main_number
    four_11_phone.gsub(/\D\,''/)[3..9]
  end
  
  def area_code
    four_11_phone.gsub(/\D\,''/)[0..2]
  end
  
  def create_employee
    if !last_name.blank?
      e = Employee.new({
        :last_name=> last_name,
        :first_name=> first_name,
        :middle_name=> middle_name,
        :division_name=> division_name,
        :email => email,
        :people_first_user => people_first_user,
        :suffix => name_suffix,
        :contractor_flag => contractor_flag,
        :active_flag => 1,
        :olo_code => olo_code,
        :position_number => pos_num,
        :group_description => employee_group_desc
      })

      e.people_first_id = employee_id
      e.confidential = confidential # not sure why mass assignment wasn't working
      e.save(false)
      
    else
      false
    end
  
  end
  
  def contractor_flag
    self.contractor =~ /x/i ? true:false
  end
  
  def fill_in_employee
    emps = Employee.find(:all, :conditions => ['first_name LIKE ? and last_name LIKE ? and middle_name LIKE ? ', "%#{first_name}%", "%#{last_name}%", "%#{middle_name}%"])
    
    if emps.size != 1
      self
    else
      emps[0].update_attributes({
      :people_first_user => people_first_user,
      :suffix => name_suffix,
      :contractor_flag => contractor_flag,
      :active_flag => 0,
      :olo_code => olo_code,
      :position_number => pos_num,
      :group_description => employee_group_desc
      })
      emps[0].save(false)
      true
    end
  end
end

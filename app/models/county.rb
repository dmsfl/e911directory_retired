class County < ActiveRecord::Base
  has_many :buildings
  has_many :campuses, :through => :buildings
  has_many :locations, :through => :buildings 
  validates_presence_of :fips_code, :on => :create, :message => "can't be blank"
  validates_presence_of :name, :on => :create, :message => "can't be blank"
  validates_presence_of :people_first_code, :on => :create, :message => "can't be blank"
  
end

#= Search utility
# This model is a utility for searching any of the MasterRelation models
# based on the SQL 'LIKE' criteria. For example, to find all locations
# whose associated telephone.main_number includes the digits 44, do
#   locations = search_obj.find_locations(:main_number => '44')
# likewise, to retrieve only conditions, use:
#   conditions = search_obj.location_conditions(:main_number => '44')

class Search

  attr_accessor :first_name,
                :last_name,
                :middle_name,
                :area_code,
                :main_number,
                :phone_category_id,
                :confidential,
                :building_id,
                :vacant,
                :floor,
                :room,
                :city,
                :zip_code,
                :email,
                :contractor_flag,
                :active_flag,
                :full_street_address,
                :building,
                :audit_user,
                :audit_date,
                #Kept for completeness, but not used in search (see commented section below)
                :county,
                :campus,
                :search_type



  def initialize(params = {})
    params.each { |att,value| self.send("#{att}=", value) if methods.include?(att)}
  end
  def find_locations(*attrs)
    get_conditions(attrs)
    Location.find(:all, location_conditions)
  end

  def find_floors(*attrs)

    get_conditions(attrs)
    Location.find(:all, location_conditions.deep_merge({:select => 'locations.floor', :group => 'floor', :order => 'floor'})).map {|e| e.floor.nil? ? '' : e.floor}
  end
  def find_employees(*attrs)
    get_conditions(attrs)
    Employee.find(:all, employee_conditions)
  end
  def find_limited_employees(*attrs)
    get_conditions(attrs)
    Employee.find(:all,
                  employee_conditions.merge({:from => "employees USE INDEX (first_last_div)",
                  :select => "id, position_number, first_name, last_name, division_name"})
                  )
  end

  def find_phones(*attrs)
    get_conditions(attrs)
    Phone.find(:all, phone_conditions)
  end

  def phone_conditions(*attrs)
    get_conditions(attrs) unless attrs.empty?
    @search_type = 'phone'
    conditions_and_includes
  end

  def location_conditions(*attrs)
    get_conditions(attrs) unless attrs.empty?
    @search_type = 'location'

    conditions_and_includes
  end

  def employee_conditions(*attrs)
    get_conditions(attrs) unless attrs.empty?
    @search_type = 'employee'
    conditions_and_includes
  end

  def vacant=(value)
    unless value.nil?
      if value == '1' || value == 'false' || value == false
        @vacant = false
      elsif value == '0' || value == 'true' || value == true
        @vacant = true
      end
    else
      @vacant = nil
    end
  end

  def add_include(*includes)
    all_included = []
    includes.collect  do |include_me|
      unless include_me == @search_type
        if @associations.include?(include_me) && !all_included.include?(include_me)
          all_included << include_me
        elsif @associations.include?(include_me.pluralize) && !all_included.include?(include_me.pluralize)
          all_included << include_me.pluralize
        end
      end
    end
    all_included
  end

  def get_conditions(attrs)
    attrs.compact!
    if attrs.class.name != 'Array'
      @given_conditions = attrs.symbolize_keys!
    elsif attrs.empty?
      @given_conditions = {}
    else
      @given_conditions = attrs.inject({}){|conds, a| conds.merge!(a) unless a.nil?}.symbolize_keys!
    end

  end

  private

    # Phone number fields
    def area_code_conditions
        {:include => add_include('phone'),
        :condition => ["phones.area_code LIKE ?", "%#{@given_conditions[:area_code].gsub(/\D/,'')}%"]}
    end

    def main_number_conditions
      {:include => add_include('phone'),
      :condition => ["phones.main_number LIKE ?", "%#{@given_conditions[:main_number].gsub(/\D/,'')}%"]}
    end

    def phone_category_id_conditions
      {:include => add_include('phone', 'phone_category'),
       :condition => ["phone_categories.id LIKE ?", "#{@given_conditions[:phone_category_id]}"]}
    end

    def vacant_conditions
      if ['1','true',1 , true].include? @given_conditions[:vacant]
        condition = ["(coalesce(sum(employees.active_flag)) = 0 or coalesce(sum(employees.active_flag)) is null)"]
      else
        condition = ["coalesce(sum(employees.active_flag)) > 0"]
      end
        {
         :include => add_include( 'phone','employee'),
         :condition => condition
        }
    end

    # Location Fields
    def floor_conditions
      {:include => add_include('location'),
      :condition => ["locations.floor LIKE ?", "%#{@given_conditions[:floor].strip}%"]}
    end

    def room_conditions
      {:include => add_include('location'),
      :condition => ["locations.room LIKE ?", "%#{@given_conditions[:room].strip}%"]}
    end

    def city_conditions
      {:include => add_include('building'),
      :condition => ["buildings.city LIKE ?", "%#{@given_conditions[:city].strip}%"]}
    end

    def zip_code_conditions
      {:include => add_include('building'),
      :condition => ["concat(buildings.zip_code_5,buildings.zip_code_4) LIKE ?", "%#{@given_conditions[:zip_code]}%"]}
    end

    # Reverse lookup of counties through Location
    # does not work because delegate :county, :to => :building
    # does not build a LEFT OUTER JOIN automatically. Therefore the
    # single-query method used in this class does not work for 'delegated' relationships.
    # def county_conditions
        #  {:include => add_include('building','county'),
        #  :condition => ["counties.name LIKE ?", "%#{@given_conditions[:county]}%"]}
        # end
        #
        # def campus_conditions
        #   {:include => add_include('campuses'),
        #    :condition => ["campuses.name LIKE ?", "%#{@given_conditions[:campus]}%"]}
        # end

    def building_conditions
      {:include => add_include('building'),
       :condition => ["buildings.name LIKE ?", "%#{@given_conditions[:building].strip}%"]}
    end

    def building_id_conditions
      {:include => add_include('building'),
       :condition => ["buildings.id = ?", @given_conditions[:building_id]]}
    end
    def lookup_address_conditions
      {:include => add_include('building'),
       :condition => ["buildings.lookup_address LIKE ?", @given_conditions[:lookup_address]]}
    end

    def full_street_address_conditions
      {:include => add_include('building'),
       :condition => ["buildings.full_street_address LIKE ?", "%#{@given_conditions[:full_street_address].strip}%"]}
    end

    # Employee Fields
    def first_name_conditions
      {:include => add_include('employee'),
      :condition => ["employees.first_name LIKE ?", "%#{@given_conditions[:first_name].strip}%"]}
    end

    def last_name_conditions
      {:include => add_include('employee'),
      :condition => ["employees.last_name LIKE ?", "%#{@given_conditions[:last_name].strip}%"]}
    end

    def email_conditions
      {:include => add_include('employee'),
      :condition => ["employees.email LIKE ?", "%#{@given_conditions[:email].strip}%"]}
    end

    def contractor_flag_conditions
      if ['1','true',1 , true].include? @given_conditions[:contractor_flag]
        condition = ["employees.contractor_flag = 1"]
      else
        condition = ["(employees.contractor_flag = 0 or employees.contractor_flag is null)"]

      end
        {
         :include => add_include('employee'),
         :condition => condition
        }
    end

    def active_flag_conditions
      {:include => add_include('employee'),
      :condition => ["employees.active_flag LIKE ?", "%#{@given_conditions[:active_flag]}%"]}
    end
    def confidential_conditions
      if @given_conditions[:confidential] == false
        {:include => add_include('employee'),
        :condition => ["employees.confidential = ''"]}
      else
        {:include => add_include('employee'),
        :condition => ["(employees.confidential is null or employees.confidential is not null)"]}
      end
    end

    #Changes fields
    def audit_user_conditions
      @given_conditions[:audit_user] =~ /^(\d)*$/ ? field = 'user_id' : field = 'username'
      {:include => add_include('audit'),
      :condition => ["audits.#{field} = ? #{@given_conditions[:audit_user]}"]}
    end
    def audit_date_conditions
      case @given_conditions[:audit_date][:operator]
      when 'before'
        operator = '<'
      when 'after'
        operator = '>'
      when 'on'
        operator = '='
      else
        operator = '='
      end

      {:include => add_include('audit'),
      :condition => ["audits.created_at #{operator} DATE(?)", @given_conditions[:audit_date][:date].to_date.strftime("%Y-%m-%d")]}
    end


    def conditions_and_includes
      @associations = @search_type.classify.constantize.reflect_on_all_associations.collect{|association| association.name.to_s}
      if @given_conditions[:vacant].blank?
        {:conditions => [conditions_clauses.join(' AND '), *conditions_options], :include => conditions_includes}
      else

        {
         :select => 'employees.active_flag, phones.*',
         :having => [conditions_clauses.join(' AND '), *conditions_options],
         :include => conditions_includes,
         :group => 'phones.id'
        }
      end
    end

    def conditions_clauses
      conditions_parts.map { |condition_and_includes| condition_and_includes[:condition].first }
    end

    def conditions_options
      conditions_parts.map { |condition_and_includes| condition_and_includes[:condition][1..-1] }.flatten
    end

    def conditions_includes
      @includes = []
      conditions_parts.map {|condition_and_includes| condition_and_includes[:include] }.flatten.compact
    end

    def conditions_parts
      if @given_conditions.nil? || @given_conditions.empty?
        []
      else
        condition_keys = @given_conditions.delete_if {|k,v|  v.nil? || v == ''}.keys.map(&:to_sym)
        private_methods(false).grep(/_conditions$/).select {|e|
        condition_keys.include?  e.sub(/\_conditions$/,'').to_sym
        }.map { |m| send(m) }
      end
    end

end

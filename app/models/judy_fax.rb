class JudyFax < ActiveRecord::Base
  def phone
    p = long_number.gsub(/\D/,'')
    {
      :ac => '850',
      :mn => p[0..9],
      :ext => p[10..-1]
    }
  end

  def create_phone
    p = phone
    
    if (p[:ac] && p[:mn] && p[:ac].size == 3 && p[:mn].size == 7) && (p[:ext].blank? || (!p[:ext].blank? && (long_phone =~ /ext/)))
      if !Phone.create( { :area_code => p[:ac], :main_number => p[:mn], :phone_category => PhoneCategory.find_by_name('Fax'), :comment => comment })
        puts "Phone failure (#{id}): #{long_phone}"
        return false
      end
      true
    else
      false
    end

  end
end

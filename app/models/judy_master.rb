class JudyMaster < ActiveRecord::Base
  acts_as_reportable
  
  def phone
    p = long_phone.gsub(/\D/,'')
    {
      :ac => p[0..2],
      :mn => p[3..9],
      :ext => p[10..-1]
    }
  end
  
  def loc
    parsed_columns = {}
    s = room.strip
    if s =~ /\,/
      parsed_columns[:bi] = s.match(/Bldg (\w+)/)[1] rescue ""
      parsed_columns[:rm] = s.match(/, (\w+)$/)[1] rescue ""
      parsed_columns[:flr] = s.match(/Flr (\w+)/)[1] rescue ""
    else
      parsed_columns[:rm] = s
    end
      
    parsed_columns
  end
  
  def create_employee
    unless first_name.blank? && (last_name.blank? || last_name.strip.match(/vacant|vacant numbers/i)) && middle_name.blank?
      e = Employee.new
      e.first_name = first_name.gsub(/\".*\"/,'').strip.upcase unless first_name.nil?
      e.last_name = last_name.strip.upcase  unless last_name.nil?
      e.middle_name = middle_name.strip.upcase  unless middle_name.nil?
      e.email = email
      unless e.save(false)
        puts "Employee failure (#{id}): #{last_name}, #{first_name}"
        return false
      else
      return true
      end
    else
      return false
    end
  end
 
  def create_phone
    p = phone
    if (p[:ac] && p[:mn] && p[:ac].size == 3 && p[:mn].size == 7) && (p[:ext].blank? || (!p[:ext].blank? && (long_phone =~ /ext/)))
      if last_name =~ /conference/i
        phone_category = PhoneCategory.find_by_name('Conference')
      else
        phone_category = PhoneCategory.find_by_name('Voice')
      end
      if !Phone.create( { :area_code => p[:ac], :main_number => p[:mn], :phone_category => phone_category })
        puts "Phone failure (#{id}): #{long_phone}"
        return false
      end
      true
    else
      false
    end
  end
  
  def create_location
    l = loc
    if Building.find(:first, :conditions => ['lookup_address like ?', "%#{lookup_address}%"]).nil?
      
      return self
    elsif !Location.create( {
      :building_identifier => l[:bi],
      :floor => l[:flr],
      :room => l[:rm],
      :building => Building.find(:first, :conditions => ['lookup_address like ?', "%#{lookup_address}%"])
    })
      puts "Location failure (#{id}): #{room}"
      return false
    else
      return true
    end
  end
  
  def create_connections
    faulty_phones    = nil
    faulty_locations = nil
    multiple_location_conditions = nil
    faulty_employees = nil
    #assign phones to locations
    if (p = phone) && (l = loc)
      unless long_phone.blank? || related_phone = Phone.find(:first, :conditions => {'area_code' => p[:ac], 'main_number' => p[:mn]})
        faulty_phones = self
        print 'p'
      end
    end 


    #assign related employees.
    unless last_name.strip == 'Vacant'
      related_employee = related_employee_on_full_name || related_employee_on_email || related_employee_on_first_and_last || related_employee_on_last_and_building
      if related_employee.nil?
        faulty_employees = self
        related_employee = nil
        
        print 'e'
      end
    end  
    #assign locations
    building_id = Building.find(:first, :conditions => ['lookup_address LIKE ?', "%#{lookup_address}%"])
    related_location = Location.find(:all, :conditions => {'building_id' => building_id, 'floor' => l[:flr], 'room' => l[:rm], 'building_identifier' => l[:bi]})
    case related_location.size
      when 0
        faulty_locations = self
        related_location = nil
        print 'l'
      when 1
      else
        multiple_location_conditions << self
        related_location = nil  
        print 'l'
    end
    
    #finalize assignments
    ml = MasterRelation.new
    ml.phone    = related_phone    unless related_phone.nil?
    ml.extension = p[:ext] unless p[:ext].nil?
    ml.employee = related_employee unless related_employee.nil? || related_employee == false
    ml.location = related_location[0] unless related_location.nil?
    ml.save
    return {:faulty_phones => faulty_phones, :faulty_locations => faulty_locations, :faulty_employees => faulty_employees, :multiple_location_conditions => multiple_location_conditions} || true
  end
    
  def related_employee_on_full_name
    Employee.find(:first, :conditions => {'first_name' => first_name.gsub(/\".*/,'').strip, 'middle_name' => middle_name.strip, 'last_name' => last_name.strip})
  end
  
  def related_employee_on_email
    Employee.find(:first, :conditions => ['email LIKE ?', "%#{email.strip}%"])
  end
  
  def related_employee_on_first_and_last
    if (PeopleFirst.find(:all, :conditions => {'first_name' => first_name.gsub(/\".*/,'').strip, 'last_name' => last_name.strip}).size == 1 &&
        JudyMaster.find(:all, :conditions => {'first_name' => first_name.gsub(/\".*/,'').strip, 'last_name' => last_name.strip}).size == 1)
      related_employee = Employee.find(:first, :conditions => {'first_name' => first_name.gsub(/\".*/,'').strip, 'last_name' => last_name.strip})
    else
      nil
    end
  end
 
  def related_employee_on_last_and_building
    building = Building.find(:first, :conditions => ['lookup_address LIKE ?', "%#{lookup_address}%"])
    if (building && 
        PeopleFirst.find(:all, :conditions => ["last_name = ? AND ? regexp location_address", last_name.strip, building.lookup_address ]).size == 1 && 
        JudyMaster.find(:all, :conditions => ["last_name = ? AND ? regexp lookup_address", last_name.strip, building.lookup_address ]).size == 1)
      noo = PeopleFirst.find(:first, :conditions => ["last_name = '#{last_name.strip}' AND ? regexp location_address", building.lookup_address])
      related_employee = Employee.find(:first, :conditions => {'first_name' => noo.first_name, 'last_name' => noo.last_name, 'middle_name' => noo.middle_name})
    elsif (building &&
          PeopleFirst.find(:all, :conditions => ["first_name = ? AND ? regexp location_address", first_name.strip, building.lookup_address ]).size == 1 &&
          JudyMaster.find(:all, :conditions => ["first_name = ? AND ? regexp lookup_address", first_name.strip, building.lookup_address ]).size == 1)
      noo = PeopleFirst.find(:first, :conditions => ["first_name = '#{first_name.strip}' AND ? regexp location_address", building.lookup_address])
      related_employee = Employee.find(:first, :conditions => {'first_name' => noo.first_name, 'last_name' => noo.last_name, 'middle_name' => noo.middle_name})
    else
      nil
    end
  end

end

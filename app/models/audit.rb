
# The Audit class actually belongs to the acts_as_audited plugin
# We use instance_eval to improve forward compatibility with future
# releases

Audit.instance_eval do

  NOT_TRACKED_FOR_PEOPLE_FIRST = Set['primary_phone_location','primary_employee_for_phone', 'primary_phone_for_employee']
  NOT_TRACKED_FOR_911 = Set['used_for_411']

  def people_first_changes
    changes ? changes.reject {|chg, val| Set[chg].subset? NOT_TRACKED_FOR_PEOPLE_FIRST} : {}
  end
  def nine_one_one_changes
    changes ? changes.reject {|chg, val| Set[chg].subset? NOT_TRACKED_FOR_911} : {}
  end
  
  
private
  
  def mark_non_people_first_sent
    self.sent = true if people_first_changes.empty?
  end
  
  def mark_non_911_sent
    self.sent_to_911 = true if nine_one_one_changes.empty?
  end

end

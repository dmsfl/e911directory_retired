class Location < ActiveRecord::Base
  include AuditHelper
  default_scope :order => 'floor, room'
  belongs_to :building
  delegate :county, :campus, :to => :building
  
  has_many :master_relations #, :dependent => :nullify not used because I want to handle more stuff in the after_destroy filter
  has_many :employees, :through => :master_relations
  has_many :phones, :through => :master_relations  
  
  before_destroy :remove_master_relations
  acts_as_audited
  acts_as_reportable
  
  def to_label
    b = building.nil? ? nil : "#{building.full_street_address}<br />#{building.city} #{building.state}"
    f = floor.nil? ? nil : "Flr #{floor}"
    r = room.nil? ? nil : "Rm #{room}"
    bi = building_identifier.nil? ? nil : "Bldg ID #{building_identifier}"
    [b, bi, f, r].compact.join(', ')
  end
  def to_s
    f = floor.nil? ? nil : "Flr #{floor}"
    r = room.nil? ? nil : "Rm #{room}"
    [building_identifier, f, r].compact.join(', ')
  end

  def remove_master_relations
    associated_primaries = master_relations.select {|ml| ml.primary_employee_location || ml.primary_phone_location}
    if associated_primaries
      associated_primaries.each do |ml| 
        ml.primary_phone_location = false if ml.primary_phone_location
        ml.primary_employee_location = false if ml.primary_employee_location
        ml.location = nil
        ml.save
      end
    end
  end

end

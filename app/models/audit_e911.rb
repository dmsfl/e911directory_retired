require 'set'
# Audit saves the changes to ActiveRecord models.  It has the following attributes:
#
# * <tt>auditable</tt>: the ActiveRecord model that was changed
# * <tt>user</tt>: the user that performed the change; a string or an ActiveRecord model
# * <tt>action</tt>: one of create, update, or delete
# * <tt>changes</tt>: a serialized hash of all the changes
# * <tt>created_at</tt>: Time that the change was performed
#
class AuditE911 < Audit
  
  attr_accessible :auditable_type

  named_scope :nightly_changes_from_people_first, lambda { |*date| 
    date.first ? changes_for_date = date : changes_for_date = Date.today
    {:conditions => ['username = ? AND DATE(created_at) = DATE(?)', APP_CONFIG['settings']['cron_auditor'], changes_for_date]} 
  }
  named_scope :monthly_changes_from_people_first, lambda { |*month| 
    month.first ? changes_for_month = month : changes_for_month = Date.today
    {:conditions => ['username = ? AND MONTH(created_at) = MONTH(?)', APP_CONFIG['settings']['cron_auditor'], changes_for_month]} 
  }
  named_scope :all_unsent_user_changes, :conditions =>   ["user_type = 'User' and sent = ?", false]
  
  
  #Returns the old version of the current Audit record
  def parent
    if version == 1
      revision
    else
      self.class.find(:first,
        :conditions => ['auditable_id = ? and auditable_type = ? and version = ?',
        auditable_id, auditable_type, version-1]).revision
    end
  end

  def ancestors_as_of(time)
    self.class.find(:all, :order => 'version',
      :conditions => ['auditable_id = ? and auditable_type = ? and version <= ?',
      auditable_id, auditable_type, version])

  end
  
  def change_description
    case auditable_type
    when 'MasterRelation'
      master_relation_change
    when 'Phone'
      phone_change
    when 'Location'
      location_change
    when 'Employee'
      employee_change
    else
      simple_change
    end
  end
  
  def scheduled_send
    APP_CONFIG['mail']['people_first_send_time']
  end
  def self.only_people_first(records)
    records.reject {|audit| NOT_TRACKED_FOR_PEOPLE_FIRST.superset? audit.changes.keys}
  end
  
  def self.non_people_first
    NOT_TRACKED_FOR_PEOPLE_FIRST
  end
  
  def self.select_only_people_first_changes(unsent_changes)
    unsent_changes.select do |unsent_change|
      revision = unsent_change.revision
      case
      when unsent_change.destroyed_and_used_for_411_was_false?
        false
      when unsent_change.destroyed_and_primary_employee_location_was_false?
        false
      when unsent_change.set_used_for_411_to_true?
        true
      when unsent_change.set_used_for_411_to_false?
        true
      when unsent_change.set_primary_employee_location_to_true?
        true
      when unsent_change.set_primary_employee_location_to_false?
        true
      else
        false
      end
    end
  end
  
  def set_used_for_411_to_true?
    changes['used_for_411'] && (changes['used_for_411'].instance_of?(Array) ? changes['used_for_411'][1] == '1' : changes['used_for_411'] == '1')
  end
  
  def set_used_for_411_to_false?
    changes['used_for_411'] && (changes['used_for_411'].instance_of?(Array) ? changes['used_for_411'][0] == '1' : changes['used_for_411'] == '1')
  end
  
  def destroyed_and_used_for_411_was_false?
    action == 'destroy' && (changes['used_for_411'] == nil || changes['used_for_411'] == '0')
  end
  
  def destroyed_and_primary_employee_location_was_false?
    action == 'destroy' && (changes['primary_employee_location'] == nil || changes['primary_employee_location'] == '0')
  end
  
  def set_primary_employee_location_to_true?
    changes['primary_employee_location'] && (changes['primary_employee_location'].instance_of?(Array) ? changes['primary_employee_location'][1] == '1' : changes['primary_employee_location'] == '1')
  end
  
  def set_primary_employee_location_to_false?
    changes['primary_employee_location'] && (changes['primary_employee_location'].instance_of?(Array) ? changes['primary_employee_location'][0] == '1' : changes['primary_employee_location'] == '1')
  end
private

  def phone_change
    create_message ({
     :create => 'Create a new phone number',
     :destroy => 'Delete a phone number',
     :update => 'Change the phone properties'
    })
  end
  def location_change
    create_message ({
     :create => 'Create a new location',
     :destroy => 'Delete a location',
     :update => "Change the location proterties"
    })
  end
  def employee_change
    create_message ({
     :create => 'Add a new employee',
     :destroy => 'Remove an employee',
     :update => "Change an employee"
    })
  end
  def master_relation_change
    create_message ({
     :create => master_relation_create,
     :destroy => master_relation_destroy,
     :update => master_relation_update
    })
  end

  def master_relation_create
    rev = revision
    
    case
    when rev.employee_id && rev.location_id
      'Add an employee to a location'
    when rev.employee_id && rev.phone_id
      'Add a phone number to an employee'
    when rev.location_id && rev.phone_id
      'Add a phone number to a location'
    end
  end
  def master_relation_update
    result = []
    changes = self.changes || {}

	  if pl = changes['primary_employee_location']
      #Set a primary location    
      result << "Set the listed location to the employee's work location address." if (pl.instance_of?(Array) ? pl[1] == '1' : pl == '1')

      #Remove a primary location
      result << "Remove the listed location from the employee's work location address."  if (pl.instance_of?(Array) ? pl[1] == '0' || pl[1].nil? : pl == '0' || pl.nil?)
    end
    
    if used_411 = changes['used_for_411']
      #Set the 411 phone number
      result << "Set the listed phone number, to the employee's 411 phone number." if (used_411.instance_of?(Array) ? used_411[1] == '1' : used_411 == '1')
      
      #Remove the 411 phone number
      result << "Remove the listed phone number from the employee's 411 phone number field."  if (used_411.instance_of?(Array) ? used_411[1] == '0' || used_411[1].nil? : used_411 == '0' || used_411.nil?)
	  
    end
    unless result.empty?
      result.join('<br/>')
    else
      false
    end
  end

  def master_relation_destroy
    result = []
    changes = self.changes || {}

    if pl = changes['primary_employee_location']
      #Remove a primary location
      result << "Remove the listed location from the employee's work location address."  if (pl[0] == '1' && !changes['employee_id'].blank? && !changes['location_id'].blank?)
    end
    
    if used_411 = changes['used_for_411']
      #Remove the 411 phone number
      result << "Remove the listed phone number from the employee's 411 phone number field."  if (used_411[0] == '1' && !changes['employee_id'].blank? && !changes['phone_id'].blank?)
    end
    
    unless result.empty?
      result.join('<br/>')
    else
      false
    end
  end
  def create_message (messages)
    messages.with_indifferent_access[action]
  end

end

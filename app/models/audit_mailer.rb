class AuditMailer < ActionMailer::Base 

  helper :audits
  
  def user_changes_to_people_first(user = 'all')
     setup_email

     if user == 'all'
       @unsent_changes =AuditE911.all_unsent_user_changes
     else
       @unsent_changes = AuditE911.find(:all, :conditions => {:user_id => user.id, :sent => false})
     end
     @unsent_changes = AuditE911.select_only_people_first_changes(@unsent_changes)
     recipients APP_CONFIG['mail']['people_first_updaters']
     subject "#{APP_CONFIG['mail']['subject_prefix']} Request for change"  
     body :unsent_changes => @unsent_changes
  end
  
  def user_changes_to_telcom(user)
     setup_email
     @unsent_changes =AuditE911.find(:all, :conditions => {:user_id => user.id, :sent => false})
     recipients APP_CONFIG['mail']['telecom_interested_parties']
     subject "#{APP_CONFIG['mail']['subject_prefix']} Request for change"  
     body :unsent_changes => @unsent_changes
  end
  
  def user_friendly_ip_phone_changes(status_report)
    operation = status_report.operation_with(:destination => MasterRelation)
    inserts = operation.inserts
    updates = operation.updates
    setup_email
    recipients APP_CONFIG['mail']['telecom_interested_parties']
     subject "#{APP_CONFIG['mail']['subject_prefix']} IP Phone change"  
     body :inserts => inserts, :updates => updates, :operation => operation
  end
  def cron_rake_message(message = "Rake error occured in the #{APP_CONFIG['settings']['name']} application")
    setup_email
    recipients APP_CONFIG['mail']['apps_support']
    body :message => message
  end
  
  def setup_email
     from APP_CONFIG['mail']['from']
     sent_on Time.now 
     content_type "text/html"
  end
end

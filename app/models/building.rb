class Building < ActiveRecord::Base
  default_scope :order => 'buildings.name'
  belongs_to :campus
  belongs_to :county
  has_many :locations, :dependent => :destroy
  has_many :master_relation, :through => :locations  
  STREET_ABBS_RX = Regexp.new(StreetAbbreviation.abbs.join('|'))
  validates_format_of(:street_suffix, :with => STREET_ABBS_RX, :message => "is invalid or non-standard.", :allow_nil => true, :allow_blank => true)
  validates_presence_of :city
  validates_presence_of :state
  validates_presence_of :zip_code_5
  #Used for advanced_find functionality
  attr_accessor :campus_name, :county_name, :zip_code, :building_name
  
  acts_as_reportable
  acts_as_audited
  before_validation :capitalize_all
  before_save :update_full_street_address

  # VALID VALUES
  #These values did not warrant their own code tables
  # They are directionals for street addresses
  def self.DIRECTIONALS
    %w{N S E W NE NW SE SW}
  end

  def capitalize_all
    [ :street_number_suffix, 
      :prefix_directional, :street_name, 
      :street_suffix, :post_directional].each do |attr|
        send("#{attr.to_s}=", send(attr).upcase) unless send(attr).blank?
    end
  end
  
  
  def name_and_city
    [name, city].join(', ')
  end
  
  def to_label
    case
    when !name.blank?
      name
    when !full_street_address.blank?
      full_street_address
    else 
      city
    end
  end
  
  
  def advanced_find
    Building.find(:all, :include => ['campus', 'county'], :conditions => conditions)
  end
  
  def conditions
    [conditions_clauses.join(' AND '), *conditions_options]
  end
  
 
  def update_full_street_address
    self.full_street_address = [street_number, street_number_suffix, prefix_directional, street_name, street_suffix, post_directional].compact.join(' ')
  end
  
  private
  def county_conditions
    ["counties.name LIKE ?", "%#{county_name}%"] unless county_name.blank?
  end
      
  def campus_name_conditions
    ["campuses.name LIKE ?", "%#{campus_name}%"] unless campus_name.blank?
  end

  def campus_id_conditions
    ["campuses.id = ?", campus_id] unless campus_id.blank?
  end

  
  def city_conditions
    ["buildings.city LIKE ?", "%#{city}%"] unless city.blank?
  end
      
  def zip_code_conditions
    ["concat(buildings.zip_code_5, buildings.zip_code_4) LIKE ?", "%#{zip_code}%"] unless zip_code.blank?
  end

  def building_name_conditions
    ["buildings.name LIKE ?", "%#{building_name}%"] unless building_name.blank?
  end

  def full_street_address_conditions
    ["full_street_address LIKE ?", "%#{full_street_address}%"] unless full_street_address.blank?
  end


  def conditions_clauses
    conditions_parts.map { |condition| condition.first }
  end

  def conditions_options
    conditions_parts.map { |condition| condition[1..-1] }.flatten
  end

  def conditions_parts
    private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
  end

end

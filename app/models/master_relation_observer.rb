class MasterRelationObserver < ActiveRecord::Observer
  def after_save(master_relation)
    UserMailer.deliver_new_user(master_relation) if user.recently_created?
    UserMailer.deliver_forgot_password(master_relation) if user.recently_forgot_password?
    UserMailer.deliver_reset_password(master_relation) if user.recently_reset_password?
  end 

end
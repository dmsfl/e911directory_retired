
class Employee < ActiveRecord::Base
  include AuditHelper
  default_scope :order => 'employees.last_name, employees.middle_name, employees.first_name'
  has_one :user
  has_many :master_relations
  has_many :phones, :through => :master_relations
  has_many :locations, :through => :master_relations
  has_one :people_first, :primary_key => 'people_first_id'
  validates_presence_of     :email
  validates_length_of       :email,       :within => 6..100 #r@a.wk
  validates_uniqueness_of   :email
  validates_format_of       :email,       :with => Authentication.email_regex, :message => Authentication.bad_email_message
  
  validates_presence_of     :people_first_user
  validates_uniqueness_of   :people_first_user
  
  validates_presence_of     :first_name
  validates_format_of       :first_name,  :with => Authentication.name_regex,  :message => Authentication.bad_name_message
  validates_length_of       :first_name,  :maximum => 100, :message => "First name must be between 1 and 100 characters"
  
  validates_presence_of     :last_name
  validates_format_of       :last_name,   :with => Authentication.name_regex,  :message => Authentication.bad_name_message
  validates_length_of       :last_name,   :maximum => 100, :message => "Last name must be between 1 and 100 characters"
  
  validates_inclusion_of :active_flag, :in => [1, true], :message => " is set to 'inactive.'"

  acts_as_reportable
  
  # HACK HACK HACK -- how to do attr_accessible from here? 
  # prevents a user from submitting a crafted form that bypasses activation
  # anything else you want your user to change should be added here.
  attr_accessible :email, 
                  :first_name,
                  :middle_name, 
                  :last_name,
                  :people_first_user,
                  :suffix,
                  :contractor_flag,
                  :active_flag,
                  :olo_code,
                  :position_number,
                  :group,
                  :confidential,
                  :division_name
  attr_accessor   :group_description,
                  :primary_labels
  
  after_save :remove_from_primary_employee_for_phones_if_necessary
  
  #Placed after attr_accessible due to audited plugin requirement
  acts_as_audited

  named_scope :with_same_division_as, lambda {|employee|
    if employee.instance_of?(Employee) 
      division = employee.division_name 
    elsif emp = (Employee.find(employee.to_i) rescue false)
      division = emp.division_name
    else
      return {:conditions => {:division_name => 'No employee found'}}
    end
    {:conditions => {:division_name => division}, :include => ['phones', 'locations']}
  }
  def email=(value)
    write_attribute :email, (value ? value.downcase : nil)
  end

  def full_name
    [first_name, middle_name, last_name].compact.collect {|e| e.capitalize}.join(' ')
  end
  alias_method :to_label, :full_name

  #I realize this is not ideal, however with pagination, we should be able to run 10 queries at a time on the database without too much performance loss
  #In the future we may want to take the time to figure out how to use named_scopes to achieve a similar goal.
  def get_primary_labels
    location = ''
    phone = ''
    phone_category = ''
    phone_411 = ''
    phone_category_411 = ''
    phone_id = ''
    master_relations = MasterRelation.find(:all, :include => ['phone','location'], 
                                           :conditions => "employee_id = #{id} AND (primary_phone_for_employee = 1 OR primary_employee_location = 1 OR used_for_411 = 1)")
    if !master_relations.empty?
      master_relations.each do |ml|
        if ml.primary_employee_location == true
          location = ml.location.to_label
          location_id = ml.location.id
        end
        if ml.primary_phone_for_employee == true
          phone = ml.phone.to_label
          phone_category = ml.phone_category.name
          phone_id = ml.phone.id
        end
        if ml.used_for_411 == true
          phone_411 = ml.phone.to_label
          phone_category_411 = ml.phone_category.name
        end
      end
    end
    @primary_labels = {:location => location, :phone => phone, :phone_category => phone_category, :phone_id => phone_id, :phone_411 => phone_411,:phone_category_411 => phone_category_411}
  end

  # the ":dependent => :nullify" option would not invoke the necessary 
  # callbacks to audit master_relation table changes
  # so we do that manually with a 'before_destroy' callback.
  def before_destroy
    master_relations.each {|ml| ml.update_attribute('employee', nil)}
    self.user.destroy if self.user
  end

  def remove_from_primary_employee_for_phones_if_necessary
    unless self.active_flag
      master_relations_with_primary_employee_for_phone = self.master_relations.select {|ml| ml.primary_employee_for_phone}
      
      master_relations_with_primary_employee_for_phone.each do |ml|
        ml.primary_employee_for_phone = false
        ml.save
      end
    end
  end
  def sorted_master_relations
    self.master_relations.sort {|a,b|
      case
        when a.primary_employee_location
          -1
        when b.primary_employee_location
           1
        when a.primary_phone_for_employee
          -1
        when b.primary_phone_for_employee
          1
        when a.location.nil? && b.location.nil?
          sort_by_phone(a,b)
        when a.location.nil?
          1
        when b.location.nil?
          -1
        when (a.location.to_label <=> b.location.to_label) == 0
          sort_by_phone(a,b)
        else
          a.location.to_label <=> b.location.to_label
      end
    }
  end
  def sort_by_phone(a,b)
    case
      when a.phone.nil? && b.phone.nil?
        0
      when a.phone.nil?
        1
      when b.phone.nil?
        -1
      else
        a.phone.to_label <=> b.phone.to_label
    end
  end
end

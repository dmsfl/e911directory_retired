class Phone < ActiveRecord::Base
  include AuditHelper
  
  belongs_to :phone_category
  has_many :master_relations, :dependent => :nullify
  has_many :employees, :through => :master_relations
  has_many :locations, :through => :master_relations 
  default_scope :order => 'phones.area_code, phones.main_number '
  validates_length_of :area_code, :within => 3..3
  validates_length_of :main_number, :within => 7..7, :message => 'must be exactly 7.'
  validates_uniqueness_of :main_number, :scope => :area_code
  attr_accessor :main_number_3, :main_number_4, :not_searching, :primary_labels
  before_validation :clean_main_number
  
  after_destroy :remove_primary_location
  
  named_scope :ascend_by_number, {:order => 'area_code asc, main_number asc'}
  named_scope :descend_by_number, {:order => 'area_code desc, main_number desc'}

  # validates_length_of :area_code, :within => 3..3, :on => [:create, :update], :message => "is not exactly 3 digits.", :if => :not_searching
    # validates_length_of :main_number_3, :within => 3..3, :on => [:create, :update], :message => "is not exactly 3 digits.", :if => :not_searching
    # validates_length_of :main_number_4, :within => 4..4, :on => [:create, :update], :message => "is not exactly 4 digits.", :if => :not_searching
    # validates_numericality_of :area_code, :on => :create, :message => "is not a number.", :if => :not_searching
    # validates_numericality_of :main_number_3, :on => [:create, :update], :message => "is not a number.", :if => :not_searching
    # validates_numericality_of :main_number_4, :on => [:create, :update], :message => "is not a number.", :if => :not_searching
    # 
    # def initialize
    #   self.not_searching = true
    # end
  acts_as_audited :include => [:master_relations, :employees, :locations]
  acts_as_reportable
  def sorted_master_relations
    self.master_relations.sort {|a,b|
      case
        when a.primary_employee_for_phone
          -1
        when b.primary_employee_for_phone
           1
        when a.primary_phone_location
          -1
        when b.primary_phone_location
          1
        when a.employee.nil? && b.employee.nil?
          sort_by_location(a,b)
        when a.employee.nil?
          1
        when b.employee.nil?
          -1
        when (a.employee.last_name <=> b.employee.last_name) == 0
          sort_by_location(a,b)
        else
          a.employee.last_name <=> b.employee.last_name
      end
    }
  end
  def sort_by_location(a,b)
    case
      when a.location.nil? && b.location.nil?
        0
      when a.location.nil?
        1
      when b.location.nil?
        -1
      else
        a.location.to_label <=> b.location.to_label
    end
  end
  
  def to_label
    "(#{area_code}) #{main_number[0..2]}-#{main_number[3..6]}"
  end

  #main_number_ methods allow forms to submit parts of main numbers.
  def main_number_3
    main_number[0..2]
  end
  
  def main_number_4
    main_number[3..6]
  end
  
  def main_number_3=(value)
    self.main_number = digitize(value) + main_number_4
  end
  
  def main_number_4=(value)
    self.main_number = main_number_3 + digitize(value)
  end
  
  def clean_main_number
      self.main_number= digitize(main_number)
  end

  #I realize this is not ideal, however with pagination, we should be able to run 10 queries at a time on the database without too much performance loss
  #In the future we may want to take the time to figure out how to use named_scopes to achieve a similar goal.
  def get_primary_labels
    master_relations = MasterRelation.find(:all, :include => ['employee','location'], 
                                           :conditions => "phone_id = #{id} AND (primary_employee_for_phone = 1 OR primary_phone_location = 1)")
    if !master_relations.blank?
      if master_relations[0].primary_phone_location == true 
        location = master_relations[0].location.to_label
        location_id = master_relations[0].location.id
      elsif master_relations[1] && master_relations[1].primary_phone_location == true 
        location = master_relations[1].location.to_label
        location_id = master_relations[1].location.id
      else
        location = ''
        location_id = ''
      end
      
      if master_relations[0].primary_employee_for_phone == true 
        employee = master_relations[0].employee.to_label
        employee_id = master_relations[0].employee.id
        employee_confidential = !master_relations[0].employee.confidential.blank? 
      elsif master_relations[1] && master_relations[1].primary_employee_for_phone == true 
        employee = master_relations[1].employee.to_label
        employee_id = master_relations[1].employee.id
      else
        employee = ''
      end
      @primary_labels = {:location => location, :employee => employee, :employee_id => employee_id.to_s, :confidential => employee_confidential}
    else
      @primary_labels = {:location => '', :employee => ''}
    end
  end

  def digitize(value)
    value.gsub /\D/,''    
  end
  
  def nine_11_employee
    nine_11_master_relations = master_relations.select {|ml| ml.primary_phone_location? && ml.employee}
    if nine_11_master_relations.size > 1
      raise TooMany911LocationsError
    elsif nine_11_master_relations.size == 1
      nine_11_master_relations[0].employee
    else
      nil
    end
  end
  
  def nine_11_location
    nine_11_master_relations = master_relations.select {|ml| ml.primary_phone_location? && ml.location}
    if nine_11_master_relations.size > 1
      raise TooMany911LocationsError
    elsif nine_11_master_relations.size == 1
      nine_11_master_relations[0].location
    else
      nil
    end
  end

  def remove_primary_location
    master_relation_with_primary_location = get_master_relation_primary_phone_location
    master_relation_with_primary_location[0].update_attribute(:primary_phone_location, false) if master_relation_with_primary_location.size == 1
  end
  
  def get_master_relation_primary_phone_location
    master_relations.select {|ml| ml.primary_phone_location}
  end

end

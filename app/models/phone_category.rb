class PhoneCategory < ActiveRecord::Base
  has_many :phones
  has_many :master_relations, :through => 'phones'
  
  acts_as_reportable
  
  def to_label
    name
  end
end
